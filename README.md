# Introduction

This is a library to support the ISO 14819 (TMC) standard ISO.

Normative References:
* DIN EN ISO 14819: Traffic and Travel Information (TTI) - TTI messages via traffic message coding
* DIN EN ISO 14819-1:2014-05: Part 1: Coding protocol for Radio Data System
* DIN EN ISO 14819-2:2014-05: Part 2: Event and information codes for Radio Data System
* DIN EN ISO 14819-3:2014-05: Part 3: Location referencing for ALERT-C
* DIN EN ISO 14819-6:2007-03: Part 6: Encryption and conditional access for the Radio Data System

Additional References:
* TMC Handbook - Location Table Exchange Format

Sources for TMC location code lists (from http://wiki.openstreetmap.org/wiki/TMC):
* Germany: http://www.bast.de/nn_42744/DE/Aufgaben/abteilung-f/referat-f4/Location-Code-List/location-code-list-start.html
* Italy: https://www.ilportaledellautomobilista.it/http://vps.ilportaledellautomobilista.it:8080/portal/dt?JSPTabContainer.setSelected=JSPTabContainer%2FUtilita&last=false&targetPage=database
* Italy: http://www.radio.rai.it/cciss/databasetmc.cfm
* Norway: http://www.vegvesen.no/en/Professional/Technology/RDS+TMC
* Sweden: http://www20.vv.se/tmc/document/Sweden2007.zip

# Build on Linux (e.g. Debian Testing)

Building under Linux works as usual:

    mkdir build
    cd build
    cmake ..
    make
    make install DESTDIR=..
    make package

# Build on Windows (e.g. Windows 7 64-Bit)

Building under Windows contains the following steps:

* Use cmake-gui
* Set "Where is the source code:" to the root directory.
* Set "Where to build the binaries:" to folder "build" below the root directory.
* Configure and Generate
* Open the Visual Studio Solution (.sln) file in the build folder.
* Compile it in Release Configuration.

# Test

Static tests are

* Cppcheck (if OPTION_RUN_CPPCHECK is set)
* CCCC (if OPTION_RUN_CCCC is set)

Dynamic tests are

* Unit tests (if OPTION_RUN_TESTS is set)
* Example runs (if OPTION_RUN_EXAMPLES is set)
* Coverage (if OPTION_USE_LCOV_GCOV is set)

The test execution can be triggered using

    make test

# Package

The package generation can be triggered using

    make package

# Repository Structure

The following files are part of the source code distribution:

* src/_project_/
* src/_project_/tests/

The following files are working directories for building and testing:

* build/_project_/

The following files are products of installation and building:

* bin/
* lib/
* share/doc/_project_/
* share/man/
* include/_project_/

# Wanted features

* None

# Missing test coverage

* No tests implemented yet.
