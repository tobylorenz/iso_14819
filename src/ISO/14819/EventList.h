/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include <cstdint>
#include <string>
#include <map>

#include <sqlite3.h>

#include "Event.h"

#include "iso_14819_export.h"
#include "platform.h"

namespace ISO14819 {

/**
 * @brief Event List
 *
 * The Event List (EL) contains tables to decode the characteristics of an event.
 */
class ISO_14819_EXPORT EventList
{
public:
    /**
     * @brief Open TMC Event List database
     * @param[in] language Language
     *
     * This function checks if the database is open.
     * In case it's not it opens the database and reads in all language-independent constants.
     */
    explicit EventList(std::string language = "en_CEN");

    /**
     * Load a different language.
     *
     * If this is unsuccessful, the default language en_CEN will be used.
     *
     * @param[in] language Language
     */
    void setLanguage(std::string language = "en_CEN");

    /**
     * Returns the filename resp. language.
     *
     * @return filename resp. language
     */
    std::string language() const;

    /**
     * Get event
     *
     * @param[in] eventCode event code
     * @return Event
     */
    Event event(uint16_t eventCode);

    /**
     * Get back Event string from EL/FEL tables
     *
     * @param[in] eventCode event Code (1-999)
     * @param[in] quantifiers Number of quantifiers
     * @return Phrase string
     */
    std::string fullPhrase(uint16_t eventCode, uint8_t quantifiers) const;

    /**
     * @brief Get back Event or Phrase Code string from PC/SIL tables
     *
     * This function returns the Phrase (A..Y) or Supplementary Info (Z) string.
     *
     * @param[in] letter Letter (A..Y=Phrase Code) (Z=Supplementary Info Code)
     * @param[in] number Number in case of letter='Z', Code number (1-999) otherwise
     * @param[in] quantifiers Number of quantifiers
     * @return Event or Phrase Code string
     */
    std::string phrase(char letter, uint16_t number, uint8_t quantifiers) const;

    /**
     * Return quantifier for label and number of quantifiers.
     *
     * @param[in] label Label (2..4)
     * @param[in] quantifier (0..5)
     * @param[in] code (0..32)
     * @return Quantifier string
     */
    std::string quantifier(uint8_t label, uint8_t quantifier, uint8_t code) const;

private:
    /**
     * @brief Language of TMC Event List database
     *
     * This variable contains the language of the Event List database.
     */
    std::string m_language;

    /**
     * @brief Pointer to the TMC Event List database
     *
     * This variable contains the pointer to the Event List database.
     */
    sqlite3 * m_database;

    /** Event List from EL/FEL tables */
    std::map<uint16_t, Event> m_events;

    /**
     *
     * Tries to load a different language.
     *
     * @return true on success, false otherwise
     */
    bool loadDatabase(std::string language = "en_CEN");
};

/**
 * Return reference to EventList.
 *
 * @return Reference to EventList
 */
extern ISO_14819_EXPORT EventList & eventList(); /* singleton */

}
