/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "EnhancedMode.h"

#include <cassert>

namespace ISO14819 {

EnhancedMode::EnhancedMode() :
    m_activityTime(0),
    m_windowTime(0),
    m_delayTime(0)
{
}

uint8_t EnhancedMode::activityTime() const
{
    return m_activityTime;
}

void EnhancedMode::setActivityTime(uint8_t value)
{
    /* check */
    assert(value <= 3);

    /* set */
    m_activityTime = value;
}

uint8_t EnhancedMode::windowTime() const
{
    return m_windowTime;
}

void EnhancedMode::setWindowTime(uint8_t value)
{
    /* check */
    assert(value <= 3);

    /* set */
    m_windowTime = value;
}

uint8_t EnhancedMode::delayTime() const
{
    return m_delayTime;
}

void EnhancedMode::setDelayTime(uint8_t value)
{
    /* check */
    assert(value <= 3);

    /* set */
    m_delayTime = value;
}

}
