/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include "iso_14819_export.h"
#include "platform.h"

namespace ISO14819 {

/** message geographical scope (MGS) */
class ISO_14819_EXPORT MessageGeographicalScope
{
public:
    explicit MessageGeographicalScope();

    /**
     * Get international
     *
     * @return International
     */
    bool international() const;

    /**
     * Set international
     *
     * @param[in] value International
     */
    void setInternational(bool value);

    /**
     * Get national
     *
     * @return National
     */
    bool national() const;

    /**
     * Set national
     *
     * @param[in] value National
     */
    void setNational(bool value);

    /**
     * Get regional
     *
     * @return Regional
     */
    bool regional() const;

    /**
     * Set regional
     *
     * @param[in] value Regional
     */
    void setRegional(bool value);

    /**
     * Get urban
     *
     * @return Urban
     */
    bool urban() const;

    /**
     * Set urban
     *
     * @param[in] value Urban
     */
    void setUrban(bool value);

private:
    /** International */
    bool m_international : 1;

    /** National */
    bool m_national : 1;

    /** Regional */
    bool m_regional : 1;

    /** Urban */
    bool m_urban : 1;
};

}
