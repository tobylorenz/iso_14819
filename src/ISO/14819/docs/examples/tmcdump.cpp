/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include <unistd.h>

#include <array>
#include <cassert>
#include <cstdio>
#include <cstdint>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iomanip>
#include <iostream>
#include <string>
#include <sstream>

#include <ISO/62106.h>
#include <ISO/14819.h>
#include <sigc++-2.0/sigc++/signal.h>

#include "RdsFile.h"

/** Language ID */
static uint8_t languageId = 0;

/**
 * Print time.
 *
 * @param[in] time time
 */
static void printTime(ISO62106::RdsTime time)
{
    std::cout
            << std::setfill('0') << std::dec
            << std::setw(4) << (time.year() + 1900) << "-"
            << std::setw(2) << static_cast<uint16_t>(time.month()) << "-"
            << std::setw(2) << static_cast<uint16_t>(time.day())
            << " "
            << std::setw(2) << static_cast<uint16_t>(time.hour()) << ":"
            << std::setw(2) << static_cast<uint16_t>(time.minute())
            << " ("
            << (time.signLocalTimeOffset() ? "-" : "+")
            << std::setw(2) << static_cast<uint16_t>(time.localTimeOffset() / 2) << ":"
            << std::setw(2) << static_cast<uint16_t>(30 * (time.localTimeOffset() % 2))
            << ")" << std::endl;
}

/**
 * Print information on event
 *
 * @param[in] message Message
 */
static void printEvent(ISO14819::Message & message)
{
    std::cout << "Event List Entry:" << std::endl;
    std::cout << "  Event: " << message.event() << std::endl;

    /* Nature */
    std::cout << "  Nature: ";
    switch (message.eventListEntry.nature()) {
    case 'I':
        std::cout << "Info" << std::endl;
        break;
    case 'F':
        std::cout << "Forecast" << std::endl;
        break;
    case 'S':
        std::cout << "Silent" << std::endl;
        break;
    default:
        std::cout << message.eventListEntry.nature() << std::endl;
    }

    /* Quantifier */
    std::cout << "  Quantifier: " << static_cast<uint16_t>(message.eventListEntry.quantifier()) << std::endl;

    /* Duration Type */
    std::cout << "  Duration Type: ";
    switch (message.eventListEntry.durationType()) {
    case 'D':
        std::cout << "Dynamic" << std::endl;
        break;
    case 'L':
        std::cout << "Longer-Lasting" << std::endl;
        break;
    default:
        std::cout << message.eventListEntry.durationType() << std::endl;
    }

    /* Directionality */
    std::cout << "  Directionality: ";
    switch (message.eventListEntry.directionality()) {
    case 1:
        std::cout << "one direction" << std::endl;
        break;
    case 2:
        std::cout << "both directions" << std::endl;
        break;
    default:
        std::cout << static_cast<uint16_t>(message.eventListEntry.directionality()) << std::endl;
        break;
    }

    /* Urgency */
    std::cout << "  Urgency: ";
    switch (message.eventListEntry.urgency()) {
    case 'N':
        std::cout << "Normal urgency" << std::endl;
        break;
    case 'U':
        std::cout << "Urgent" << std::endl;
        break;
    case 'X':
        std::cout << "Extremly urgent" << std::endl;
        break;
    default:
        std::cout << message.eventListEntry.urgency() << std::endl;
    }

    /* Update Class */
    std::cout << "  UpdateClass: " << static_cast<uint16_t>(message.eventListEntry.updateClass()) << std::endl;

    /* Phrase Codes */
    std::cout << "  PhraseCodes: " << message.eventListEntry.phraseCodes() << std::endl;
}

/**
 * Returns the second location the extent points to
 *
 * @param[in] message Message
 * @return Location Code at extent
 */
uint16_t secondLocation(ISO14819::Message & message)
{
    uint16_t countryId = ISO14819::locationCodeLists().countryId(message.locationTableExtendedCountryCode(), message.locationTableCountryCode());
    uint16_t location = message.location();
    for (uint8_t i = 0; i < message.extent(); i++) {
        ISO14819::LocationCodeList::OffsetUnion allOffset = ISO14819::locationCodeLists().locationCodeList(countryId, message.locationTableNumber()).offsetUnion(
                    countryId,
                    message.locationTableNumber(),
                    location);
        location = message.direction() ? allOffset.negativeOffset : allOffset.positiveOffset;
    }
    return location;
}

/**
 * Get situation type:
 *   - 0: Error
 *   - 1: Area message
 *   - 2: Road message
 *   - 3: One segment
 *   - 4: Two segments
 *   - 5: One point in a city
 *   - 6: One point outside a city
 *   - 7: Two points in a city
 *   - 8: Two points in different cities
 *   - 9: Two points, one in a city
 *   - 10: Two points outside cities
 *
 * @param[in] location1 First location
 * @param[in] location2 Second location
 * @return situation code
 */
uint8_t situationType(ISO14819::LocationCodeList::LocationUnion & location1, ISO14819::LocationCodeList::LocationUnion & location2)
{
    uint8_t situation = 0;

    if ((location1.origin() == 'A') || (location1.origin() == 'O')) {
        situation = 1;
    } else if (location1.origin() == 'R') {
        situation = 2;
    } else if (location1.origin() == 'S') {
        if (location1.locationCode == location2.locationCode) { // extent = 0
            situation = 3;
        } else {
            situation = 4;
        }
    } else if (location1.origin() == 'P') {
        if (location1.locationCode == location2.locationCode) { // extent = 0
            if (location1.urban) {
                situation = 5;
            } else {
                situation = 6;
            }
        } else {
            if ((location1.urban) && (location2.urban)) {
                if (location1.adminAreaReference == location2.adminAreaReference) { // @todo check for different cities correct?
                    situation = 7;
                } else {
                    situation = 8;
                }
            } else if ((location1.urban) || (location2.urban)) {
                situation = 9;
            } else {
                situation = 10;
            }
        }
    };

    return situation;
}

/**
 * Print information on location
 *
 * @param[in] message Message
 */
static void printLocation(ISO14819::Message & message)
{
    /* loc = 0: reserved */
    if (message.location() == 0) {
        std::cout << "  reserved" << std::endl;
        return;
    }

    /* loc = 65533: '(message) for all users' */
    if (message.location() == 65533) {
        std::cout << "  (message) for all users" << std::endl;
    }

    /* loc = 65534: 'silent' location code */
    if (message.location() == 65534) {
        std::cout << "  'silent' location code)" << std::endl;
        return;
    }

    /* loc = 65535: updating or cancelling of messages */
    if (message.location() == 65535) {
        std::cout << "  updating or cancelling of messages" << std::endl;
        return;
    }

    /* loc = 63488..64511: special use */
    if ((message.location() >= 63488) && (message.location() <= 64511)) {
        std::cout << "  special use" << std::endl;
        return;
    }

    /* loc = 64512 (0xFC00)..65532: INTER-ROAD (foreign location table) */
    if ((message.location() >= 64512) && (message.location() <= 65532)) {
        /*
         * This is an incomplete INTER-ROAD message.
         * Normally it's processed already in TrafficMessageChannel::decodeMultiSubsequent.
         * But if the second part of the message is missing, it arrives here.
         */
        assert(false);
        return;
    }

    /* loc = 1..63487: regular locations */

    /* Location Code List */
    uint16_t countryId = ISO14819::locationCodeLists().countryId(message.locationTableExtendedCountryCode(), message.locationTableCountryCode());
    ISO14819::LocationCodeList lcl = ISO14819::locationCodeLists().locationCodeList(countryId, message.locationTableNumber());

    /* Location 1 */
    ISO14819::LocationCodeList::LocationUnion location1 = lcl.locationUnion(
                countryId,
                message.locationTableNumber(),
                message.location());

    /* Location2 (= Location1 +/- extent) */
    ISO14819::LocationCodeList::LocationUnion location2 = lcl.locationUnion(
                countryId,
                message.locationTableNumber(),
                secondLocation(message));

    /* print situation */
    if (message.extent() == 0) {
        std::cout
                << "  Im "
                << lcl.type(location1.typeClass, location1.typeCode).nationalTypeDescription
                << " "
                << lcl.name(location1.countryId, languageId, location1.name1).name
                << std::endl;
    } else {
        assert(location1.adminAreaReference);
        ISO14819::LocationCodeList::LocationUnion location1pol = lcl.locationUnion(
                    location1.countryId,
                    location1.tableCode,
                    location1.adminAreaReference);
        assert(location1pol.locationCode);
        assert(location1.segmentReference);
        ISO14819::LocationCodeList::LocationUnion location1seg = lcl.locationUnion(
                    location1.countryId,
                    location1.tableCode,
                    location1.segmentReference);
        assert(location1seg.locationCode);
        assert(location2.adminAreaReference);
        ISO14819::LocationCodeList::LocationUnion location2pol = lcl.locationUnion(
                    location1.countryId,
                    location1.tableCode,
                    location2.adminAreaReference);
        assert(location2pol.locationCode);
        assert(location2.segmentReference);
        ISO14819::LocationCodeList::LocationUnion location2seg = lcl.locationUnion(
                    location1.countryId,
                    location1.tableCode,
                    location2.segmentReference);
        assert(location2seg.locationCode);
        if (message.direction()) {
            /* negative direction */
            // SEG/POL
            std::cout
                    << "  "
                    << location1seg.number
                    << " "
                    << lcl.name(location1.countryId, languageId, location2seg.name1).name
                    << " Richtung "
                    << lcl.name(location1.countryId, languageId, location1seg.name2).name
                    << " zwischen "
                    << lcl.name(location1.countryId, languageId, location2pol.name1).name
                    << " und "
                    << lcl.name(location1.countryId, languageId, location1pol.name1).name
                    << std::endl;
            // SEG/LOC
            std::cout
                    << "  "
                    << location1seg.number
                    << " "
                    << lcl.name(location1.countryId, languageId, location1seg.name1).name
                    << " Richtung "
                    << lcl.name(location1.countryId, languageId, location1seg.name2).name
                    << " zwischen "
                    << lcl.name(location1.countryId, languageId, location2.name1).name
                    << " und "
                    << lcl.name(location1.countryId, languageId, location1.name1).name
                    << std::endl;
        } else {
            /* positive direction */
            // SEG/POL
            std::cout
                    << "  "
                    << location1seg.number
                    << " "
                    << lcl.name(location1.countryId, languageId, location1seg.name2).name
                    << " Richtung "
                    << lcl.name(location1.countryId, languageId, location2seg.name1).name
                    << " zwischen "
                    << lcl.name(location1.countryId, languageId, location2pol.name1).name
                    << " und "
                    << lcl.name(location1.countryId, languageId, location1pol.name1).name
                    << std::endl;
            // POL/LOC
            std::cout
                    << "  "
                    << location1seg.number
                    << " "
                    << lcl.name(location1.countryId, languageId, location1seg.name2).name
                    << " Richtung "
                    << lcl.name(location1.countryId, languageId, location1seg.name1).name
                    << " zwischen "
                    << lcl.name(location1.countryId, languageId, location2.name1).name
                    << " und "
                    << lcl.name(location1.countryId, languageId, location1.name1).name
                    << std::endl;
        }
    }
}

/**
 * Print duration and persistence.
 *
 * @param[in] message Message
 */
static void printDurationPersistence(ISO14819::Message & message)
{
    std::cout << "Duration/Persistence:" << std::endl;
    char nature = static_cast<char>(message.eventListEntry.nature());
    char durationType = static_cast<char>(message.eventListEntry.durationType());
    uint8_t durationPersistence = message.durationPersistence();

    /* Duration */
    std::cout << "  Duration: ";
    if ((durationType == 'D') && (nature == 'I')) {
        std::cout << "The situation is expected to continue ";
        static const std::array<std::string, 8> durationStringInfoDynamic = {{
                "(no explicit duration to be given)",       /* do not decrement */
                "for at least the next 15 minutes",         /* do not decrement */
                "for at least the next 30 minutes",         /* decrement after 15 minutes */
                "for at least the next 1 hour",             /* decrement after 30 minutes */
                "for at least the next 2 hours",            /* decrement after 1 hour */
                "for at least the next 3 hours",            /* decrement after 1 hour */
                "for at least the next 4 hours",            /* decrement after 1 hour */
                "for the rest of the day"
            }
        };                /* do not decrement */
        std::cout << durationStringInfoDynamic[durationPersistence];
    } else if ((durationType == 'D') && (nature == 'F')) {
        std::cout << "The situation is expected ";
        static const std::array<std::string, 8> durationStringForecastDynamic = {{
                "(no explicit start-time to be given)",     /* do not decrement */
                "within the next 15 minutes",               /* do not decrement */
                "within the next 30 minutes",               /* decrement after 15 minutes */
                "within the next 1 hour",                   /* decrement after 30 minutes */
                "within the next 2 hours",                  /* decrement after 1 hour */
                "within the next 3 hours",                  /* decrement after 1 hour */
                "within the next 4 hours",                  /* decrement after 1 hour */
                "later today"
            }
        };                            /* do not decrement */
        std::cout << durationStringForecastDynamic[durationPersistence];
    } else if ((durationType == 'L') && (nature == 'I')) {
        std::cout << "The situation is expected to continue ";
        static const std::array<std::string, 8> durationStringInfoLongerLasting = {{
                "(no explicit duration to be given)",       /* do not decrement */
                "for the next few hours",                   /* do not decrement */
                "for the rest of the day",                  /* do not decrement */
                "until tomorrow evening",                   /* decrement at midnight */
                "for the rest of the week",                 /* decrement Friday midnight */
                "until the end of next week",               /* decrement Sunday midnight */
                "until the end of the month",               /* do not decrement */
                "for a long period"
            }
        };                      /* do not decrement */
        std::cout << durationStringInfoLongerLasting[durationPersistence];
    } else if ((durationType == 'L') && (nature == 'F')) {
        std::cout << "The situation is expected ";
        static const std::array<std::string, 8> durationStringForecastLongerLasting = {{
                "(no explicit time horizon given)",         /* do not decrement */
                "within the next few hours",                /* do not decrement */
                "later today",                              /* do not decrement */
                "tomorrow",                                 /* decrement at midnight */
                "the day after tomorrow",                   /* decrement at midnight */
                "this weekend",                             /* do not decrement */
                "later this week",                          /* do not decrement */
                "next week"
            }
        };                              /* do not decrement */
        std::cout << durationStringForecastLongerLasting[durationPersistence];
    }
    std::cout << std::endl;

    /* Persistence */
    std::cout << "  Persistence Period: ";
    if (durationType == 'D') {
        static const std::array<std::string, 8> persistanceStringDynamic = {{
                "15 minutes (no message to end-user)",
                "15 minutes (with message to end-user)",
                "30 minutes",
                "1 hour",
                "2 hours",
                "3 hours",
                "4 hours",
                "until midnight on the day of message receipt"
            }
        };
        std::cout << persistanceStringDynamic[durationPersistence];
    } else if (durationType == 'L') {
        if ((nature == 'I') || (nature == 'S')) {
            static const std::array<std::string, 8> persistanceStringInfo = {{
                    "1 hour",
                    "2 hours",
                    "until midnight on the day of message receipt",
                    "until midnight on the day after message receipt",
                    "until midnight on the day after message receipt",
                    "until midnight on the day after message receipt",
                    "until midnight on the day after message receipt",
                    "until midnight on the day after message receipt"
                }
            };
            std::cout << persistanceStringInfo[durationPersistence];
        } else if (nature == 'F') {
            static const std::array<std::string, 8> persistanceStringForecast = {{
                    "1 hour",
                    "2 hours",
                    "until midnight on the day of message receipt",
                    "until midnight on the day after message receipt",
                    "until midnight on the day after message receipt",
                    "until midnight on the day after message receipt",
                    "until midnight on the day after message receipt",
                    "until midnight on the day after message receipt"
                }
            };
            std::cout << persistanceStringForecast[durationPersistence];
        }
    }
    std::cout << std::endl;
}

/**
 * Print diversion advice.
 *
 * @param[in] message Message
 */
static void printDiversionAdvice(ISO14819::Message & message)
{
    std::cout << "Diversion Advice: ";
    static const std::array<std::string, 2> diversionAdviceString = {{
            "(no diversion recommended)",
            "end-users are recommended to avoid the area if possible"
        }
    };

    std::cout << diversionAdviceString[message.diversionAdvice()]
              << std::endl;
}

/**
 * Print explicit start/stop time
 *
 * @param[in] time
 */
static void printStartStopTime(uint8_t time)
{
    if (time <= 95) {
        std::cout << "today at "
                  << std::setw(2) << std::dec << (time * 15) / 60 << ":"
                  << std::setw(2) << std::dec << (time * 15) % 60;
    } else if ((time >= 96) && (time <= 200)) {
        std::cout << "in "
                  << (time - 96) / 24 << " days at "
                  << std::setw(2) << std::dec << (time - 96) % 24 << ":00";
    } else if ((time >= 201) && (time <= 231)) {
        std::cout << "in "
                  << time - 200 << " days";
    } else { // 232..255
        std::cout
                << ((time - 231) % 2 ? "mid of" : "end of")
                << " " << (time - 230) / 2 << ". month";
    }
    std::cout << std::endl;
}

/**
 * Return price string.
 *
 * @param[in] tel telephone number
 * @return price string
 */
std::string priceStr(ISO14819::TelephoneNumber & tel)
{
    /* Cost */
    double costWithMulti = tel.cost();
    switch (tel.decimalMultiplier()) {
    case 0:
        costWithMulti /= 1;
        break;
    case 1:
        costWithMulti /= 10;
        break;
    case 2:
        costWithMulti /= 100;
        break;
    case 3:
        costWithMulti /= 1000;
        break;
    }

    /* Currency */
    std::string currency = ISO4217::currencies().at(tel.currencyReference()).unicode;

    /* print */
    std::stringstream ss;
    if (tel.currencyInFront() == 1) {
        ss << currency;
    }
    ss << std::dec << std::fixed << std::setprecision(tel.decimalMultiplier()) << costWithMulti;
    if (tel.currencyInFront() == 0) {
        ss << currency;
    }

    return ss.str();
}

/**
 * Return time unit string.
 *
 * @param[in] tel telephone number
 * @return time unit string
 */
std::string timeUnitStr(ISO14819::TelephoneNumber & tel)
{
    /* Time */
    static const std::map<uint8_t, std::string> callCostTimePeriod = {
        { 0, "(Free)" },
        { 1, "per sec" },
        { 2, "per minute" },
        { 3, "per hour" },
        { 4, "per call" },
        { 5, "per day" },
        { 6, "(Variable costs)" },
        { 7, "(Undefined)" },
    };

    return callCostTimePeriod.at(tel.timeUnit());
}

/**
 * Print optional message content.
 *
 * @param[in] message message
 */
static void printOptionalMessageContent(ISO14819::Message & message)
{
    std::cout << "Optional Message Content:" << std::endl;
    for (auto & omc : message.optionalMessageContent()) {
        switch (omc.label()) {
        case 0:
            /* Duration (3 bit) */
            std::cout << "  0 (Duration): "
                      << omc.data() << std::endl;
            break;
        case 1:
            /* Control code (3 bit) */
            std::cout << "  1 (Control code): "
                      << omc.data() << std::endl;
            break;
        case 2:
            /* Length of route affected (5 bit) */
            std::cout << "  2 (Length of route affected): ";
            if (omc.data() == 0) {
                std::cout << "Problem extends for more than 100 km";
            } else if ((omc.data() >= 1) && (omc.data() <= 10)) {
                std::cout << "Length of problem is " << std::dec << omc.data() << " km";
            } else if ((omc.data() >= 11) && (omc.data() <= 15)) {
                std::cout << "Length of problem is " << std::dec << (12 + (omc.data() - 11) * 2) << " km";
            } else if ((omc.data() >= 16) && (omc.data() <= 31)) {
                std::cout << "Length of problem is " << std::dec << (25 + (omc.data() - 16) * 5) << " km";
            }
            std::cout << std::endl;
            break;
        case 3:
            /* Speed limit advice (5 bit) */
            std::cout << "  3 (Speed limit advice): "
                      << "Maximum speed is " << std::dec << omc.data() << " km/h" << std::endl;
            break;
        case 4:
            /* Quantifier (5 bit) */
            std::cout << "  4 (Quantifier): "
                      << omc.data() << std::endl;
            break;
        case 5:
            /* Quantifier (8 bit) */
            std::cout << "  5 (Quantifier): "
                      << omc.data() << std::endl;
            break;
        case 6:
            /* Supplementary information code (8 bit) */
            std::cout << "  6 (Supplementary information code): "
                      << omc.data() << std::endl;
            break;
        case 7:
            /* Explicit start time (8 bit) */
            std::cout << "  7 (Explicit start time): ";
            printStartStopTime(static_cast<uint8_t>(omc.data()));
            break;
        case 8:
            /* Explicit stop time (8 bit) */
            std::cout << "  8 (Explicit stop time): ";
            printStartStopTime(static_cast<uint8_t>(omc.data()));
            break;
        case 9:
            /* Additional event (11 bit) */
            std::cout << "  9 (Additional event): "
                      << omc.data() << std::endl;
            break;
        case 10:
            /* Detailed diversion advice (16 bit) */
            std::cout << "  10 (Detailed diversion advice): "
                      << omc.data() << std::endl;
            break;
        case 11:
            /* Destination (16 bit) */
            std::cout << "  11 (Destination): "
                      << omc.data() << std::endl;
            break;
        case 12:
            /* Precise location reference (16 bit) */
            std::cout << "  12 (Precise location reference): "
                      << omc.data() << std::endl;
            break;
        case 13:
            /* Cross linkage to source of problem, on another route (16 bit) */
            std::cout << "  13 (Cross linkage to source of problem, on another route): "
                      << omc.data() << std::endl;
            break;
        case 14:
            /* Separator (0 bit) */
            std::cout << "  14 (Separator)" << std::endl;
            break;
        case 15:
            /* Other information as defined by sub-labels (6 bit) */
            std::cout << "  15 (Other information as defined by sub-labels): "
                      << omc.data() << std::endl;
            break;
        }
    }
}

/**
 * Print the system information.
 *
 * @param[in] systemInformation System Information
 */
static void printSystemInformation(ISO14819::SystemInformation & systemInformation)
{
    /* System Information */
    std::cout << "System Information:" << std::endl;
    std::cout << "  Country Code: 0x" << std::hex << static_cast<uint16_t>(systemInformation.countryCode()) << std::endl;
    std::cout << "  Extended Country Code: 0x" << std::hex << static_cast<uint16_t>(systemInformation.extendedCountryCode()) << std::endl;
    std::cout << "  Location Table Number: " << std::dec << static_cast<uint16_t>(systemInformation.locationTableNumber()) << std::endl;
    std::cout << "  Service Identifier: " << std::dec << static_cast<uint16_t>(systemInformation.serviceId()) << std::endl;
    std::cout << "  Location Table Country Code: 0x" << std::hex << static_cast<uint16_t>(systemInformation.locationTableCountryCode()) << std::endl;
    std::cout << "  Location Table Extended Country Code: 0x" << std::hex << static_cast<uint16_t>(systemInformation.locationTableExtendedCountryCode()) << std::endl;
}

/**
 * Print TMC message
 *
 * @param[in] message Message
 */
static void printMessage(ISO14819::Message & message)
{
    /* Location */
    std::cout << "Location:"
              << " LTECC=0x" << std::hex << static_cast<uint16_t>(message.locationTableExtendedCountryCode())
              << " LTCC=0x" << std::hex << static_cast<uint16_t>(message.locationTableCountryCode())
              << " LTN=" << std::dec << static_cast<uint16_t>(message.locationTableNumber())
              << " LCD=" << std::dec << message.location()
              << std::endl;
    printLocation(message);
    std::cout << "Direction: " << std::dec << static_cast<uint16_t>(message.direction()) << std::endl;
    std::cout << "Extent: " << std::dec << static_cast<uint16_t>(message.extent()) << std::endl;

    /* Event */
    printEvent(message);
    std::cout << "    FullPhrase: " << ISO14819::eventList().fullPhrase(message.event(), message.eventListEntry.quantifier()) << std::endl;

    /* Duration / Persistence */
    printDurationPersistence(message);

    /* Diversion Advice */
    printDiversionAdvice(message);

    /* Optional Message Content */
    printOptionalMessageContent(message);

    /* System Information */
    printSystemInformation(message.systemInformation);

    /* Times */
    std::cout << "Times:" << std::endl;
    std::cout << "  Receive Time: ";
    printTime(message.receiveTime);
    if (message.persistenceTime.valid()) {
        std::cout << "  Expiry Time: ";
        printTime(message.persistenceTime);
    }
    if (message.startTime.valid()) {
        std::cout << "  Start Time: ";
        printTime(message.startTime);
    }
    if (message.stopTime.valid()) {
        std::cout << "  Stop Time: ";
        printTime(message.stopTime);
    }
    if (message.decrementTime.valid()) {
        std::cout << "  Decrement Time: ";
        printTime(message.decrementTime);
    }
}

static void printMessageInsertion(ISO14819::Message message)
{
    /* new message */
    std::cout << std::endl;
    std::cout << std::endl;
    std::cout << "<message inserted:>" << std::endl;
    printMessage(message);
}

static void printMessageUpdation(ISO14819::Message message)
{
    /* message updated (by timer or by reception) */
    std::cout << std::endl;
    std::cout << std::endl;
    std::cout << "<message updated:>" << std::endl;
    printMessage(message);
}

static void printMessageDeletion(ISO14819::Message message)
{
    /* problem cleared (by cancellation message) */
    std::cout << std::endl;
    std::cout << std::endl;
    std::cout << "<message deleted:>" << std::endl;
    printMessage(message);
}

void clockTimePrint(uint16_t pi)
{
    ISO62106::RdsProgram & program = ISO62106::rdsProgram(pi);
    ISO62106::RdsTime time = program.clockTime.value();
    std::cout << std::endl
              << "Clock/Time Received: ";
    printTime(time);
}

int main(int argc, char * argv[])
{
    /* evaluate options */
    int opt;
    int filter = 0;
    int usage  = 0;
    uint8_t defaultEcc = 0;
    std::string language = "en_CEN";
    while ((opt = getopt(argc, argv, "e:f:l:")) != -1) {
        switch (opt) {
        case 'e':
            defaultEcc = static_cast<uint8_t>(strtoul(optarg, nullptr, 16));
            break;
        case 'f':
            if (strcmp(optarg, "v4l") == 0) {
                filter = 0;
            } else if (strcmp(optarg, "raw") == 0) {
                filter = 1;
            } else if (strcmp(optarg, "csv") == 0) {
                filter = 2;
            } else if (strcmp(optarg, "smp") == 0) {
                filter = 3;
            } else {
                filter = -1;
            }
            break;
        case 'l':
            language = optarg;
            break;
        default: /* '?' */
            usage = 1;
        }
    }

    /* checks */
    if ((filter == -1) || ((argc - optind) != 1)) {
        usage = 1;
    }
    if (usage == 1) {
        std::cout << "Usage: " << argv[0] << " [-e default ecc in hex] [-f filter] [-l language] filename" << std::endl;
        return EXIT_FAILURE;
    }

    /* set language for event list */
    ISO14819::eventList().setLanguage(language);
    std::cout << "Language set to " << ISO14819::eventList().language() << std::endl;

    /* set language for location list */
    languageId = 1; // @todo German Language in CID=58 (Germany)

    /* set callbacks */
    RdsFile rdsFile;
    ISO14819::TrafficMessageChannel::registerHandler();
    ISO62106::ClockTime::onChange.connect(sigc::ptr_fun(clockTimePrint));
    ISO14819::MessageMemory::onMessageInsertion.connect(sigc::ptr_fun(printMessageInsertion));
    ISO14819::MessageMemory::onMessageUpdation.connect(sigc::ptr_fun(printMessageUpdation));
    ISO14819::MessageMemory::onMessageDeletion.connect(sigc::ptr_fun(printMessageDeletion));
    std::cout << "Waiting for Clock/Time message..." << std::endl;

    /* get time */
    time_t rawtime;
    (void) time(&rawtime);
    struct tm * tm = gmtime(&rawtime);
    ISO62106::RdsTime rdsTime;
    rdsTime.setDate(static_cast<uint16_t>(tm->tm_year), static_cast<uint8_t>(tm->tm_mon + 1), static_cast<uint8_t>(tm->tm_mday));
    rdsTime.setTime(static_cast<uint8_t>(tm->tm_hour), static_cast<uint8_t>(tm->tm_min));
    // @todo rdsTime.setSignLocalTimeOffset(false); rdsTime.setLocalTimeOffset(0);

    /* set clock */
    ISO62106::RdsClock rdsClock(rdsTime);
    ISO62106::ClockTime::onChange.connect(sigc::mem_fun(rdsClock, &ISO62106::RdsClock::ctUpdated));
    rdsClock.onMinuteChange.connect(sigc::mem_fun(ISO14819::messageMemory(), &ISO14819::MessageMemory::updateMessages));
    rdsClock.startThread();

    /* open file */
    std::ifstream ifs;
    bool inputFromCin = (strcmp(argv[optind], "-") == 0);
    if (!inputFromCin) {
        ifs.open(argv[optind]);
        if (!ifs.is_open()) {
            std::cout << "Unable to open file " << argv[optind] << std::endl;
            return EXIT_FAILURE;
        }
    }

    /* read in stream */
    bool decodeSuccess = false;
    do {
        /* set default ECC */
        if (defaultEcc && rdsFile.currentProgram()) {
            if (!rdsFile.currentProgram()->extendedCountryCode.value()) {
                rdsFile.currentProgram()->extendedCountryCode.decode(defaultEcc);
            }
        }

        /* apply filter */
        switch (filter) {
        case 0:
            decodeSuccess = rdsFile.decodeV4l(inputFromCin ? std::cin : ifs);
            break;
        case 1:
            decodeSuccess = rdsFile.decodeRaw(inputFromCin ? std::cin : ifs);
            break;
        case 2:
            decodeSuccess = rdsFile.decodeCsv(inputFromCin ? std::cin : ifs);
            break;
        case 3:
            decodeSuccess = rdsFile.decodeSmp(inputFromCin ? std::cin : ifs);
            break;
        }
    } while (decodeSuccess);

    /* close file */
    if (inputFromCin) {
        ifs.close();
    }

    /* close file */
    ifs.close();

    return EXIT_SUCCESS;
}
