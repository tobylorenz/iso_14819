/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include <fstream>

#include <ISO/62106.h>

#include "iso_14819_export.h"

class ISO_14819_EXPORT RdsFile : public ISO62106::RdsReceiver
{
public:
    /**
     * Decode one RDS block from input stream.
     *
     * format: V4L format
     *
     * @param[in] is input stream
     * @return True on success, false otherwise
     */
    bool decodeV4l(std::istream & is);

    /**
     * Decode one RDS block from input stream.
     *
     * format: 13-byte pattern (raw unsynchronized RDS bitstream, including CRC bits, with no particular alignment)
     *
     * @param[in] is input stream
     * @return True on success, false otherwise
     */
    bool decodeRaw(std::istream & is);

    /**
     * Decode one RDS block from input stream.
     *
     * format: csv
     *
     * @param[in] is input stream
     * @return True on success, false otherwise
     */
    bool decodeCsv(std::istream & is);

    /**
     * Decode one RDS block from input stream.
     *
     * format: smp
     *
     * @param[in] is input stream
     * @return True on success, false otherwise
     */
    bool decodeSmp(std::istream & is);
};
