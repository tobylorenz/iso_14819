/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "RdsGroup.h"

#include <cassert>

/* offset words (9 bit) */
static constexpr uint16_t offsetA = 0x0FC;
static constexpr uint16_t offsetB = 0x198;
static constexpr uint16_t offsetC = 0x168;
static constexpr uint16_t offsetC_ = 0x350; // not used any more?
static constexpr uint16_t offsetD = 0x1B4;
// static constexpr uint16_t offsetE = 0x000; // not used any more, was for MMBS

/* syndromes (9 bit) */
// static constexpr uint16_t syndomeA = 0x3D8;
// static constexpr uint16_t syndomeB = 0x3D4;
// static constexpr uint16_t syndomeC = 0x25C;
// static constexpr uint16_t syndomeC_ = 0x3CC;
// static constexpr uint16_t syndomeD = 0x258;

RdsGroup::RdsGroup() :
    blk1(0), crc1(0), crc1Valid(false),
    blk2(0), crc2(0), crc2Valid(false),
    blk3(0), crc3(0), crc3Valid(false),
    blk4(0), crc4(0), crc4Valid(false),
    validBlks(0),
    bitOffset(0), bitOffsetContinuous(0)
{
    /* self test */
    assert((calcRdsCrc(0xd4f1) ^ offsetA) == 0x28f);
    assert((calcRdsCrc(0xe40c) ^ offsetB) == 0x389);
    assert((calcRdsCrc(0x0000) ^ offsetC) == 0x168);
    assert((calcRdsCrc(0xd3c3) ^ offsetD) == 0x3d9);
    assert((calcRdsCrc(0xd4f1) ^ offsetA) == 0x28f);
    assert((calcRdsCrc(0x0409) ^ offsetB) == 0x052);
    assert((calcRdsCrc(0xf32a) ^ offsetC) == 0x08e);
    assert((calcRdsCrc(0x5220) ^ offsetD) == 0x327);
    assert((calcRdsCrc(0xd4f1) ^ offsetA) == 0x28f);
    assert((calcRdsCrc(0x641e) ^ offsetB) == 0x333);
    assert((calcRdsCrc(0x0944) ^ offsetC) == 0x187);
    assert((calcRdsCrc(0xabaa) ^ offsetD) == 0x289);
    assert((calcRdsCrc(0xd4f1) ^ offsetA) == 0x28f);
    assert((calcRdsCrc(0x040a) ^ offsetB) == 0x299);
    assert((calcRdsCrc(0x2a07) ^ offsetC) == 0x1b0);
    assert((calcRdsCrc(0x5448) ^ offsetD) == 0x01c);
}

void RdsGroup::push(uint8_t data, uint8_t bit)
{
    /* block 1 */
    blk1 <<= bit;
    blk1 |= (crc1 >> (10 - bit));
    blk1 &= 0xffff;
    crc1 <<= bit;
    crc1 |= (blk2 >> (16 - bit));
    crc1 &= 0x3ff;
    crc1Valid = ((calcRdsCrc(blk1) ^ offsetA) == crc1);

    /* block 2 */
    blk2 <<= bit;
    blk2 |= (crc2 >> (10 - bit));
    blk2 &= 0xffff;
    crc2 <<= bit;
    crc2 |= (blk3 >> (16 - bit));
    crc2 &= 0x3ff;
    crc2Valid = ((calcRdsCrc(blk2) ^ offsetB) == crc2);

    /* block 3 */
    blk3 <<= bit;
    blk3 |= (crc3 >> (10 - bit));
    blk3 &= 0xffff;
    crc3 <<= bit;
    crc3 |= (blk4 >> (16 - bit));
    crc3 &= 0x3ff;
    crc3Valid =
        ((calcRdsCrc(blk3) ^ offsetC) == crc3) ||
        ((calcRdsCrc(blk3) ^ offsetC_) == crc3);

    /* block 4 */
    blk4 <<= bit;
    blk4 |= (crc4 >> (10 - bit));
    blk4 &= 0xffff;
    crc4 <<= bit;
    crc4 |= data;
    crc4 &= 0x3ff;
    crc4Valid = ((calcRdsCrc(blk4) ^ offsetD) == crc4);

    /* calculate number of valid blocks */
    validBlks =
        (crc1Valid ? 1 : 0) +
        (crc2Valid ? 1 : 0) +
        (crc3Valid ? 1 : 0) +
        (crc4Valid ? 1 : 0);

    /* calculate bit offset to reach synchronization */
    bitOffsetContinuous += bit;
    bitOffsetContinuous %= 104;
    if (validBlks) {
        bitOffset = bitOffsetContinuous;
    }
}

uint16_t RdsGroup::calcRdsCrc(uint16_t data)
{
    constexpr uint16_t polynom = 0x5B9; /* 10110111001 */
    uint16_t crc = 0;

    uint16_t tmp = data;
    for (uint8_t bit = 26; bit > 0; --bit) {
        crc <<= 1;
        crc |= tmp >> 15;
        if (crc & (1 << 10)) {
            crc ^= polynom;
        }
        tmp <<= 1;
    }

    return crc & 0x3ff;
}
