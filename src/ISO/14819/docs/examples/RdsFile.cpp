/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "RdsFile.h"

#include <cstring>
#include <iomanip>
#include <iostream>
#include <sstream>

#include <ISO/62106.h>

#include "RdsGroup.h"

bool RdsFile::decodeV4l(std::istream & is)
{
    uint8_t buf[3]; /* v4l data packet */
    static uint16_t rds[4]; /* rds block */
    uint8_t rdsIdx; /* rds block index */
    static uint8_t nextRdsIdx = 0; /* next rds block index */

    /* buf[0]: Least Significant Byte of RDS Block */
    /* buf[1]: Most Significant Byte of RDS Block */
    /* buf[2]: */
    // Bit 7:    Error bit. Indicates that an uncorrectable error occurred during reception of this block.
    // Bit 6:    Corrected bit. Indicates that an error was corrected for this data block.
    // Bits 5-3:    Received Offset. Indicates the offset received by the sync system.
    // Bits 2-0:    Offset Name. Indicates the offset applied to this data.
    /** @todo Make input (incl. rds quality settings) and output somehow configurable */

    /* clear RDS buffer */
    memset(&buf, 0, sizeof(buf));

    /* read RDS buffer */
    is.read(reinterpret_cast<char *>(&buf), sizeof(buf));
    if (is.gcount() < sizeof(buf)) {
        nextRdsIdx = 0; // reset index
        return false;
    }

    /* no processing on error or corrected blocks */
    if ((buf[2] >> 6) != 0) {
        nextRdsIdx = 0;    // reset index
        return true;
    }

    /* check index */
    rdsIdx = buf[2] & 7; /* offset name */
    /* rds_idx = (buf[2] >> 3) & 7; */ /* received offset */
    if (nextRdsIdx == rdsIdx) {
        rds[rdsIdx] = (buf[1] << 8) | buf[0];
        if (rdsIdx == 3) {
            decode(rds[0], rds[1], rds[2], rds[3]);
            nextRdsIdx = 0; // reset index
        } else {
            nextRdsIdx++;
        }
    } else {
        nextRdsIdx = 0; // reset index
        return true;
    }

    return true;
}

bool RdsFile::decodeRaw(std::istream & is)
{
    /* read RDS buffer */
    static RdsGroup rdsGroup;

    /* push bit-wise into buffer */
    uint8_t byte;
    is.read(reinterpret_cast<char *>(&byte), sizeof(byte));
    if (is.gcount() < sizeof(byte)) {
        return false;
    }
    for (int bitPos = 8; bitPos-- > 0; ) {
        uint8_t bit = (byte >> bitPos) & 1;
        rdsGroup.push(bit, 1);

        /* check validity */
        if (rdsGroup.validBlks == 4) {
            decode(
                rdsGroup.blk1,
                rdsGroup.blk2,
                rdsGroup.blk3,
                rdsGroup.blk4);
        }
    }

    return true;
}

bool RdsFile::decodeCsv(std::istream & is)
{
    static uint16_t rds[4]; /* rds block (4x 16 bits) */

    std::string str;
    getline(is, str);
    if (!is.good()) {
        return false;
    }

    std::istringstream iss(str);
    char c;
    iss >> rds[0];
    iss >> c;
    iss >> rds[1];
    iss >> c;
    iss >> rds[2];
    iss >> c;
    iss >> rds[3];
    decode(rds[0], rds[1], rds[2], rds[3]);

    return true;
}

bool RdsFile::decodeSmp(std::istream & is)
{
    static uint8_t buf[16]; /* 16-byte data packet */
    static uint16_t rds[4]; /* rds block (4x 16 bits) */

    /* clear RDS buffer */
    memset(&buf, 0, sizeof(buf));

    /* read RDS buffer */
    is.read((char *) &buf, sizeof(buf));
    if (is.gcount() != sizeof(buf)) {
        return EXIT_FAILURE;
    }

    rds[0] = (buf[ 2] << 8) | buf[ 3];
    rds[1] = (buf[ 6] << 8) | buf[ 7];
    rds[2] = (buf[10] << 8) | buf[11];
    rds[3] = (buf[14] << 8) | buf[15];

    decode(rds[0], rds[1], rds[2], rds[3]);

    return true;
}
