/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include <unistd.h>

#include <iostream>
#include <string>
#include <set>
#include <vector>

#include <ISO/14819.h>
#include <sigc++-2.0/sigc++/signal.h>

/* constants */
static const uint16_t countryId = 58; // Germany
static const uint8_t tableCode = 1; // Germany
static const uint8_t languageId = 1; // German

/* graphviz elements */
static std::set<uint16_t> nodesSeen;
static std::set<uint16_t> nodesToBeSeen;
static std::vector<std::string> nodes;
static std::vector<std::string> edges;
static uint16_t locationCode1; // primary location
static uint16_t locationCode2; // secondary location = primary location + extent in direction

/**
 * Add information about Location Code
 *
 * @param[in] locationCodeList Location Code List
 * @param[in] locationCode Location Code (LCD)
 */
static void addLocationCode(ISO14819::LocationCodeList & locationCodeList, uint16_t locationCode)
{
    /* null location */
    if (!locationCode) {
        return;
    }

    /* check if seen already */
    if (nodesSeen.count(locationCode)) {
        return;
    }
    nodesSeen.insert(locationCode);

    /* load location */
    ISO14819::LocationCodeList::LocationUnion location = locationCodeList.locationUnion(countryId, tableCode, locationCode);
    ISO14819::LocationCodeList::TypeUnion type = locationCodeList.typeUnion(location.typeClass, location.typeCode, location.subtypeCode);
    if (!location.locationCode) {
        std::cerr << "Location " << locationCode << " not found!" << std::endl;
        return;
    }

    /* add node */
    std::string s;
    s = "LCD_" + std::to_string(locationCode) + " [ label = ";
    s += "\"LCD=" + std::to_string(locationCode);
    s += "\\nTYPE=";
    s += type.typeClass;
    s += std::to_string(type.typeCode) + "." + std::to_string(type.subtypeCode) + " (" + type.nationalDescription + ")";
    if (!location.number.empty()) {
        s += "\\nNUMBER=" + location.number;
    }
    if (location.roadName) {
        ISO14819::LocationCodeList::Name roadName = locationCodeList.name(countryId, languageId, location.roadName);
        s += "\\nRNID=" + roadName.name;
    }
    if (location.name1) {
        ISO14819::LocationCodeList::Name name1 = locationCodeList.name(countryId, languageId, location.name1);
        s += "\\nN1ID=" + name1.name;
    }
    if (location.name2) {
        ISO14819::LocationCodeList::Name name2 = locationCodeList.name(countryId, languageId, location.name2);
        s += "\\nN2ID=" + name2.name;
    }
    s += "\"";
    if (locationCode == locationCode1) {
        s += ", color = \"green\"";
    }
    if (locationCode == locationCode2) {
        s += ", color = \"red\"";
    }
    s += " ];";
    nodes.push_back(s);

    /* add edges */
    if (location.adminAreaReference) {
        edges.push_back("LCD_" + std::to_string(locationCode) + " -> LCD_" + std::to_string(location.adminAreaReference) + " [ label = \"POL_LCD\" ];");
        nodesToBeSeen.insert(location.adminAreaReference);
    }
    if (location.otherAreaReference) {
        edges.push_back("LCD_" + std::to_string(locationCode) + " -> LCD_" + std::to_string(location.otherAreaReference) + " [ label = \"OTH_LCD\" ];");
        nodesToBeSeen.insert(location.otherAreaReference);
    }
    if (location.roadReference) {
        edges.push_back("LCD_" + std::to_string(locationCode) + " -> LCD_" + std::to_string(location.roadReference) + " [ label = \"ROA_LCD\" ];");
        nodesToBeSeen.insert(location.roadReference);
    }
    if (location.segmentReference) {
        edges.push_back("LCD_" + std::to_string(locationCode) + " -> LCD_" + std::to_string(location.segmentReference) + " [ label = \"SEG_LCD\" ];");
        nodesToBeSeen.insert(location.segmentReference);
    }
}

/**
 * Add information about Location Code
 *
 * @param[in] locationCodeList Location Code List
 * @param[in] locationCode Location Code (LCD)
 * @return Location Code at Extent
 */
uint16_t addExtent(ISO14819::LocationCodeList & locationCodeList, uint16_t locationCode, uint8_t direction, uint8_t extent)
{
    /* null location */
    if (!locationCode) {
        return 0;
    }

    /* load location */
    ISO14819::LocationCodeList::LocationUnion location = locationCodeList.locationUnion(countryId, tableCode, locationCode);
    if (!location.locationCode) {
        std::cerr << "Location " << locationCode << " not found!" << std::endl;
        return 0;
    }

    /* location 2 */
    uint16_t locationCode2 = locationCode;
    for (uint8_t i = 0; i < extent; i++) {
        ISO14819::LocationCodeList::OffsetUnion allOffset = ISO14819::locationCodeLists().locationCodeList(countryId, tableCode).offsetUnion(countryId, tableCode, locationCode2);
        if (direction) {
            edges.push_back("LCD_" + std::to_string(locationCode2) + " -> LCD_" + std::to_string(allOffset.negativeOffset) + " [ label = \"NEG_OFF_LCD\" ];");
            locationCode2 = allOffset.negativeOffset;
        } else {
            edges.push_back("LCD_" + std::to_string(locationCode2) + " -> LCD_" + std::to_string(allOffset.positiveOffset) + " [ label = \"POS_OFF_LCD\" ];");
            locationCode2 = allOffset.positiveOffset;
        }
    }

    nodesToBeSeen.insert(locationCode2);
    return locationCode2;
}

int main(int argc, char * argv[])
{
    /* evaluate options */
    int opt;
    int usage  = 0;
    while ((opt = getopt(argc, argv, ":")) != -1) {
        switch (opt) {
        default: /* '?' */
            usage = 1;
        }
    }

    /* checks */
    if ((argc - optind) != 3) {
        usage = 1;
    }
    if (usage == 1) {
        std::cout << "Usage: " << argv[0] << " locationCode direction extent" << std::endl;
        return EXIT_FAILURE;
    }


    /* open location code list */
    ISO14819::LocationCodeList locationCodeList(countryId, tableCode);
    if (locationCodeList.locationDataSets().empty()) {
        std::cerr << "Unable to load Location Code List" << std::endl;
        return EXIT_FAILURE;
    }

    /* parse locations */
    locationCode1 = static_cast<uint16_t>(std::stoi(argv[optind]));
    uint8_t direction = static_cast<uint8_t>(std::stoi(argv[optind + 1]));
    uint8_t extent = static_cast<uint8_t>(std::stoi(argv[optind + 2]));
    nodesToBeSeen.insert(locationCode1);
    if (extent) {
        locationCode2 = addExtent(locationCodeList, locationCode1, direction, extent);
    }

    /* evaluate graph recursivly */
    while (nodesToBeSeen.size()) {
        uint16_t locationCode = *nodesToBeSeen.begin();
        addLocationCode(locationCodeList, locationCode);
        nodesToBeSeen.erase(locationCode);
    }

    /* open file to write */
    std::cout << "digraph lcl" << std::endl;
    std::cout << "{" << std::endl;
    for (std::string & s : nodes) {
        std::cout << "  " << s << std::endl;
    }
    for (std::string & s : edges) {
        std::cout << "  " << s << std::endl;
    }
    std::cout << "}" << std::endl;

    return EXIT_SUCCESS;
}
