/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include <array>

#include "iso_14819_export.h"
#include "platform.h"

namespace ISO14819 {

/** Encryption Keys */
class ISO_14819_EXPORT EncryptionKeys
{
public:
    explicit EncryptionKeys();

    /**
     * Get key table pre advised.
     *
     * @return Array of xor value, start bit and rotate left
     */
    std::array<uint8_t, 3> keyTablePreAdvised() const;

    /**
     * Set key table pre advised.
     *
     * @param[in] xorValue xor value
     * @param[in] startBit start bit
     * @param[in] rotateLeft rotate left
     */
    void setKeyTablePreAdvised(uint8_t xorValue, uint8_t startBit, uint8_t rotateLeft);

    /**
     * Get key table.
     *
     * @param[in] encryptionTableId Service key table ID (SID)
     * @param[in] encryptionId Encryption ID (ENCID)
     * @return Array of xor value, start bit and rotate left
     */
    std::array<uint8_t, 3> keyTable(uint8_t encryptionTableId, uint8_t encryptionId) const;

    /**
     * Set key table.
     *
     * @param[in] encryptionTableId Service key table ID (SID)
     * @param[in] encryptionId Encryption ID (ENCID)
     * @param[in] xorValue xor value
     * @param[in] startBit start bit
     * @param[in] rotateLeft rotate left
     */
    void setKeyTable(uint8_t encryptionTableId, uint8_t encryptionId, uint8_t xorValue, uint8_t startBit, uint8_t rotateLeft);

private:
    /** enc_test=1: encryption parameters pre-advised by the service provider (xor value, start bit, rotate left) */
    std::array<uint8_t, 3> m_keyTablePreAdvised;

    /** enc_test=3: 8 tables, each with 32 lines (addressed by ENCID), each with 3 decryption parameters (xor value, start bit, rotate left) */
    std::array<std::array<std::array<uint8_t, 3>, 32>, 8> m_keyTable;
};

/**
 * Return reference to EncryptionKeys.
 *
 * @return Reference to EncryptionKeys
 */
extern ISO_14819_EXPORT EncryptionKeys & encryptionKeys(); /* singleton */

}
