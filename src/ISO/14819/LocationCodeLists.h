/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include <map>

#include "LocationCodeList.h"

#include "iso_14819_export.h"
#include "platform.h"

namespace ISO14819 {

/**
 * @brief Location Code Lists
 *
 * The Location Code Lists manages the available Location Code Lists.
 */
class ISO_14819_EXPORT LocationCodeLists
{
public:
    explicit LocationCodeLists();

    /**
     * Get the Location Code List for given Country ID (CID) and Table Code (TABCD).
     *
     * @param[in] countryId Country ID (CID)
     * @param[in] tableCode Table Code (TABCD)
     * @return Location Code List
     */
    LocationCodeList & locationCodeList(uint16_t countryId, uint8_t tableCode);

    /**
     * Determines the Coutry ID (CID) from the ECC and CC.
     *
     * @param[in] extendedCountryCode Extended Country Code (ECC)
     * @param[in] countryCode Country Code (CC)
     * @return Country ID (CID)
     */
    uint16_t countryId(uint8_t extendedCountryCode, uint8_t countryCode) const;

    /**
     * Get the Location Code List for given ECC, CC and LTN/TABCD.
     *
     * @param[in] extendedCountryCode Extended Country Code (ECC)
     * @param[in] countryCode Country Code (CC)
     * @param[in] tableCode Table Code (TABCD)
     * @return Location Code List
     */
    LocationCodeList & locationCodeList(uint8_t extendedCountryCode, uint8_t countryCode, uint8_t tableCode);

private:
    /**
     * The key to access the Location Code Lists (LCL) is
     *   - Country ID (CID)
     *   - Table code (TABCD)
     */
    using Key = std::pair<uint16_t, uint8_t>;

    /**
     * This map contains all loaded Location Code Lists.
     */
    std::map<Key, LocationCodeList> m_locationCodeLists;
};

/**
 * Return reference to LocationCodeLists.
 *
 * @return Reference to LocationCodeLists
 */
extern ISO_14819_EXPORT LocationCodeLists & locationCodeLists(); /* singleton */

}
