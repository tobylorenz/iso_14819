/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "EventList.h"

#include <cassert>

#include "DatabaseHelpers.h"

namespace ISO14819 {

EventList::EventList(std::string language) :
    m_language(),
    m_database(nullptr),
    m_events()
{
    setLanguage(language);
}

void EventList::setLanguage(std::string language)
{
    /* tries to load the new language */
    if (!loadDatabase(language)) {
        /* on failure, load default language */
        (void) loadDatabase("en_CEN");
    }
}

std::string EventList::language() const
{
    return m_language;
}

Event EventList::event(uint16_t eventCode)
{
    /* check */
    assert(eventCode <= 0x7ff); // 11 bit

    /* return */
    return m_events[eventCode];
}

std::string EventList::fullPhrase(uint16_t eventCode, uint8_t quantifiers) const
{
    /* check */
    assert(eventCode <= 2047);
    // assert(quantifiers <= 32);
    assert(m_database);

    /* prepare SQL statement */
    std::string retVal;
    for (int fel = 0; fel <= 1; fel++) {
        std::string sql = std::string("SELECT TEXT,TEXT_Q0,TEXT_Q1,TEXT_QN FROM ")
                          + (fel ? "FEL" : "EL") + " WHERE CODE=" + std::to_string(eventCode);

        /* execute SQL statement */
        sqlite3_stmt * stmt = nullptr;
        (void) sqlite3_prepare(m_database, sql.c_str(), static_cast<int>(sql.size()), &stmt, nullptr);
        if (sqlite3_step(stmt) == SQLITE_ROW) {
            switch (quantifiers) {
            case 0:
                /* specific TEXT_Q0 */
                retVal = sqlite3string(stmt, 1);
                break;
            case 1:
                /* specific TEXT_Q1 */
                retVal = sqlite3string(stmt, 2);
                break;
            default:
                /* specific TEXT_QN */
                retVal = sqlite3string(stmt, 3);
                break;
            }

            /* fallback to default TEXT */
            if (retVal.empty()) {
                retVal = sqlite3string(stmt, 0);
            }
        }
        (void) sqlite3_finalize(stmt);
        if (!retVal.empty()) {
            break;
        }
    }
    return retVal;
}

std::string EventList::phrase(char letter, uint16_t number, uint8_t quantifiers) const
{
    /* check */
    assert((letter >= 'A') && (letter <= 'Z'));
    // assert(number <= 999);
    // assert(quantifiers <= 32);
    assert(m_database);

    /* prepare SQL statement */
    std::string sql;
    if (letter == 'Z') {
        /** Supplementary Info */
        sql = std::string("SELECT TEXT,TEXT_Q0,TEXT_Q1,TEXT_QN FROM SIL WHERE L='")
              + letter + "' AND N=" + std::to_string(number);
    } else {
        /** Phrase */
        sql = std::string("SELECT TEXT,TEXT_Q0,TEXT_Q1,TEXT_QN FROM PC WHERE L='")
              + letter + "' AND N=" + std::to_string(number);
    }

    /* execute SQL statement */
    sqlite3_stmt * stmt = nullptr;
    (void) sqlite3_prepare(m_database, sql.c_str(), static_cast<int>(sql.size()), &stmt, nullptr);
    std::string retVal;
    if (sqlite3_step(stmt) == SQLITE_ROW) {
        switch (quantifiers) {
        case 0:
            /* specific TEXT_Q0 */
            retVal = sqlite3string(stmt, 1);
            break;
        case 1:
            /* specific TEXT_Q1 */
            retVal = sqlite3string(stmt, 2);
            break;
        default:
            /* specific TEXT_QN */
            retVal = sqlite3string(stmt, 3);
            break;
        }

        /* fallback to default TEXT */
        if (retVal.empty()) {
            retVal = sqlite3string(stmt, 0);
        }
    }
    (void) sqlite3_finalize(stmt);
    return retVal;
}

std::string EventList::quantifier(uint8_t label, uint8_t quantifier, uint8_t code) const
{
    /* check */
    assert((label >= 2) && (label <= 4));
    assert(quantifier <= 5);
    assert(code <= 32);
    assert(m_database);
    code %= 32; // 32 becomes 0

    /* prepare SQL statement */
    std::string sql = "SELECT ";
    switch (label) {
    case 2:
        sql += "L2";
        break;
    case 3:
        sql += "L3";
        break;
    case 4:
        switch (quantifier) {
        case 0:
            sql += "L4_Q0";
            break;
        case 1:
            sql += "L4_Q1";
            break;
        case 2:
            sql += "L4_Q2";
            break;
        case 3:
            sql += "L4_Q3";
            break;
        case 4:
            sql += "L4_Q4";
            break;
        case 5:
            sql += "L4_Q5";
            break;
        }
        break;
    }
    sql += " FROM QC WHERE CODE=" + std::to_string(code);

    /* execute SQL statement */
    sqlite3_stmt * stmt = nullptr;
    (void) sqlite3_prepare(m_database, sql.c_str(), static_cast<int>(sql.size()), &stmt, nullptr);
    std::string retVal;
    if (sqlite3_step(stmt) == SQLITE_ROW) {
        retVal = sqlite3string(stmt, 0);
    }
    return retVal;
}

bool EventList::loadDatabase(std::string language)
{
    /* create database filename */
    m_language = "";
    std::string filename = std::string(DATABASEDIR) + "/el/" + language + ".db";

    /* open database */
    if (sqlite3_open_v2(filename.c_str(), &m_database, SQLITE_OPEN_READONLY, nullptr) != SQLITE_OK) {
        if (m_database) {
            (void) sqlite3_close(m_database);
            m_database = nullptr;
        }
        return false;
    }

    /* read in all language-independent constants from (Forecast) Event List */
    m_events.clear();
    for (int fel = 0; fel <= 1; fel++) {
        std::string sql = std::string("SELECT CODE,N,Q,T,D,U,C,R FROM ") + (fel ? "FEL" : "EL");
        sqlite3_stmt * stmt = nullptr;
        (void) sqlite3_prepare(m_database, sql.c_str(), static_cast<int>(sql.size()), &stmt, nullptr);
        while (sqlite3_step(stmt) == SQLITE_ROW) {
            uint16_t code = static_cast<uint16_t>(sqlite3_column_int(stmt, 0));
            Event & entry = m_events[code];

            /* N(ature): ''=Info 'F'=Forecast 'S'=Silent */
            std::string str = sqlite3string(stmt, 1);
            if (str == "F") {
                entry.setNature('F');
            } else if (str == "S") {
                entry.setNature('S');
            } else {
                entry.setNature('I');
            }

            /* Q(uantifier): 0..12 */
            entry.setQuantifier(static_cast<uint8_t>(sqlite3_column_int(stmt, 2)));

            /* (Duration) T(ype): 'D'=Dynamic 'L'=Longer-lasting '(D)' '(L)' */
            str = sqlite3string(stmt, 3);
            if (str == "D") {
                entry.setDurationType('D');
                entry.setSpoken(true);
            } else if (str == "L") {
                entry.setDurationType('L');
                entry.setSpoken(true);
            } else if (str == "(D)") {
                entry.setDurationType('D');
                entry.setSpoken(false);
            } else if (str == "(L)") {
                entry.setDurationType('L');
                entry.setSpoken(false);
            }

            /* D(irectionality): 1..2 */
            entry.setDirectionality(static_cast<uint8_t>(sqlite3_column_int(stmt, 4)));

            /* U(rgency): ' ' 'U' 'X' */
            str = sqlite3string(stmt, 5);
            if (str == "U") {
                entry.setUrgency('U');
            } else if (str == "X") {
                entry.setUrgency('X');
            } else {
                entry.setUrgency('N');
            }

            /* (Update) C(lass): 1..31=EL, 32..39=FEL */
            entry.setUpdateClass(static_cast<uint8_t>(sqlite3_column_int(stmt, 6)));

            /* Phrase Codes (References) */
            entry.setPhraseCodes(sqlite3string(stmt, 7));
        }
        (void) sqlite3_finalize(stmt);
    }

    m_language = language;
    return true;
}

EventList & eventList()
{
    /* singleton */
    static EventList list;
    return list;
}

}
