/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include <cstdlib>
#include <string>

#include "BitFifo.h"
#include "Currencies.h"

#include "iso_14819_export.h"
#include "platform.h"

namespace ISO14819 {

/** Telephone Number */
class ISO_14819_EXPORT TelephoneNumber
{
public:
    explicit TelephoneNumber();

    /**
     * Get Telephone number
     *
     * @return Telephone number
     */
    std::string telephoneNumber() const;

    /**
     * Set Telephone number
     *
     * @param[in] value Telephone number
     */
    void setTelephoneNumber(std::string value);

    /**
     * Get IVR option number
     *
     * @return IVR optiona number
     */
    std::string ivrOptionNumber() const;

    /**
     * Set IVR option number
     *
     * @param[in] value IVR option number
     */
    void setIvrOptionNumber(std::string value);

    /** Time unit of call cost */
    enum TimeUnit : uint8_t {
        /** Free */
        TimeFree = 0,

        /** Per second */
        TimePerSecond = 1,

        /** Per minute */
        TimePerMinute = 2,

        /** Per hour */
        TimePerHour = 3,

        /** Per Call */
        TimePerCall = 4,

        /** Per Day (Multiple calls on any individual day, incur single cost) */
        TimePerDay = 5,

        /** Variable fees apply */
        TimeVariableFeesApply = 6,

        /** (Undefined) not displayed */
        TimeUndefined = 7
    };

    /**
     * Get Time unit of call cost
     *
     * @return Time unit of call cost
     */
    uint8_t timeUnit() const;

    /**
     * Set Time unit of call cost
     *
     * @param[in] value Time unit of call cost
     */
    void setTimeUnit(uint8_t value);

    /**
     * Get Cost
     *
     * @return Cost
     */
    uint16_t cost() const;

    /**
     * Set Cost
     *
     * @param[in] value Cost
     */
    void setCost(uint16_t value);

    /**
     * Get Decimal Multiplier
     *
     * @return Decimal Multiplier
     */
    uint8_t decimalMultiplier() const;

    /**
     * Set Decimal Multiplier
     *
     * @param[in] value Decimal Multiplier
     */
    void setDecimalMultiplier(uint8_t value);

    /**
     * Get Currency Position
     *
     * @return Currency Position
     */
    bool currencyInFront() const;

    /**
     * Set Currency Position
     *
     * @param[in] value Currency Position
     */
    void setCurrencyInFront(bool value);

    /**
     * Get Currency Symbol (reference to ISO 4217:2008)
     *
     * @return Currency Symbol
     */
    uint8_t currencyReference() const;

    /**
     * Set Currency Symbol (reference to ISO 4217:2008)
     *
     * @param[in] value Currency Symbol
     */
    void setCurrencyReference(uint8_t value);

    /**
     * Pull data from the bit fifo
     *
     * @param[in] fifo FIFO to pull data from
     * @return true if data is complete, false if more to be read
     */
    bool pull(BitFifo & fifo);

private:
    /** Telephone number */
    std::string m_telephoneNumber;

    /** IVR option number */
    std::string m_ivrOptionNumber;

    /** Time unit of call cost */
    uint8_t m_timeUnit : 3; // see TimeUnit

    /** Cost */
    uint16_t m_cost : 14;

    /** Decimal Multiplier */
    uint8_t m_decimalMultiplier : 2; // 0..3 => /1, /10, /100, /1000

    /**
     * Currency Position
     *   - 0: currency symbol shall be placed after the cost number
     *   - 1: currency symbol shall be placed in front of the cost number
     */
    bool m_currencyInFront;

    /** Currency Symbol (reference to ISO 4217:2008) */
    uint8_t m_currencyReference : 8;

    /** State Machine State */
    enum class State {
        /** Telephone number */
        TelephoneNumber = 0,

        /** IVR option number */
        IvrOptionNumber,

        /** Time unit of call cost */
        TimeUnit,

        /** Decimal Multiplier */
        Multiplier,

        /** Cost */
        Cost,

        /** Currency Position */
        Pos,

        /** Currency Symbol */
        Currency,

        /** Complete */
        Complete
    };

    /** State Machine State */
    State m_state;

    /** State Machine is in Alpha Value mode */
    bool m_alphaValueMode;

    /** Append an numerical value */
    void appendNumericalValue(uint8_t data);

    /** Append an alpha value */
    void appendAlphaValue(uint8_t data);
};

}
