/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#define BOOST_TEST_MODULE OptionalMessageContent
#if !defined(WIN32)
#define BOOST_TEST_DYN_LINK
#endif
#include <boost/test/unit_test.hpp>

#include "ISO/14819/Message.h"
#include "ISO/14819/OptionalMessageContent.h"

/**
 * 0: Duration
 */
BOOST_AUTO_TEST_CASE(Duration)
{
    ISO14819::Message message0;

    message0.pushFreeFormat(0, 4); // label = 0
    message0.pushFreeFormat(0, 3); // durationPersistence = 0

    BOOST_CHECK(message0.durationPersistence() == 0);
    message0.processOptionalMessageContents();
    BOOST_CHECK(message0.durationPersistence() == 0);

    ISO14819::Message message;

    message.pushFreeFormat(0, 4); // label = 0
    message.pushFreeFormat(5, 3); // durationPersistence = 5

    BOOST_CHECK(message.durationPersistence() == 0);
    message.processOptionalMessageContents();
    BOOST_CHECK(message.durationPersistence() == 5);
}

/**
 * 1: Control Codes
 */
BOOST_AUTO_TEST_CASE(ControlCodes)
{
    ISO14819::Message message0;
    message0.pushFreeFormat(1, 4); // label = 1
    message0.pushFreeFormat(0, 3); // control code = 0
    message0.eventListEntry.setUrgency('N');
    message0.processOptionalMessageContents();
    BOOST_CHECK(message0.eventListEntry.urgency() == 'U');

    ISO14819::Message message1;
    message1.pushFreeFormat(1, 4); // label = 1
    message1.pushFreeFormat(1, 3); // control code = 1
    message1.eventListEntry.setUrgency('N');
    message1.processOptionalMessageContents();
    BOOST_CHECK(message1.eventListEntry.urgency() == 'X');

    ISO14819::Message message2;
    message2.pushFreeFormat(1, 4); // label = 1
    message2.pushFreeFormat(2, 3); // control code = 2
    message2.eventListEntry.setDirectionality(1);
    message2.processOptionalMessageContents();
    BOOST_CHECK(message2.eventListEntry.directionality() == 2);

    ISO14819::Message message3;
    message3.pushFreeFormat(1, 4); // label = 1
    message3.pushFreeFormat(3, 3); // control code = 3
    message3.eventListEntry.setDurationType('D');
    message3.processOptionalMessageContents();
    BOOST_CHECK(message3.eventListEntry.durationType() == 'L');

    ISO14819::Message message4;
    message4.pushFreeFormat(1, 4); // label = 1
    message4.pushFreeFormat(4, 3); // control code = 4
    message4.eventListEntry.setSpoken(false);
    message4.processOptionalMessageContents();
    BOOST_CHECK(message4.eventListEntry.spoken() == true);

    ISO14819::Message message5;
    message5.pushFreeFormat(1, 4); // label = 1
    message5.pushFreeFormat(5, 3); // control code = 5
    message5.setDiversionAdvice(false);
    message5.processOptionalMessageContents();
    BOOST_CHECK(message5.diversionAdvice() == true);

    ISO14819::Message message6;
    message6.pushFreeFormat(1, 4); // label = 1
    message6.pushFreeFormat(6, 3); // control code = 6
    message6.setExtent(7);
    message6.processOptionalMessageContents();
    BOOST_CHECK(message6.extent() == 7 + 8);

    ISO14819::Message message7;
    message7.pushFreeFormat(1, 4); // label = 1
    message7.pushFreeFormat(7, 3); // control code = 7
    message7.setExtent(7);
    message7.processOptionalMessageContents();
    BOOST_CHECK(message7.extent() == 7 + 16);
}

/**
 * 2: Length of route affected
 */
BOOST_AUTO_TEST_CASE(LengthOfRouteAffected)
{
    ISO14819::Message message;

    message.pushFreeFormat(2, 4); // label = 2
    message.pushFreeFormat(0, 5); // >100km
    message.pushFreeFormat(2, 4); // label = 2
    message.pushFreeFormat(1, 5); // 1km
    message.pushFreeFormat(2, 4); // label = 2
    message.pushFreeFormat(10, 5); // 10km
    message.pushFreeFormat(2, 4); // label = 2
    message.pushFreeFormat(11, 5); // 12km
    message.pushFreeFormat(2, 4); // label = 2
    message.pushFreeFormat(15, 5); // 20km
    message.pushFreeFormat(2, 4); // label = 2
    message.pushFreeFormat(16, 5); // 25km
    message.pushFreeFormat(2, 4); // label = 2
    message.pushFreeFormat(31, 5); // 100km
    BOOST_REQUIRE(message.optionalMessageContent().size() == 7);
    BOOST_CHECK(message.optionalMessageContent().at(0).lengthOfRouteAffected() == 0); // km
    BOOST_CHECK(message.optionalMessageContent().at(1).lengthOfRouteAffected() == 1); // km
    BOOST_CHECK(message.optionalMessageContent().at(2).lengthOfRouteAffected() == 10); // km
    BOOST_CHECK(message.optionalMessageContent().at(3).lengthOfRouteAffected() == 12); // km
    BOOST_CHECK(message.optionalMessageContent().at(4).lengthOfRouteAffected() == 20); // km
    BOOST_CHECK(message.optionalMessageContent().at(5).lengthOfRouteAffected() == 25); // km
    BOOST_CHECK(message.optionalMessageContent().at(6).lengthOfRouteAffected() == 100); // km

    message.processOptionalMessageContents();
    // Nothing to test here...
}

/**
 * 3: Speed limit
 */
BOOST_AUTO_TEST_CASE(SpeedLimit)
{
    ISO14819::Message message;

    message.pushFreeFormat(3, 4); // label = 3
    message.pushFreeFormat(16, 5); // 80km/h
    BOOST_REQUIRE(message.optionalMessageContent().size() == 1);
    BOOST_REQUIRE(message.optionalMessageContent().at(0).label() == 3);
    BOOST_REQUIRE(message.optionalMessageContent().at(0).data() == 16);
    BOOST_CHECK(message.optionalMessageContent().at(0).speedLimit() == 80); // km/h

    message.processOptionalMessageContents();
    // Nothing to test here...
}

/**
 * 4: Quantifier 5-bit
 */
BOOST_AUTO_TEST_CASE(Quantifier5)
{
    ISO14819::Message message;

    message.pushFreeFormat(4, 4); // label = 4
    message.pushFreeFormat(0, 5);

    message.processOptionalMessageContents();
    BOOST_REQUIRE(message.optionalMessageContent().size() == 1);
    BOOST_CHECK(message.optionalMessageContent().at(0).label() == 4);
    BOOST_CHECK(message.optionalMessageContent().at(0).data() == 0);
}

/**
 * 5: Quantifier 8-bit
 */
BOOST_AUTO_TEST_CASE(Quantifier)
{
    ISO14819::Message message;

    message.pushFreeFormat(5, 4); // label = 5
    message.pushFreeFormat(0, 8);

    message.processOptionalMessageContents();
    BOOST_REQUIRE(message.optionalMessageContent().size() == 1);
    BOOST_CHECK(message.optionalMessageContent().at(0).label() == 5);
    BOOST_CHECK(message.optionalMessageContent().at(0).data() == 0);
}

/**
 * 6: Supplementary information code
 */
BOOST_AUTO_TEST_CASE(SupplementaryInformationCode)
{
    ISO14819::Message message;

    message.pushFreeFormat(6, 4); // label = 6
    message.pushFreeFormat(0, 8);

    message.processOptionalMessageContents();
    BOOST_REQUIRE(message.optionalMessageContent().size() == 1);
    BOOST_CHECK(message.optionalMessageContent().at(0).label() == 6);
    BOOST_CHECK(message.optionalMessageContent().at(0).data() == 0);
}

/**
 * 7: Explicit start time
 */
BOOST_AUTO_TEST_CASE(ExplicitStartTime)
{
    ISO14819::Message message;

    message.pushFreeFormat(7, 4); // label = 7
    message.pushFreeFormat(0, 8);

    message.processOptionalMessageContents();
    BOOST_REQUIRE(message.optionalMessageContent().size() == 1);
    BOOST_CHECK(message.optionalMessageContent().at(0).label() == 7);
    BOOST_CHECK(message.optionalMessageContent().at(0).data() == 0);
}

/**
 * 8: Explicit stop time
 */
BOOST_AUTO_TEST_CASE(ExplicitStopTime)
{
    ISO14819::Message message;

    message.pushFreeFormat(8, 4); // label = 8
    message.pushFreeFormat(0, 8);

    message.processOptionalMessageContents();
    BOOST_REQUIRE(message.optionalMessageContent().size() == 1);
    BOOST_CHECK(message.optionalMessageContent().at(0).label() == 8);
    BOOST_CHECK(message.optionalMessageContent().at(0).data() == 0);
}

/**
 * 9: Additional event
 */
BOOST_AUTO_TEST_CASE(AdditionalEvent)
{
    ISO14819::Message message;

    message.pushFreeFormat(9, 4); // label = 9
    message.pushFreeFormat(0, 11);

    message.processOptionalMessageContents();
    BOOST_REQUIRE(message.optionalMessageContent().size() == 1);
    BOOST_CHECK(message.optionalMessageContent().at(0).label() == 9);
    BOOST_CHECK(message.optionalMessageContent().at(0).data() == 0);
}

/**
 * 10: Detailed diversion instructions
 */
BOOST_AUTO_TEST_CASE(DetailedDiversionInstructions)
{
    ISO14819::Message message;

    message.pushFreeFormat(10, 4); // label = 10
    message.pushFreeFormat(0, 16);

    message.processOptionalMessageContents();
    BOOST_REQUIRE(message.optionalMessageContent().size() == 1);
    BOOST_CHECK(message.optionalMessageContent().at(0).label() == 10);
    BOOST_CHECK(message.optionalMessageContent().at(0).data() == 0);
}

/**
 * 11: Destinations
 */
BOOST_AUTO_TEST_CASE(Destination)
{
    ISO14819::Message message;

    message.pushFreeFormat(11, 4); // label = 11
    message.pushFreeFormat(0, 16);

    message.processOptionalMessageContents();
    BOOST_REQUIRE(message.optionalMessageContent().size() == 1);
    BOOST_CHECK(message.optionalMessageContent().at(0).label() == 11);
    BOOST_CHECK(message.optionalMessageContent().at(0).data() == 0);
}

/**
 * 12: Precise location reference
 */
BOOST_AUTO_TEST_CASE(PreciseLocationReference)
{
    ISO14819::Message message;

    message.pushFreeFormat(12, 4); // label = 12
    message.pushFreeFormat((10 << 0) | (2 << 11) | (1 << 13) | (2 << 14), 16);
    message.processOptionalMessageContents();
    BOOST_REQUIRE(message.optionalMessageContent().size() == 1);
    BOOST_REQUIRE(message.optionalMessageContent().at(0).label() == 12);
    ISO14819::PreciseLocationReference plr = message.optionalMessageContent().at(0).preciseLocationReference();
    BOOST_CHECK(plr.distance() == 10);
    BOOST_CHECK(plr.accuracy() == 2);
    BOOST_CHECK(plr.reliability() == 1);
    BOOST_CHECK(plr.dynamics() == 2);
}

/**
 * 13: Cross linkage to source of problem, on another route
 */
BOOST_AUTO_TEST_CASE(CrossLinkage)
{
    ISO14819::Message message;

    message.pushFreeFormat(13, 4); // label = 13
    message.pushFreeFormat(0, 16);

    message.processOptionalMessageContents();
    BOOST_REQUIRE(message.optionalMessageContent().size() == 1);
    BOOST_CHECK(message.optionalMessageContent().at(0).label() == 13);
    BOOST_CHECK(message.optionalMessageContent().at(0).data() == 0);
}

/**
 * 14: Separator
 */
BOOST_AUTO_TEST_CASE(Separator)
{
    ISO14819::Message message;

    message.pushFreeFormat(14, 4); // label = 14
    // message.pushFreeFormat(0, 0);

    message.processOptionalMessageContents();
    BOOST_REQUIRE(message.optionalMessageContent().size() == 1);
    BOOST_CHECK(message.optionalMessageContent().at(0).label() == 14);
    BOOST_CHECK(message.optionalMessageContent().at(0).data() == 0);
}

/**
 * 15: Other information as defined by sub-labels
 */
BOOST_AUTO_TEST_CASE(OtherInformation)
{
    ISO14819::Message message;

    message.pushFreeFormat(15, 4); // label = 15
    // message.pushFreeFormat(0, 0);

    // message.processOptionalMessageContents();
    // This is tested in test_TelephoneNumber already
}

/**
 * Reserved for future use
 */
BOOST_AUTO_TEST_CASE(OtherInformationRfu)
{
    ISO14819::Message message;

    BOOST_CHECK(message.optionalMessageContent().size() == 0);

    /* push a RFU message */
    message.pushFreeFormat(15, 4); // label = 15
    message.pushFreeFormat(0, 6); // only 1&2 are defined
    BOOST_CHECK(message.optionalMessageContent().size() == 1);
    BOOST_CHECK(message.optionalMessageContent().at(0).label() == 15);

    /* further messages shouldn't be processed */
    message.pushFreeFormat(15, 4); // label = 12
    message.pushFreeFormat((10 << 0) | (2 << 11) | (1 << 13) | (2 << 14), 16);
    BOOST_CHECK(message.optionalMessageContent().size() == 1);
}
