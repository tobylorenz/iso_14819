/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#define BOOST_TEST_MODULE Encryption
#if !defined(WIN32)
#define BOOST_TEST_DYN_LINK
#endif
#include <boost/test/unit_test.hpp>

#include "ISO/14819/EncryptionKeys.h"
#include "ISO/14819/Message.h"
#include "ISO/14819/MessageMemory.h"
#include "ISO/14819/TrafficMessageChannel.h"

/**
 * This is the example from the specification.
 */
BOOST_AUTO_TEST_CASE(SpecExample)
{
    /* partially set table 0 */
    ISO14819::encryptionKeys().setKeyTable(0,  0, 0x00, 0x00, 0x00);
    ISO14819::encryptionKeys().setKeyTable(0,  1, 0x19, 0x01, 0x08);
    ISO14819::encryptionKeys().setKeyTable(0,  2, 0x9B, 0x03, 0x04);
    ISO14819::encryptionKeys().setKeyTable(0,  3, 0x7E, 0x06, 0x0C);
    ISO14819::encryptionKeys().setKeyTable(0,  4, 0x39, 0x07, 0x02);
    ISO14819::encryptionKeys().setKeyTable(0, 31, 0xAB, 0x01, 0x03);

    /* define a message */
    ISO14819::Message message;
    message.receiveTime.setModifiedJulianDay(20000); // some valid date
    message.setLocation(0x180D);
    message.systemInformation.setLocationTableNumber(0);
    message.systemInformation.encryptionInfo.setLocationTableNumberBeforeEncryption(1);
    message.systemInformation.encryptionInfo.setTest(3); // full encryption
    message.systemInformation.encryptionInfo.setTableId(0);
    message.systemInformation.encryptionInfo.setEncryptionId(4); // ENCID=4

    /* pass it on into the message memory and check it */
    ISO14819::MessageMemory messageMemory;
    messageMemory.insertMessage(message);
    BOOST_REQUIRE(messageMemory.list().size() == 1);
    BOOST_CHECK(messageMemory.list().at(0).location() == 0x1234);
}

/**
 * This is derived from specExample. The same with pre advised keys is tested here.
 */
BOOST_AUTO_TEST_CASE(SpecExamplePreAdvised)
{
    ISO14819::encryptionKeys().setKeyTablePreAdvised(0x39, 0x07, 0x02);

    ISO14819::Message message;
    message.setLocation(0x180D);
    message.systemInformation.encryptionInfo.setTest(1); // pre-advised
    message.systemInformation.encryptionInfo.setTableId(0); // SID=0
    message.systemInformation.encryptionInfo.setEncryptionId(4); // ENCID=4
    ISO14819::MessageMemory messageMemory;
    messageMemory.decryptLocationCode(message);
    BOOST_CHECK(message.location() == 0x1234);
}

/**
 * Test decrypt location with test=0 (no encryption) and test=2 (RFU).
 */
BOOST_AUTO_TEST_CASE(EncryptionTest02)
{
    ISO14819::MessageMemory messageMemory;

    /* 0: No Encryption */
    ISO14819::Message message0;
    message0.setLocation(0x180D);
    message0.systemInformation.encryptionInfo.setTest(0); // no encryption
    messageMemory.decryptLocationCode(message0);
    BOOST_CHECK(message0.location() == 0x180D);

    /* 2: RFU */
    ISO14819::Message message2;
    message2.setLocation(0x180D);
    message2.systemInformation.encryptionInfo.setTest(2); // RFU
    messageMemory.decryptLocationCode(message2);
    BOOST_CHECK(message2.location() == 0x180D);
}

/**
 * Test encryption settings
 */
BOOST_AUTO_TEST_CASE(DecodeEncryption)
{
    /* register ISO14819 handler */
    ISO14819::TrafficMessageChannel::registerHandler();
    ISO62106::RdsReceiver rdsReceiver;

    /* register TMC on 8A */
    rdsReceiver.decode(0xD392, (3 << 12) | (0 << 11) | (8 << 1) | (0 << 0), (0 << 14) | (1 << 6), 0xCD46);
    BOOST_CHECK(ISO62106::rdsProgram(0xD392).openDataApplications.value().at(8).at(0) == 0xCD46);

    /* decode encryption */
    rdsReceiver.decode(0xD392, 0x8400 | (0 & 0x1f), (0 << 13) | (1 << 11) | (2 << 5) | (3 << 0), (4 << 10) | (5 << 0));
    rdsReceiver.decode(0xD392, 0x8400 | (0 & 0x1f), (0 << 13) | (1 << 11) | (2 << 5) | (3 << 0), (4 << 10) | (5 << 0));
    BOOST_CHECK(ISO14819::trafficMessageChannel(0xD392).systemInformation.encryptionInfo.test() == 1);
    BOOST_CHECK(ISO14819::trafficMessageChannel(0xD392).systemInformation.serviceId() == 2);
    BOOST_CHECK(ISO14819::trafficMessageChannel(0xD392).systemInformation.encryptionInfo.encryptionId() == 3);
    BOOST_CHECK(ISO14819::trafficMessageChannel(0xD392).systemInformation.encryptionInfo.locationTableNumberBeforeEncryption() == 4);

    /* RFU 1..7 */
    rdsReceiver.decode(0xD392, 0x8400 | (0 & 0x1f), (1 << 13), 0);
    rdsReceiver.decode(0xD392, 0x8400 | (0 & 0x1f), (1 << 13), 0);
    rdsReceiver.decode(0xD392, 0x8400 | (0 & 0x1f), (2 << 13), 0);
    rdsReceiver.decode(0xD392, 0x8400 | (0 & 0x1f), (2 << 13), 0);
    rdsReceiver.decode(0xD392, 0x8400 | (0 & 0x1f), (3 << 13), 0);
    rdsReceiver.decode(0xD392, 0x8400 | (0 & 0x1f), (3 << 13), 0);
    rdsReceiver.decode(0xD392, 0x8400 | (0 & 0x1f), (4 << 13), 0);
    rdsReceiver.decode(0xD392, 0x8400 | (0 & 0x1f), (4 << 13), 0);
    rdsReceiver.decode(0xD392, 0x8400 | (0 & 0x1f), (5 << 13), 0);
    rdsReceiver.decode(0xD392, 0x8400 | (0 & 0x1f), (5 << 13), 0);
    rdsReceiver.decode(0xD392, 0x8400 | (0 & 0x1f), (6 << 13), 0);
    rdsReceiver.decode(0xD392, 0x8400 | (0 & 0x1f), (6 << 13), 0);
    rdsReceiver.decode(0xD392, 0x8400 | (0 & 0x1f), (7 << 13), 0);
    rdsReceiver.decode(0xD392, 0x8400 | (0 & 0x1f), (7 << 13), 0);
    // nothing to test here...
}
