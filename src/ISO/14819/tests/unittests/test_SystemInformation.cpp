/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#define BOOST_TEST_MODULE SystemInformation
#if !defined(WIN32)
#define BOOST_TEST_DYN_LINK
#endif
#include <boost/test/unit_test.hpp>

#include <ISO/62106.h>

#include "ISO/14819/SystemInformation.h"
#include "ISO/14819/TrafficMessageChannel.h"

/** RDS Receiver for the tests */
static ISO62106::RdsReceiver rdsReceiver;

/**
 * Prepare the environment
 */
BOOST_AUTO_TEST_CASE(TestInit)
{
    /* register ISO14819 handler */
    ISO14819::TrafficMessageChannel::registerHandler();

    /* register TMC on 8A */
    rdsReceiver.decode(0xD392, (3 << 12) | (0 << 11) | (8 << 1) | (0 << 0), (0 << 14) | (1 << 6), 0xCD46);
    BOOST_CHECK(ISO62106::rdsProgram(0xD392).openDataApplications.value().at(8).at(0) == 0xCD46);

    /* set a valid date */
    ISO62106::rdsProgram(0xD392).clockTime.decode(20000, 12, 0, 0, 0);
    BOOST_CHECK(ISO62106::rdsProgram(0xD392).clockTime.value().modifiedJulianDay() == 20000);
    BOOST_CHECK(ISO62106::rdsProgram(0xD392).clockTime.value().hour() == 12);
    BOOST_CHECK(ISO62106::rdsProgram(0xD392).clockTime.value().minute() == 0);
}

/**
 * System Information
 */
BOOST_AUTO_TEST_CASE(SystemInformation)
{
    /* variant code 0 */
    rdsReceiver.decode(0xD392, (3 << 12) | (0 << 11) | (8 << 1) | (0 << 0), (0 << 14) | (3 << 12) | (0x21 << 6) | (1 << 5) | (1 << 4) | (1 << 3) | (1 << 2) | (1 << 1) | (1 << 0), 0xCD46);
    BOOST_CHECK(ISO14819::trafficMessageChannel(0xD392).systemInformation.locationTableNumber() == 0x21);
    BOOST_CHECK(ISO14819::trafficMessageChannel(0xD392).systemInformation.alternativeFrequencyIndicator() == 1);
    BOOST_CHECK(ISO14819::trafficMessageChannel(0xD392).systemInformation.mode() == 1);
    BOOST_CHECK(ISO14819::trafficMessageChannel(0xD392).systemInformation.messageGeographicalScope.international() == 1);
    BOOST_CHECK(ISO14819::trafficMessageChannel(0xD392).systemInformation.messageGeographicalScope.national() == 1);
    BOOST_CHECK(ISO14819::trafficMessageChannel(0xD392).systemInformation.messageGeographicalScope.regional() == 1);
    BOOST_CHECK(ISO14819::trafficMessageChannel(0xD392).systemInformation.messageGeographicalScope.urban() == 1);

    /* variant code 1 (mode = 1) */
    rdsReceiver.decode(0xD392, (3 << 12) | (0 << 11) | (8 << 1) | (0 << 0), (1 << 14) | (2 << 12) | (0x21 << 6) | (2 << 4) | (2 << 2) | (2 << 0), 0xCD46);
    BOOST_CHECK(ISO14819::trafficMessageChannel(0xD392).systemInformation.gap() == 2);
    BOOST_REQUIRE(ISO14819::trafficMessageChannel(0xD392).systemInformation.mode() == 1);
    BOOST_CHECK(ISO14819::trafficMessageChannel(0xD392).systemInformation.enhancedMode.activityTime() == 2);
    BOOST_CHECK(ISO14819::trafficMessageChannel(0xD392).systemInformation.enhancedMode.windowTime() == 2);
    BOOST_CHECK(ISO14819::trafficMessageChannel(0xD392).systemInformation.enhancedMode.delayTime() == 2);

    /* variant code 1 (mode = 0) */
    rdsReceiver.decode(0xD392, (3 << 12) | (0 << 11) | (8 << 1) | (0 << 0), (0 << 14) | (1 << 6), 0xCD46); // reset to default
    BOOST_CHECK(ISO14819::trafficMessageChannel(0xD392).systemInformation.mode() == 0);
    rdsReceiver.decode(0xD392, (3 << 12) | (0 << 11) | (8 << 1) | (0 << 0), (1 << 14) | (2 << 4) | (0xD << 0), 0xCD46);
    BOOST_CHECK(ISO14819::trafficMessageChannel(0xD392).systemInformation.locationTableCountryCode() == 0xD);

    /* variant code 2 */
    rdsReceiver.decode(0xD392, (3 << 12) | (0 << 11) | (8 << 1) | (0 << 0), (2 << 14) | (0x21 << 8) | (0xE0 << 0), 0xCD46);
    BOOST_CHECK(ISO14819::trafficMessageChannel(0xD392).systemInformation.locationTableExtendedCountryCode() == 0xE0);

    /* variant code 3 */
    rdsReceiver.decode(0xD392, (3 << 12) | (0 << 11) | (8 << 1) | (0 << 0), (3 << 14), 0xCD46);
    // Nothing to test here...
}

/**
 * Test getters/setters of Enhanced Mode.
 */
BOOST_AUTO_TEST_CASE(GetSetEnhancedMode)
{
    ISO14819::EnhancedMode enhancedMode;

    /* activity time */
    BOOST_CHECK(enhancedMode.activityTime() == 0);
    enhancedMode.setActivityTime(3);
    BOOST_CHECK(enhancedMode.activityTime() == 3);

    /* window time */
    BOOST_CHECK(enhancedMode.windowTime() == 0);
    enhancedMode.setWindowTime(3);
    BOOST_CHECK(enhancedMode.windowTime() == 3);

    /* delay time */
    BOOST_CHECK(enhancedMode.delayTime() == 0);
    enhancedMode.setDelayTime(3);
    BOOST_CHECK(enhancedMode.delayTime() == 3);
}

/**
 * Test getters/setters of Message Geographical Scope.
 */
BOOST_AUTO_TEST_CASE(GetSetMessageGeographicalScope)
{
    ISO14819::MessageGeographicalScope mgs;

    /* international */
    BOOST_CHECK(mgs.international() == false);
    mgs.setInternational(true);
    BOOST_CHECK(mgs.international() == true);

    /* national */
    BOOST_CHECK(mgs.national() == false);
    mgs.setNational(true);
    BOOST_CHECK(mgs.national() == true);

    /* regional */
    BOOST_CHECK(mgs.regional() == false);
    mgs.setRegional(true);
    BOOST_CHECK(mgs.regional() == true);

    /* urban */
    BOOST_CHECK(mgs.urban() == false);
    mgs.setUrban(true);
    BOOST_CHECK(mgs.urban() == true);
}
