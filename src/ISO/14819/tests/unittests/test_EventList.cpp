/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#define BOOST_TEST_MODULE EventList
#if !defined(WIN32)
#define BOOST_TEST_DYN_LINK
#endif
#include <boost/test/unit_test.hpp>

#include "ISO/14819/Event.h"
#include "ISO/14819/EventList.h"

/**
 * Test event rotates.
 */
BOOST_AUTO_TEST_CASE(EventRotates)
{
    ISO14819::Event event;

    /* duration type: D/L */
    event.setDurationType('D');
    BOOST_CHECK(event.durationType() == 'D');
    event.switchDurationType();
    BOOST_CHECK(event.durationType() == 'L');
    event.switchDurationType();
    BOOST_CHECK(event.durationType() == 'D');

    /* spoken: true/false */
    event.setSpoken(false);
    BOOST_CHECK(event.spoken() == false);
    event.switchSpoken();
    BOOST_CHECK(event.spoken() == true);
    event.switchSpoken();
    BOOST_CHECK(event.spoken() == false);

    /* directionality: 1/2 */
    event.setDirectionality(1);
    BOOST_CHECK(event.directionality() == 1);
    event.switchDirectionality();
    BOOST_CHECK(event.directionality() == 2);
    event.switchDirectionality();
    BOOST_CHECK(event.directionality() == 1);

    /* urgency: N/U/X */
    event.setUrgency('N');
    BOOST_CHECK(event.urgency() == 'N');
    event.increaseUrgency();
    BOOST_CHECK(event.urgency() == 'U');
    event.increaseUrgency();
    BOOST_CHECK(event.urgency() == 'X');
    event.increaseUrgency();
    BOOST_CHECK(event.urgency() == 'N');
    event.decreaseUrgency();
    BOOST_CHECK(event.urgency() == 'X');
    event.decreaseUrgency();
    BOOST_CHECK(event.urgency() == 'U');
    event.decreaseUrgency();
    BOOST_CHECK(event.urgency() == 'N');
}

/**
 * Test some entries in the Event List.
 */
BOOST_AUTO_TEST_CASE(EventList)
{
    ISO14819::EventList eventList("en_GB");

    /* EL */
    BOOST_CHECK(eventList.event(1812).nature() == 'F');
    BOOST_CHECK(eventList.event(1812).quantifier() == 0);
    BOOST_CHECK(eventList.event(1812).durationType() == 'D');
    BOOST_CHECK(eventList.event(1812).directionality() == 1);
    BOOST_CHECK(eventList.event(1812).urgency() == 'U');
    BOOST_CHECK(eventList.event(1812).updateClass() == 2);
    BOOST_CHECK(eventList.event(1812).phraseCodes() == "T4.A3E");
    BOOST_CHECK(eventList.fullPhrase(1812, 0) == "traffic lights faulty. Slow traffic expected"); // TEXT_Q0
    BOOST_CHECK(eventList.fullPhrase(1812, 1) == "($Q sets of) traffic lights faulty. Slow traffic expected"); // TEXT_Q1 is empty, TEXT is used instead
    BOOST_CHECK(eventList.fullPhrase(1812, 2) == "($Q sets of) traffic lights faulty. Slow traffic expected"); // TEXT_QN is empty, TEXT is used instead

    /* FEL */
    BOOST_CHECK(eventList.event(80).nature() == 'F');
    BOOST_CHECK(eventList.fullPhrase(80, 0) == "heavy traffic should be expected");

    /* PC */
    BOOST_CHECK(eventList.phrase('T', 4, 0) == "traffic lights not working"); // TEXT_Q0
    BOOST_CHECK(eventList.phrase('T', 4, 1) == "($Q sets of) traffic lights not working"); // TEXT_Q1 is empty, TEXT is used instead
    BOOST_CHECK(eventList.phrase('T', 4, 2) == "($Q sets of) traffic lights not working"); // TEXT_QN is empty, TEXT is used instead
    BOOST_CHECK(eventList.phrase('A', 3, 0) == "slow traffic");

    /* SIL */
    BOOST_CHECK(eventList.phrase('Z', 2, 0) == "follow signs");
}

/**
 * Test quantifiers
 */
BOOST_AUTO_TEST_CASE(Quantifiers)
{
    ISO14819::EventList eventList("en_GB");

    BOOST_CHECK(eventList.quantifier(2, 0, 20) == "30 miles");
    BOOST_CHECK(eventList.quantifier(3, 0, 20) == "60 mph");
    BOOST_CHECK(eventList.quantifier(4, 0, 20) == "20");
    BOOST_CHECK(eventList.quantifier(4, 1, 20) == "400");
    BOOST_CHECK(eventList.quantifier(4, 2, 20) == "less than 200 m");
    BOOST_CHECK(eventList.quantifier(4, 3, 20) == "100 %");
    BOOST_CHECK(eventList.quantifier(4, 4, 20) == "60 mph");
    BOOST_CHECK(eventList.quantifier(4, 5, 20) == "up to 10 hours");
}

/**
 * Test languages.
 */
BOOST_AUTO_TEST_CASE(Languages)
{
    ISO14819::eventList().setLanguage("en_CEN");
    BOOST_CHECK(ISO14819::eventList().language() == "en_CEN");
    BOOST_CHECK(ISO14819::eventList().quantifier(2, 0, 20) == "45 km");

    ISO14819::eventList().setLanguage("en_GB");
    BOOST_CHECK(ISO14819::eventList().language() == "en_GB");
    BOOST_CHECK(ISO14819::eventList().quantifier(2, 0, 20) == "30 miles");

    ISO14819::eventList().setLanguage("xx_XX");
    BOOST_CHECK(ISO14819::eventList().language() == "en_CEN");
}
