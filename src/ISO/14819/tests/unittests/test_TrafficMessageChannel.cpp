/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#define BOOST_TEST_MODULE TrafficMessageChannel
#if !defined(WIN32)
#define BOOST_TEST_DYN_LINK
#endif
#include <boost/test/unit_test.hpp>

#include <ISO/62106.h>

#include "ISO/14819/MessageMemory.h"
#include "ISO/14819/TrafficMessageChannel.h"

/** RDS Receiver for the tests */
static ISO62106::RdsReceiver rdsReceiver;

/**
 * Prepare the environment
 */
BOOST_AUTO_TEST_CASE(TestInit)
{
    /* register ISO14819 handler */
    ISO14819::TrafficMessageChannel::registerHandler();

    /* register TMC on 8A */
    rdsReceiver.decode(0xD392, (3 << 12) | (0 << 11) | (8 << 1) | (0 << 0), (0 << 14) | (1 << 6), 0xCD46);
    BOOST_CHECK(ISO62106::rdsProgram(0xD392).openDataApplications.value().at(8).at(0) == 0xCD46);

    /* set a valid date */
    ISO62106::rdsProgram(0xD392).clockTime.decode(20000, 12, 0, 0, 0);
    BOOST_CHECK(ISO62106::rdsProgram(0xD392).clockTime.value().modifiedJulianDay() == 20000);
    BOOST_CHECK(ISO62106::rdsProgram(0xD392).clockTime.value().hour() == 12);
    BOOST_CHECK(ISO62106::rdsProgram(0xD392).clockTime.value().minute() == 0);
}

/**
 * Decode example from Alert-C Coding Handbook Chapter 6.6 Example 1
 */
BOOST_AUTO_TEST_CASE(Example1)
{
    /* clear the message memory first */
    ISO14819::messageMemory().clear();

    /* decode example */
    rdsReceiver.decode(0xD392, 0x8401, 0x8865, 0x2094);
    rdsReceiver.decode(0xD392, 0x8401, 0x8865, 0x2094);
    rdsReceiver.decode(0xD392, 0x8401, 0x4258, 0x0000);
    rdsReceiver.decode(0xD392, 0x8401, 0x4258, 0x0000);

    /* check contents */
    std::vector<ISO14819::Message> messageMemory = ISO14819::messageMemory().list();
    BOOST_REQUIRE(messageMemory.size() == 1);
    ISO14819::Message & message = messageMemory.at(0);
    BOOST_CHECK(message.direction() == 0);
    BOOST_CHECK(message.extent() == 1);
    BOOST_CHECK(message.event() == 101);
    BOOST_CHECK(message.location() == 8340);
    BOOST_REQUIRE(message.optionalMessageContent().size() == 1);
    BOOST_CHECK(message.optionalMessageContent().at(0).label() == 2);
    BOOST_CHECK(message.optionalMessageContent().at(0).lengthOfRouteAffected() == 12); // km
}

/**
 * Decode example from Alert-C Coding Handbook Chapter 6.6 Example 2
 */
BOOST_AUTO_TEST_CASE(Example2)
{
    /* clear the message memory first */
    ISO14819::messageMemory().clear();

    /* decode example */
    rdsReceiver.decode(0xD392, 0x8402, 0x8865, 0x2094);
    rdsReceiver.decode(0xD392, 0x8402, 0x8865, 0x2094);
    rdsReceiver.decode(0xD392, 0x8402, 0x42D0, 0xD000);
    rdsReceiver.decode(0xD392, 0x8402, 0x42D0, 0xD000);

    /* check contents */
    std::vector<ISO14819::Message> messageMemory = ISO14819::messageMemory().list();
    BOOST_REQUIRE(messageMemory.size() == 1);
    ISO14819::Message & message = messageMemory.at(0);
    BOOST_CHECK(message.direction() == 0);
    BOOST_CHECK(message.extent() == 1);
    BOOST_CHECK(message.event() == 101);
    BOOST_CHECK(message.location() == 8340);
    BOOST_REQUIRE(message.optionalMessageContent().size() == 2);
    BOOST_CHECK(message.optionalMessageContent().at(0).label() == 2);
    BOOST_CHECK(message.optionalMessageContent().at(0).lengthOfRouteAffected() == 75); // km
    BOOST_CHECK(message.optionalMessageContent().at(1).label() == 1);
    BOOST_CHECK(message.optionalMessageContent().at(1).data() == 5); // diversion bit set to 1
    BOOST_CHECK(message.diversionAdvice() == true);
}

/**
 * Decode example from Alert-C Coding Handbook Chapter 6.6 Example 3 (the 1st)
 */
BOOST_AUTO_TEST_CASE(Example3)
{
    /* clear the message memory first */
    ISO14819::messageMemory().clear();

    /* decode example */
    rdsReceiver.decode(0xD392, 0x8403, 0xC2C2, 0x1BBE);
    rdsReceiver.decode(0xD392, 0x8403, 0xC2C2, 0x1BBE);
    rdsReceiver.decode(0xD392, 0x8403, 0x7E90, 0xE6CE);
    rdsReceiver.decode(0xD392, 0x8403, 0x7E90, 0xE6CE);
    rdsReceiver.decode(0xD392, 0x8403, 0x2560, 0x8C34);
    rdsReceiver.decode(0xD392, 0x8403, 0x2560, 0x8C34);
    rdsReceiver.decode(0xD392, 0x8403, 0x15DE, 0xF45B);
    rdsReceiver.decode(0xD392, 0x8403, 0x15DE, 0xF45B);
    rdsReceiver.decode(0xD392, 0x8403, 0x0214, 0x5888);
    rdsReceiver.decode(0xD392, 0x8403, 0x0214, 0x5888);

    /* check contents */
    std::vector<ISO14819::Message> messageMemory = ISO14819::messageMemory().list();
    BOOST_REQUIRE(messageMemory.size() == 1);
    ISO14819::Message & message = messageMemory.at(0);
    BOOST_CHECK(message.direction() == 1);
    BOOST_CHECK(message.extent() == 0);
    BOOST_CHECK(message.event() == 706);
    BOOST_CHECK(message.location() == 7102);
    BOOST_REQUIRE(message.optionalMessageContent().size() == 7);
    BOOST_CHECK(message.optionalMessageContent().at(0).label() == 14);
    BOOST_CHECK(message.optionalMessageContent().at(0).data() == 0);
    BOOST_CHECK(message.optionalMessageContent().at(1).label() == 9);
    BOOST_CHECK(message.optionalMessageContent().at(1).data() == 115); // slow traffic
    BOOST_CHECK(message.optionalMessageContent().at(2).label() == 6);
    BOOST_CHECK(message.optionalMessageContent().at(2).data() == 114); // do not allow unnecessary gaps
    BOOST_CHECK(message.optionalMessageContent().at(3).label() == 11);
    BOOST_CHECK(message.optionalMessageContent().at(3).data() == 1121); // city "Duisburg"
    BOOST_CHECK(message.optionalMessageContent().at(4).label() == 10);
    BOOST_CHECK(message.optionalMessageContent().at(4).data() == 12023); // "A61, AK Mönchengladbach"
    BOOST_CHECK(message.optionalMessageContent().at(5).label() == 10);
    BOOST_CHECK(message.optionalMessageContent().at(5).data() == 1424); // Example is wrong here: 11664 = "A52, AK Neersen"
    BOOST_CHECK(message.optionalMessageContent().at(6).label() == 10);
    BOOST_CHECK(message.optionalMessageContent().at(6).data() == 11332); // "A44, AK Strümp"
}

/**
 * Decode example from Alert-C Coding Handbook Chapter 6.6 Example 3 (the 2nd)
 */
BOOST_AUTO_TEST_CASE(Example4)
{
    /* clear the message memory first */
    ISO14819::messageMemory().clear();

    /* decode example */
    rdsReceiver.decode(0xD392, 0x8404, 0xF06C, 0x272E);
    rdsReceiver.decode(0xD392, 0x8404, 0xF06C, 0x272E);
    rdsReceiver.decode(0xD392, 0x8404, 0x61E4, 0x715D);
    rdsReceiver.decode(0xD392, 0x8404, 0x61E4, 0x715D);
    rdsReceiver.decode(0xD392, 0x8404, 0x1345, 0x5086);
    rdsReceiver.decode(0xD392, 0x8404, 0x1345, 0x5086);
    rdsReceiver.decode(0xD392, 0x8404, 0x0C61, 0x30E0);
    rdsReceiver.decode(0xD392, 0x8404, 0x0C61, 0x30E0);

    /* check contents */
    std::vector<ISO14819::Message> messageMemory = ISO14819::messageMemory().list();
    BOOST_REQUIRE(messageMemory.size() == 1);
    ISO14819::Message & message = messageMemory.at(0);
    BOOST_CHECK(message.direction() == 1);
    BOOST_CHECK(message.extent() == 6 + 16); // OMC adds 16 here
    BOOST_CHECK(message.event() == 108);
    BOOST_CHECK(message.location() == 10030); // Example is wrong here: 7102
    BOOST_REQUIRE(message.optionalMessageContent().size() == 9);
    BOOST_CHECK(message.optionalMessageContent().at(0).label() == 1);
    BOOST_CHECK(message.optionalMessageContent().at(0).data() == 7); // increase the number of steps in extent by 16
    BOOST_CHECK(message.optionalMessageContent().at(1).label() == 2);
    BOOST_CHECK(message.optionalMessageContent().at(1).data() == 7);
    BOOST_CHECK(message.optionalMessageContent().at(1).lengthOfRouteAffected() == 7); // 7 km
    BOOST_CHECK(message.optionalMessageContent().at(2).label() == 1);
    BOOST_CHECK(message.optionalMessageContent().at(2).data() == 2); // default directionality changed
    BOOST_CHECK(message.eventListEntry.directionality() == 2);
    BOOST_CHECK(message.optionalMessageContent().at(3).label() == 14);
    BOOST_CHECK(message.optionalMessageContent().at(3).data() == 0);
    BOOST_CHECK(message.optionalMessageContent().at(4).label() == 9);
    BOOST_CHECK(message.optionalMessageContent().at(4).data() == 1301); // dense fog
    BOOST_CHECK(message.optionalMessageContent().at(5).label() == 4); // quantifier
    BOOST_CHECK(message.optionalMessageContent().at(5).data() == 4); // visibility less than 40 m
    BOOST_CHECK(message.optionalMessageContent().at(6).label() == 3);
    BOOST_CHECK(message.optionalMessageContent().at(6).data() == 12); // speed limit 60 km/h
    BOOST_CHECK(message.optionalMessageContent().at(6).speedLimit() == 60); // km/h
    BOOST_CHECK(message.optionalMessageContent().at(7).label() == 6);
    BOOST_CHECK(message.optionalMessageContent().at(7).data() == 19); // use fog lights
    BOOST_CHECK(message.optionalMessageContent().at(8).label() == 0);
    BOOST_CHECK(message.optionalMessageContent().at(8).data() == 7); // duration = 7
    BOOST_CHECK(message.durationPersistence() == 7);
}

/**
 * Test if the different AIDs work
 */
BOOST_AUTO_TEST_CASE(DifferentAIDs)
{
    /* AID=0xCD46 (V1) */
    rdsReceiver.decode(0xD393, (3 << 12) | (0 << 11) | (8 << 1) | (0 << 0), (0 << 14) | (1 << 6), 0xCD46);
    BOOST_CHECK(ISO62106::rdsProgram(0xD393).openDataApplications.value().at(8).at(0) == 0xCD46);
    rdsReceiver.decode(0xD393, 0x8407, 0, 0);
    BOOST_CHECK(ISO14819::trafficMessageChannel(0xD393).rdsProgram.programmeIdentification.value() == 0xD393);

    /* AID=0xCD47 (V2) */
    rdsReceiver.decode(0xD394, (3 << 12) | (0 << 11) | (8 << 1) | (0 << 0), (0 << 14) | (1 << 6), 0xCD47);
    BOOST_CHECK(ISO62106::rdsProgram(0xD394).openDataApplications.value().at(8).at(0) == 0xCD47);
    rdsReceiver.decode(0xD394, 0x8407, 0, 0);
    BOOST_CHECK(ISO14819::trafficMessageChannel(0xD394).rdsProgram.programmeIdentification.value() == 0xD394);

    /* AID=0x0D45 (Test) */
    rdsReceiver.decode(0xD395, (3 << 12) | (0 << 11) | (8 << 1) | (0 << 0), (0 << 14) | (1 << 6), 0x0D45);
    BOOST_CHECK(ISO62106::rdsProgram(0xD395).openDataApplications.value().at(8).at(0) == 0x0D45);
    rdsReceiver.decode(0xD395, 0x8407, 0, 0);
    BOOST_CHECK(ISO14819::trafficMessageChannel(0xD395).rdsProgram.programmeIdentification.value() == 0xD395);

    /* other AID */
    rdsReceiver.decode(0xD396, (3 << 12) | (0 << 11) | (8 << 1) | (0 << 0), (0 << 14) | (1 << 6), 0x1234);
    BOOST_CHECK(ISO62106::rdsProgram(0xD396).openDataApplications.value().at(8).at(0) == 0x1234);
    rdsReceiver.decode(0xD396, 0x8407, 0, 0);

    /* reset back to default */
    rdsReceiver.decode(0xD392, (3 << 12) | (0 << 11) | (8 << 1) | (0 << 0), (0 << 14) | (1 << 6), 0xCD46);
    BOOST_CHECK(ISO62106::rdsProgram(0xD392).openDataApplications.value().at(8).at(0) == 0xCD46);
}

/**
 * Test multi-group messges
 */
BOOST_AUTO_TEST_CASE(MultiGroup)
{
    /* 0x00: Encryption */
    // already tested in test_Encryption.cpp

    /* 0x01..0x06: multi-group */
    // Examples1..3 already covers this
    // rdsReceiver.decode(0xD392, 0x8401, 0, 0);
    // rdsReceiver.decode(0xD392, 0x8402, 0, 0);
    // rdsReceiver.decode(0xD392, 0x8403, 0, 0);
    // rdsReceiver.decode(0xD392, 0x8404, 0, 0);
    // rdsReceiver.decode(0xD392, 0x8405, 0, 0);
    // rdsReceiver.decode(0xD392, 0x8406, 0, 0);

    /* 0x07: RFU */
    rdsReceiver.decode(0xD392, 0x8407, 0, 0);
    rdsReceiver.decode(0xD392, 0x8407, 0, 0);
    // Nothing to test here...
}

/**
 * Test single-group messages
 */
BOOST_AUTO_TEST_CASE(SingleGroup)
{
    /* clear the message memory first */
    ISO14819::messageMemory().clear();

    /* 0x08..0x0f: single-group */
    rdsReceiver.decode(0xD392, 0x8408 | (3), (1 << 15) | (1 << 14) | (5 << 11) | (0x201), 0x8001);
    rdsReceiver.decode(0xD392, 0x8408 | (3), (1 << 15) | (1 << 14) | (5 << 11) | (0x201), 0x8001);

    /* check contents */
    std::vector<ISO14819::Message> messageMemory = ISO14819::messageMemory().list();
    BOOST_REQUIRE(messageMemory.size() == 1);
    ISO14819::Message & message = messageMemory.at(0);
    BOOST_CHECK(message.durationPersistence() == 3);
    BOOST_CHECK(message.direction() == 1);
    BOOST_CHECK(message.extent() == 5);
    BOOST_CHECK(message.event() == 0x201);
    BOOST_CHECK(message.location() == 0x8001);
}

/**
 * Test INTER-ROAD message
 */
BOOST_AUTO_TEST_CASE(InterRoadMessage)
{
    /* clear the message memory first */
    ISO14819::messageMemory().clear();

    /* decode inter-road */
    rdsReceiver.decode(0xD392, 0x8405, 0x8865, 0xFC00 | (0x9 << 6) | (0x21 << 0));
    rdsReceiver.decode(0xD392, 0x8405, 0x8865, 0xFC00 | (0x9 << 6) | (0x21 << 0));
    rdsReceiver.decode(0xD392, 0x8405, 0x4000 | (0x123), 0x4000);
    rdsReceiver.decode(0xD392, 0x8405, 0x4000 | (0x123), 0x4000);

    /* check contents */
    std::vector<ISO14819::Message> messageMemory = ISO14819::messageMemory().list();
    BOOST_REQUIRE(messageMemory.size() == 1);
    ISO14819::Message & message = messageMemory.at(0);
    BOOST_CHECK(message.direction() == 0);
    BOOST_CHECK(message.extent() == 1);
    BOOST_CHECK(message.event() == 0x65);
    BOOST_CHECK(message.locationTableCountryCode() == 0x9);
    BOOST_CHECK(message.locationTableNumber() == 0x21);
    BOOST_CHECK(message.location() == 0x1234);
    BOOST_CHECK(message.optionalMessageContent().size() == 0);
}

/**
 * Test multi-subsequent with no multi-first
 */
BOOST_AUTO_TEST_CASE(MultiSubsequentWithoutMultiFirst)
{
    /* clear the message memory first */
    ISO14819::messageMemory().clear();

    /* multi-subsequent with no multi-first */
    BOOST_CHECK(ISO14819::messageMemory().list().size() == 0);
    rdsReceiver.decode(0xD392, 0x8402, 0x42D0, 0xD000);
    rdsReceiver.decode(0xD392, 0x8402, 0x42D0, 0xD000);
    BOOST_CHECK(ISO14819::messageMemory().list().size() == 0);
}

/**
 * Test incomplete transfer to Message Memory in Single
 */
BOOST_AUTO_TEST_CASE(IncompleteMessageSingle)
{
    /* clear the message memory first */
    ISO14819::messageMemory().clear();

    /* incomplete decode */
    BOOST_CHECK(ISO14819::messageMemory().list().size() == 0);
    rdsReceiver.decode(0xD392, 0x8401, 0x8865, 0x2094); // event=0x65 location=0x2094
    rdsReceiver.decode(0xD392, 0x8401, 0x8865, 0x2094); // event=0x65 location=0x2094
    BOOST_CHECK(ISO14819::messageMemory().list().size() == 0);

    /* complete decode of single message */
    rdsReceiver.decode(0xD392, 0x8408, 0, 0);
    rdsReceiver.decode(0xD392, 0x8408, 0, 0);
    BOOST_CHECK(ISO14819::messageMemory().list().size() == 2);
}

/**
 * Test incomplete transfer to Message Memory in Multi-First
 */
BOOST_AUTO_TEST_CASE(IncompleteMessageMultiFirst)
{
    /* clear the message memory first */
    ISO14819::messageMemory().clear();

    /* incomplete decode */
    BOOST_CHECK(ISO14819::messageMemory().list().size() == 0);
    rdsReceiver.decode(0xD392, 0x8401, 0x8865, 0x2094); // event=0x65 location=0x2094
    rdsReceiver.decode(0xD392, 0x8401, 0x8865, 0x2094); // event=0x65 location=0x2094
    BOOST_CHECK(ISO14819::messageMemory().list().size() == 0);

    /* complete decode of multi-first message */
    rdsReceiver.decode(0xD392, 0x8402, 0x8866, 0x2095); // event=0x66 location=0x2095
    rdsReceiver.decode(0xD392, 0x8402, 0x8866, 0x2095); // event=0x66 location=0x2095
    BOOST_CHECK(ISO14819::messageMemory().list().size() == 1); // should pass 1st decode to message memory
    rdsReceiver.decode(0xD392, 0x8402, 0x42D0, 0xD000);
    rdsReceiver.decode(0xD392, 0x8402, 0x42D0, 0xD000);
    BOOST_CHECK(ISO14819::messageMemory().list().size() == 2); // now also 2nd decode
}

/**
 * CI test in MF and MS
 */
BOOST_AUTO_TEST_CASE(ContinuityIndex)
{
    /* clear the message memory first */
    ISO14819::messageMemory().clear();

    /* decode and test */
    BOOST_CHECK(ISO14819::messageMemory().list().size() == 0);
    rdsReceiver.decode(0xD392, 0x8403, 0xC2C2, 0x1BBE);
    rdsReceiver.decode(0xD392, 0x8403, 0xC2C2, 0x1BBE);
    rdsReceiver.decode(0xD392, 0x8403, 0xC2C2, 0x1BBE); // MF with same CI
    BOOST_CHECK(ISO14819::messageMemory().list().size() == 0);
    rdsReceiver.decode(0xD392, 0x8403, 0x7E90, 0xE6CE);
    rdsReceiver.decode(0xD392, 0x8403, 0x7E90, 0xE6CE);
    BOOST_CHECK(ISO14819::messageMemory().list().size() == 0); // message not complete yet
    rdsReceiver.decode(0xD392, 0x8404, 0x2560, 0x8C34); // other CI
    rdsReceiver.decode(0xD392, 0x8404, 0x2560, 0x8C34);
    BOOST_CHECK(ISO14819::messageMemory().list().size() == 1); // should pass 1st decode to message memory
}
