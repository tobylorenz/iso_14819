/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#define BOOST_TEST_MODULE Message
#if !defined(WIN32)
#define BOOST_TEST_DYN_LINK
#endif
#include <boost/test/unit_test.hpp>

#include "ISO/14819/Message.h"

/**
 * Tests if the calculation of the expiry time works correctly.
 */
BOOST_AUTO_TEST_CASE(PersistenceTime)
{
    /* Example 2.4.3 in TMC Compendium */
    ISO14819::Message message;
    message.receiveTime = ISO62106::RdsTime(16, 0); // 16:00 UTC
    message.eventListEntry.setNature('I');
    message.eventListEntry.setDurationType('D');
    message.setDurationPersistence(5); // for at least next 3 h
    BOOST_CHECK(message.persistenceTime == ISO62106::RdsTime(19, 0)); // 19:00 UTC
}

/**
 * Test message complete.
 */
BOOST_AUTO_TEST_CASE(MessageComplete)
{
    /* single-message with 1 group */
    ISO14819::Message message1;
    message1.setGroupsExpected(1);
    message1.incGroupsReceived();
    BOOST_CHECK(message1.complete() == true);

    /* multi-message with 2 groups */
    ISO14819::Message message2;
    // MF
    message2.incGroupsReceived();
    BOOST_CHECK(message2.complete() == false);
    // MS(sgi=1, gsi=0)
    message2.setGroupsExpected(0 + 2);
    message2.incGroupsReceived();
    BOOST_CHECK(message2.complete() == true);

    /* multi-message with 3 groups */
    ISO14819::Message message3;
    // MF
    message3.incGroupsReceived();
    BOOST_CHECK(message3.complete() == false);
    // MS(sgi=1, gsi=1)
    message3.setGroupsExpected(1 + 2);
    message3.incGroupsReceived();
    BOOST_CHECK(message3.complete() == false);
    // MS(sgi=0, gsi=0)
    message3.incGroupsReceived();
    BOOST_CHECK(message3.complete() == true);

    /* multi-message with 4 groups */
    ISO14819::Message message4;
    // MF
    message4.incGroupsReceived();
    BOOST_CHECK(message4.complete() == false);
    // MS(sgi=1, gsi=2)
    message4.setGroupsExpected(2 + 2);
    message4.incGroupsReceived();
    BOOST_CHECK(message4.complete() == false);
    // MS(sgi=0, gsi=1)
    message4.incGroupsReceived();
    BOOST_CHECK(message4.complete() == false);
    // MS(sgi=0, gsi=0)
    message4.incGroupsReceived();
    BOOST_CHECK(message4.complete() == true);

    /* multi-message with 5 groups */
    ISO14819::Message message5;
    // MF
    message5.incGroupsReceived();
    BOOST_CHECK(message5.complete() == false);
    // MS(sgi=1, gsi=3)
    message5.setGroupsExpected(3 + 2);
    message5.incGroupsReceived();
    BOOST_CHECK(message5.complete() == false);
    // MS(sgi=0, gsi=2)
    message5.incGroupsReceived();
    BOOST_CHECK(message5.complete() == false);
    // MS(sgi=0, gsi=1)
    message5.incGroupsReceived();
    BOOST_CHECK(message5.complete() == false);
    // MS(sgi=0, gsi=0)
    message5.incGroupsReceived();
    BOOST_CHECK(message5.complete() == true);
}

/**
 * Test LTECC, LTCC, LTN
 */
BOOST_AUTO_TEST_CASE(locationTableCodes)
{
    ISO14819::Message message;

    /* LTECC */
    BOOST_CHECK(message.locationTableExtendedCountryCode() == 0x00);
    message.systemInformation.setExtendedCountryCode(0x01); // from RDS
    BOOST_CHECK(message.locationTableExtendedCountryCode() == 0x01);
    message.systemInformation.setLocationTableExtendedCountryCode(0x02); // from TMC
    BOOST_CHECK(message.locationTableExtendedCountryCode() == 0x02);

    /* LTCC */
    BOOST_CHECK(message.locationTableCountryCode() == 0x0);
    message.systemInformation.setCountryCode(0x1); // from RDS
    BOOST_CHECK(message.locationTableCountryCode() == 0x1);
    message.systemInformation.setLocationTableCountryCode(0x2); // from TMC
    BOOST_CHECK(message.locationTableCountryCode() == 0x2);
    message.setInterRoadLocation(3, 0, 0); // from INTER-ROAD message
    BOOST_CHECK(message.locationTableCountryCode() == 0x3);

    /* LTN */
    BOOST_CHECK(message.locationTableNumber() == 0);
    message.systemInformation.setLocationTableNumber(1); // from RDS
    BOOST_CHECK(message.locationTableNumber() == 1);
    message.setInterRoadLocation(0, 2, 0); // from INTER-ROAD message
    BOOST_CHECK(message.locationTableNumber() == 2);
}

/**
 * Test several getter/setter.
 */
BOOST_AUTO_TEST_CASE(GetterSetter)
{
    ISO14819::Message message;

    /* event */
    BOOST_CHECK(message.event() == 0);
    message.setEvent(0x7ff);
    BOOST_CHECK(message.event() == 0x7ff);

    /* location */
    BOOST_CHECK(message.location() == 0);
    message.setLocation(0xffff);
    BOOST_CHECK(message.location() == 0xffff);

    /* direction */
    BOOST_CHECK(message.direction() == 0);
    message.setDirection(1);
    BOOST_CHECK(message.direction() == 1);

    /* extent */
    BOOST_CHECK(message.extent() == 0);
    message.setExtent(31);
    BOOST_CHECK(message.extent() == 31);

    /* duration/persistence */
    BOOST_CHECK(message.durationPersistence() == 0);
    message.setDurationPersistence(7);
    BOOST_CHECK(message.durationPersistence() == 7);

    /* diversion advice */
    BOOST_CHECK(message.diversionAdvice() == false);
    message.setDiversionAdvice(true);
    BOOST_CHECK(message.diversionAdvice() == true);
}

/**
 * Test different Duration/Persistence settings for Dynamic events.
 */
BOOST_AUTO_TEST_CASE(DurationPersistenceDynamic)
{
    ISO14819::Message message;
    message.receiveTime.setDate(2017 - 1900, 2, 15);
    message.receiveTime.setTime(16, 0);
    message.receiveTime.setLocalTimeOffset(2); // CET (UTC+1)
    message.eventListEntry.setDurationType('D'); // Dynamic

    /* DP=0 */
    message.setDurationPersistence(0);
    BOOST_CHECK(message.persistenceTime.day() == 15);
    BOOST_CHECK(message.persistenceTime.hour() == 16);
    BOOST_CHECK(message.persistenceTime.minute() == 15);
    BOOST_CHECK(message.decrementTime.valid() == false);

    /* DP=1 */
    message.setDurationPersistence(1);
    BOOST_CHECK(message.persistenceTime.day() == 15);
    BOOST_CHECK(message.persistenceTime.hour() == 16);
    BOOST_CHECK(message.persistenceTime.minute() == 15);
    BOOST_CHECK(message.decrementTime.valid() == false);

    /* DP=2 */
    message.setDurationPersistence(2);
    BOOST_CHECK(message.persistenceTime.day() == 15);
    BOOST_CHECK(message.persistenceTime.hour() == 16);
    BOOST_CHECK(message.persistenceTime.minute() == 30);
    BOOST_CHECK(message.decrementTime.day() == 15);
    BOOST_CHECK(message.decrementTime.hour() == 16);
    BOOST_CHECK(message.decrementTime.minute() == 15);

    /* DP=3 */
    message.setDurationPersistence(3);
    BOOST_CHECK(message.persistenceTime.day() == 15);
    BOOST_CHECK(message.persistenceTime.hour() == 17);
    BOOST_CHECK(message.persistenceTime.minute() == 0);
    BOOST_CHECK(message.decrementTime.day() == 15);
    BOOST_CHECK(message.decrementTime.hour() == 16);
    BOOST_CHECK(message.decrementTime.minute() == 30);

    /* DP=4 */
    message.setDurationPersistence(4);
    BOOST_CHECK(message.persistenceTime.day() == 15);
    BOOST_CHECK(message.persistenceTime.hour() == 18);
    BOOST_CHECK(message.persistenceTime.minute() == 0);
    BOOST_CHECK(message.decrementTime.day() == 15);
    BOOST_CHECK(message.decrementTime.hour() == 17);
    BOOST_CHECK(message.decrementTime.minute() == 0);

    /* DP=5 */
    message.setDurationPersistence(5);
    BOOST_CHECK(message.persistenceTime.day() == 15);
    BOOST_CHECK(message.persistenceTime.hour() == 19);
    BOOST_CHECK(message.persistenceTime.minute() == 0);
    BOOST_CHECK(message.decrementTime.day() == 15);
    BOOST_CHECK(message.decrementTime.hour() == 17);
    BOOST_CHECK(message.decrementTime.minute() == 0);

    /* DP=6 */
    message.setDurationPersistence(6);
    BOOST_CHECK(message.persistenceTime.day() == 15);
    BOOST_CHECK(message.persistenceTime.hour() == 20);
    BOOST_CHECK(message.persistenceTime.minute() == 0);
    BOOST_CHECK(message.decrementTime.day() == 15);
    BOOST_CHECK(message.decrementTime.hour() == 17);
    BOOST_CHECK(message.decrementTime.minute() == 0);

    /* DP=7 */
    message.setDurationPersistence(7);
    BOOST_CHECK(message.persistenceTime.day() == 15);
    BOOST_CHECK(message.persistenceTime.hour() == 23); // midnight in CET timezone
    BOOST_CHECK(message.persistenceTime.minute() == 0);
    BOOST_CHECK(message.decrementTime.valid() == false);
}

/**
 * Test different Duration/Persistence settings for Longer-lasting Information events.
 */
BOOST_AUTO_TEST_CASE(DurationPersistenceLongerLastingInformation)
{
    ISO14819::Message message;
    message.receiveTime.setDate(2017 - 1900, 2, 15);
    message.receiveTime.setTime(16, 0);
    message.receiveTime.setLocalTimeOffset(2); // CET (UTC+1)
    message.eventListEntry.setDurationType('L'); // Longer-lasting
    message.eventListEntry.setNature('I'); // Information

    /* DP=0 */
    message.setDurationPersistence(0);
    BOOST_CHECK(message.persistenceTime.day() == 15);
    BOOST_CHECK(message.persistenceTime.hour() == 17);
    BOOST_CHECK(message.persistenceTime.minute() == 0);
    BOOST_CHECK(message.decrementTime.valid() == false);

    /* DP=1 */
    message.setDurationPersistence(1);
    BOOST_CHECK(message.persistenceTime.day() == 15);
    BOOST_CHECK(message.persistenceTime.hour() == 18);
    BOOST_CHECK(message.persistenceTime.minute() == 0);
    BOOST_CHECK(message.decrementTime.valid() == false);

    /* DP=2 */
    message.setDurationPersistence(2);
    BOOST_CHECK(message.persistenceTime.day() == 15);
    BOOST_CHECK(message.persistenceTime.hour() == 23); // midnight in CET timezone
    BOOST_CHECK(message.persistenceTime.minute() == 00);
    BOOST_CHECK(message.decrementTime.valid() == false);

    /* DP=3 */
    message.setDurationPersistence(3);
    BOOST_CHECK(message.persistenceTime.day() == 16); // midnight the day after
    BOOST_CHECK(message.persistenceTime.hour() == 23);
    BOOST_CHECK(message.persistenceTime.minute() == 0);
    BOOST_CHECK(message.decrementTime.day() == 15);
    BOOST_CHECK(message.decrementTime.hour() == 23); // midnight in CET timezone
    BOOST_CHECK(message.decrementTime.minute() == 00);

    /* DP=4 */
    message.setDurationPersistence(4);
    BOOST_CHECK(message.persistenceTime.day() == 16); // midnight the day after
    BOOST_CHECK(message.persistenceTime.hour() == 23);
    BOOST_CHECK(message.persistenceTime.minute() == 0);
    BOOST_CHECK(message.decrementTime.day() == 17); // Friday
    BOOST_CHECK(message.decrementTime.hour() == 23); // midnight in CET timezone
    BOOST_CHECK(message.decrementTime.minute() == 00);

    /* DP=5 */
    message.setDurationPersistence(5);
    BOOST_CHECK(message.persistenceTime.day() == 16); // midnight the day after
    BOOST_CHECK(message.persistenceTime.hour() == 23);
    BOOST_CHECK(message.persistenceTime.minute() == 0);
    BOOST_CHECK(message.decrementTime.day() == 19); // Sunday
    BOOST_CHECK(message.decrementTime.hour() == 23); // midnight in CET timezone
    BOOST_CHECK(message.decrementTime.minute() == 00);

    /* DP=6 */
    message.setDurationPersistence(6);
    BOOST_CHECK(message.persistenceTime.day() == 16); // midnight the day after
    BOOST_CHECK(message.persistenceTime.hour() == 23);
    BOOST_CHECK(message.persistenceTime.minute() == 0);
    BOOST_CHECK(message.decrementTime.valid() == false);

    /* DP=7 */
    message.setDurationPersistence(7);
    BOOST_CHECK(message.persistenceTime.day() == 16); // midnight the day after
    BOOST_CHECK(message.persistenceTime.hour() == 23);
    BOOST_CHECK(message.persistenceTime.minute() == 0);
    BOOST_CHECK(message.decrementTime.valid() == false);
}

/**
 * Test different Duration/Persistence settings for Longer-lasting Forecast events.
 */
BOOST_AUTO_TEST_CASE(DurationPersistenceLongerLastingForecast)
{
    ISO14819::Message message;
    message.receiveTime.setDate(2017 - 1900, 2, 15);
    message.receiveTime.setTime(16, 0);
    message.receiveTime.setLocalTimeOffset(2); // CET (UTC+1)
    message.eventListEntry.setDurationType('L'); // Longer-lasting
    message.eventListEntry.setNature('F'); // Forecast

    /* DP=0 */
    message.setDurationPersistence(0);
    BOOST_CHECK(message.persistenceTime.day() == 15);
    BOOST_CHECK(message.persistenceTime.hour() == 17);
    BOOST_CHECK(message.persistenceTime.minute() == 0);
    BOOST_CHECK(message.decrementTime.valid() == false);

    /* DP=1 */
    message.setDurationPersistence(1);
    BOOST_CHECK(message.persistenceTime.day() == 15);
    BOOST_CHECK(message.persistenceTime.hour() == 18);
    BOOST_CHECK(message.persistenceTime.minute() == 0);
    BOOST_CHECK(message.decrementTime.valid() == false);

    /* DP=2 */
    message.setDurationPersistence(2);
    BOOST_CHECK(message.persistenceTime.day() == 15);
    BOOST_CHECK(message.persistenceTime.hour() == 23); // midnight in CET timezone
    BOOST_CHECK(message.persistenceTime.minute() == 00);
    BOOST_CHECK(message.decrementTime.valid() == false);

    /* DP=3 */
    message.setDurationPersistence(3);
    BOOST_CHECK(message.persistenceTime.day() == 16); // midnight the day after
    BOOST_CHECK(message.persistenceTime.hour() == 23);
    BOOST_CHECK(message.persistenceTime.minute() == 0);
    BOOST_CHECK(message.decrementTime.day() == 15);
    BOOST_CHECK(message.decrementTime.hour() == 23); // midnight in CET timezone
    BOOST_CHECK(message.decrementTime.minute() == 00);

    /* DP=4 */
    message.setDurationPersistence(4);
    BOOST_CHECK(message.persistenceTime.day() == 16); // midnight the day after
    BOOST_CHECK(message.persistenceTime.hour() == 23);
    BOOST_CHECK(message.persistenceTime.minute() == 0);
    BOOST_CHECK(message.decrementTime.day() == 15);
    BOOST_CHECK(message.decrementTime.hour() == 23); // midnight in CET timezone
    BOOST_CHECK(message.decrementTime.minute() == 00);

    /* DP=5 */
    message.setDurationPersistence(5);
    BOOST_CHECK(message.persistenceTime.day() == 16); // midnight the day after
    BOOST_CHECK(message.persistenceTime.hour() == 23);
    BOOST_CHECK(message.persistenceTime.minute() == 0);
    BOOST_CHECK(message.decrementTime.valid() == false);

    /* DP=6 */
    message.setDurationPersistence(6);
    BOOST_CHECK(message.persistenceTime.day() == 16); // midnight the day after
    BOOST_CHECK(message.persistenceTime.hour() == 23);
    BOOST_CHECK(message.persistenceTime.minute() == 0);
    BOOST_CHECK(message.decrementTime.valid() == false);

    /* DP=7 */
    message.setDurationPersistence(7);
    BOOST_CHECK(message.persistenceTime.day() == 16); // midnight the day after
    BOOST_CHECK(message.persistenceTime.hour() == 23);
    BOOST_CHECK(message.persistenceTime.minute() == 0);
    BOOST_CHECK(message.decrementTime.valid() == false);
}

/**
 * Test different Duration/Persistence settings for Longer-Lasting Silent events.
 */
BOOST_AUTO_TEST_CASE(DurationPersistenceLongerLastingSilent)
{
    ISO14819::Message message;
    message.receiveTime.setDate(2017 - 1900, 2, 15);
    message.receiveTime.setTime(16, 0);
    message.receiveTime.setLocalTimeOffset(2); // CET (UTC+1)
    message.eventListEntry.setDurationType('L'); // Longer-lasting
    message.eventListEntry.setNature('S'); // Silent

    /* DP=0 */
    message.setDurationPersistence(0);
    BOOST_CHECK(message.persistenceTime.day() == 15);
    BOOST_CHECK(message.persistenceTime.hour() == 17);
    BOOST_CHECK(message.persistenceTime.minute() == 0);
    BOOST_CHECK(message.decrementTime.valid() == false);

    /* DP=1 */
    message.setDurationPersistence(1);
    BOOST_CHECK(message.persistenceTime.day() == 15);
    BOOST_CHECK(message.persistenceTime.hour() == 18);
    BOOST_CHECK(message.persistenceTime.minute() == 0);
    BOOST_CHECK(message.decrementTime.valid() == false);

    /* DP=2 */
    message.setDurationPersistence(2);
    BOOST_CHECK(message.persistenceTime.day() == 15);
    BOOST_CHECK(message.persistenceTime.hour() == 23); // midnight in CET timezone
    BOOST_CHECK(message.persistenceTime.minute() == 00);
    BOOST_CHECK(message.decrementTime.valid() == false);

    /* DP=3 */
    message.setDurationPersistence(3);
    BOOST_CHECK(message.persistenceTime.day() == 16); // midnight the day after
    BOOST_CHECK(message.persistenceTime.hour() == 23);
    BOOST_CHECK(message.persistenceTime.minute() == 0);
    BOOST_CHECK(message.decrementTime.valid() == false);

    /* DP=4 */
    message.setDurationPersistence(4);
    BOOST_CHECK(message.persistenceTime.day() == 16); // midnight the day after
    BOOST_CHECK(message.persistenceTime.hour() == 23);
    BOOST_CHECK(message.persistenceTime.minute() == 0);
    BOOST_CHECK(message.decrementTime.valid() == false);

    /* DP=5 */
    message.setDurationPersistence(5);
    BOOST_CHECK(message.persistenceTime.day() == 16); // midnight the day after
    BOOST_CHECK(message.persistenceTime.hour() == 23);
    BOOST_CHECK(message.persistenceTime.minute() == 0);
    BOOST_CHECK(message.decrementTime.valid() == false);

    /* DP=6 */
    message.setDurationPersistence(6);
    BOOST_CHECK(message.persistenceTime.day() == 16); // midnight the day after
    BOOST_CHECK(message.persistenceTime.hour() == 23);
    BOOST_CHECK(message.persistenceTime.minute() == 0);
    BOOST_CHECK(message.decrementTime.valid() == false);

    /* DP=7 */
    message.setDurationPersistence(7);
    BOOST_CHECK(message.persistenceTime.day() == 16); // midnight the day after
    BOOST_CHECK(message.persistenceTime.hour() == 23);
    BOOST_CHECK(message.persistenceTime.minute() == 0);
    BOOST_CHECK(message.decrementTime.valid() == false);
}


/**
 * Test the startStoptime static function.
 */
BOOST_AUTO_TEST_CASE(StartStopTime)
{
    ISO62106::RdsTime receiveTime;
    ISO62106::RdsTime startStopTime;

    /* 42 (example from spec) */
    receiveTime.setTime(9, 0); // 09:00
    startStopTime = ISO14819::Message::startStopTime(receiveTime, 42);
    BOOST_CHECK(startStopTime.hour() == 10);
    BOOST_CHECK(startStopTime.minute() == 30);

    /* 153 (example from spec) */
    receiveTime.setDate(2017 - 1900, 2, 17); // Friday
    receiveTime.setTime(9, 0); // 09:00
    startStopTime = ISO14819::Message::startStopTime(receiveTime, 153);
    BOOST_CHECK(startStopTime.day() == 20); // Monday
    BOOST_CHECK(startStopTime.hour() == 9);
    BOOST_CHECK(startStopTime.minute() == 0);

    /* 218 (example from spec) */
    receiveTime.setDate(2017 - 1900, 8, 20);
    startStopTime = ISO14819::Message::startStopTime(receiveTime, 218);
    BOOST_CHECK(startStopTime.month() == 9); // September
    BOOST_CHECK(startStopTime.day() == 18);

    /* 236 (example from spec) */
    receiveTime.setDate(2017 - 1900, 9, 15);
    startStopTime = ISO14819::Message::startStopTime(receiveTime, 236);
    BOOST_CHECK(startStopTime.year() == 2018 - 1900);
    BOOST_CHECK(startStopTime.month() == 3); // March
    BOOST_CHECK(startStopTime.day() == 15); // mid of March

    /* 239 (example from spec) */
    receiveTime.setDate(2017 - 1900, 9, 15);
    startStopTime = ISO14819::Message::startStopTime(receiveTime, 239);
    BOOST_CHECK(startStopTime.year() == 2018 - 1900);
    BOOST_CHECK(startStopTime.month() == 4); // March
    BOOST_CHECK(startStopTime.day() == 30); // end of March

    /* the following are all corner case tests */
    receiveTime.setTime(12, 0); // 12:00
    receiveTime.setDate(2017 - 1900, 2, 17);

    /* 0..95 => 00:00 to 23:45 (15 minute interval) */
    /* 0 => 00:00 */
    startStopTime = ISO14819::Message::startStopTime(receiveTime, 0);
    BOOST_CHECK(startStopTime.modifiedJulianDay() == receiveTime.modifiedJulianDay());
    BOOST_CHECK(startStopTime.hour() == 0);
    BOOST_CHECK(startStopTime.minute() == 0);
    /* 95 => current day, 23:45 */
    startStopTime = ISO14819::Message::startStopTime(receiveTime, 95);
    BOOST_CHECK(startStopTime.modifiedJulianDay() == receiveTime.modifiedJulianDay());
    BOOST_CHECK(startStopTime.hour() == 23);
    BOOST_CHECK(startStopTime.minute() == 45);

    /* 96..200 => Hour and day, starting at midnight following message receipt (1 hour interval) */
    /* 96 */
    startStopTime = ISO14819::Message::startStopTime(receiveTime, 96);
    BOOST_CHECK(startStopTime.modifiedJulianDay() == receiveTime.modifiedJulianDay() + 1);
    BOOST_CHECK(startStopTime.hour() == 0);
    BOOST_CHECK(startStopTime.minute() == 0);
    /* 200 */
    startStopTime = ISO14819::Message::startStopTime(receiveTime, 200);
    BOOST_CHECK(startStopTime.modifiedJulianDay() == receiveTime.modifiedJulianDay() + 1 + 4);
    BOOST_CHECK(startStopTime.hour() == 8);
    BOOST_CHECK(startStopTime.minute() == 0);

    /* 201..231 => 1st to 31st day of the month (1 day interval) */
    /* 201 => 1st of month */
    startStopTime = ISO14819::Message::startStopTime(receiveTime, 201);
    BOOST_CHECK(startStopTime.year() == 2017 - 1900);
    BOOST_CHECK(startStopTime.month() == 3);
    BOOST_CHECK(startStopTime.day() == 1);
    BOOST_CHECK(startStopTime.hour() == 0);
    BOOST_CHECK(startStopTime.minute() == 0);
    /* 216 => 16th of month */
    startStopTime = ISO14819::Message::startStopTime(receiveTime, 216);
    BOOST_CHECK(startStopTime.year() == 2017 - 1900);
    BOOST_CHECK(startStopTime.month() == 3);
    BOOST_CHECK(startStopTime.day() == 16);
    BOOST_CHECK(startStopTime.hour() == 0);
    BOOST_CHECK(startStopTime.minute() == 0);
    /* 217 => 17th of month */
    startStopTime = ISO14819::Message::startStopTime(receiveTime, 217);
    BOOST_CHECK(startStopTime.year() == 2017 - 1900);
    BOOST_CHECK(startStopTime.month() == 3);
    BOOST_CHECK(startStopTime.day() == 17);
    BOOST_CHECK(startStopTime.hour() == 0);
    BOOST_CHECK(startStopTime.minute() == 0);
    /* 218 => 18th of month */
    startStopTime = ISO14819::Message::startStopTime(receiveTime, 218);
    BOOST_CHECK(startStopTime.year() == 2017 - 1900);
    BOOST_CHECK(startStopTime.month() == 2);
    BOOST_CHECK(startStopTime.day() == 18);
    BOOST_CHECK(startStopTime.hour() == 0);
    BOOST_CHECK(startStopTime.minute() == 0);
    /* 228 => 28th of month */
    startStopTime = ISO14819::Message::startStopTime(receiveTime, 228);
    BOOST_CHECK(startStopTime.year() == 2017 - 1900);
    BOOST_CHECK(startStopTime.month() == 2);
    BOOST_CHECK(startStopTime.day() == 28);
    BOOST_CHECK(startStopTime.hour() == 0);
    BOOST_CHECK(startStopTime.minute() == 0);
    /* 231 => 31th of month */
    receiveTime.setDate(2017 - 1900, 3, 17); // February has only 28 days, so set to March that has 31 days
    startStopTime = ISO14819::Message::startStopTime(receiveTime, 231);
    BOOST_CHECK(startStopTime.year() == 2017 - 1900);
    BOOST_CHECK(startStopTime.month() == 3);
    BOOST_CHECK(startStopTime.day() == 31);
    BOOST_CHECK(startStopTime.hour() == 0);
    BOOST_CHECK(startStopTime.minute() == 0);
    /* month overflow */
    receiveTime.setDate(2017 - 1900, 12, 15);
    startStopTime = ISO14819::Message::startStopTime(receiveTime, 214);
    BOOST_CHECK(startStopTime.year() == 2018 - 1900);
    BOOST_CHECK(startStopTime.month() == 1);
    BOOST_CHECK(startStopTime.day() == 14);
    BOOST_CHECK(startStopTime.hour() == 0);
    BOOST_CHECK(startStopTime.minute() == 0);

    /* 232..255 => 15th January to 31st December (half month interval) */
    /* 232 => 15th January */
    receiveTime.setDate(2017 - 1900, 3, 17);
    startStopTime = ISO14819::Message::startStopTime(receiveTime, 232);
    BOOST_CHECK(startStopTime.year() == 2018 - 1900);
    BOOST_CHECK(startStopTime.month() == 1);
    BOOST_CHECK(startStopTime.day() == 15);
    BOOST_CHECK(startStopTime.hour() == 0);
    BOOST_CHECK(startStopTime.minute() == 0);
    /* 255 => 31st December */
    startStopTime = ISO14819::Message::startStopTime(receiveTime, 255);
    BOOST_CHECK(startStopTime.year() == 2017 - 1900);
    BOOST_CHECK(startStopTime.month() == 12);
    BOOST_CHECK(startStopTime.day() == 31);
    BOOST_CHECK(startStopTime.hour() == 0);
    BOOST_CHECK(startStopTime.minute() == 0);
}

/**
 * Test the decrementDurationPersistence function
 */
BOOST_AUTO_TEST_CASE(DecrementDurationPersistence)
{
    ISO14819::Message message;
    message.receiveTime.setDate(2017 - 1900, 2, 15);
    message.receiveTime.setTime(16, 0);
    message.receiveTime.setLocalTimeOffset(2); // CET (UTC+1)
    message.eventListEntry.setDurationType('D'); // Dynamic

    ISO62106::RdsTime expectedDecrementTime;

    /* DP=7 */
    message.setDurationPersistence(7);
    expectedDecrementTime = ISO62106::RdsTime();
    BOOST_CHECK(message.decrementTime == expectedDecrementTime);

    /* DP=6 */
    message.setDurationPersistence(6);
    expectedDecrementTime = message.receiveTime;
    expectedDecrementTime.addTime(0, 1, 0);
    BOOST_CHECK(message.decrementTime == expectedDecrementTime);

    /* DP=5 */
    message.decrementDurationPersistence();
    expectedDecrementTime.addTime(0, 1, 0);
    BOOST_CHECK(message.decrementTime == expectedDecrementTime);

    /* DP=4 */
    message.decrementDurationPersistence();
    expectedDecrementTime.addTime(0, 1, 0);
    BOOST_CHECK(message.decrementTime == expectedDecrementTime);

    /* DP=3 */
    message.decrementDurationPersistence();
    expectedDecrementTime.addTime(0, 0, 30);
    BOOST_CHECK(message.decrementTime == expectedDecrementTime);

    /* DP=2 */
    message.decrementDurationPersistence();
    expectedDecrementTime.addTime(0, 0, 15);
    BOOST_CHECK(message.decrementTime == expectedDecrementTime);

    /* DP=1 */
    message.decrementDurationPersistence();
    expectedDecrementTime = ISO62106::RdsTime();
    BOOST_CHECK(message.decrementTime == expectedDecrementTime);

    /* DP=0 */
    message.decrementDurationPersistence();
    expectedDecrementTime = ISO62106::RdsTime();
    BOOST_CHECK(message.decrementTime == expectedDecrementTime);
}

/**
 * Test update classes.
 */
BOOST_AUTO_TEST_CASE(UpdateClasses)
{
    /* define a message with one event */
    ISO14819::Message message;
    message.setEvent(1); // Update Class = 1

    /* check */
    std::set<uint16_t> updateClasses = message.updateClasses();
    BOOST_CHECK(updateClasses.size() == 1);
    BOOST_CHECK(updateClasses.count(1) == 1);

    /* add another event with different update class */
    message.addOptionalMessageContent(9, 55); // Update Class = 2

    /* check */
    updateClasses = message.updateClasses();
    BOOST_CHECK(updateClasses.size() == 2);
    BOOST_CHECK(updateClasses.count(1) == 1);
    BOOST_CHECK(updateClasses.count(2) == 1);
}

/**
 * Test duration types
 */
BOOST_AUTO_TEST_CASE(DurationTypes)
{
    /* define a message with one event */
    ISO14819::Message message;
    message.setEvent(1); // Duration Type = 'D'

    /* check */
    std::set<char> durationTypes = message.durationTypes();
    BOOST_CHECK(durationTypes.size() == 1);
    BOOST_CHECK(durationTypes.count('D') == 1);

    /* add another event with different update class */
    message.addOptionalMessageContent(9, 401); // Duration Type = 'L'

    /* check */
    durationTypes = message.durationTypes();
    BOOST_CHECK(durationTypes.size() == 2);
    BOOST_CHECK(durationTypes.count('D') == 1);
    BOOST_CHECK(durationTypes.count('L') == 1);
}
