/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#define BOOST_TEST_MODULE MessageMemory
#if !defined(WIN32)
#define BOOST_TEST_DYN_LINK
#endif
#include <boost/test/unit_test.hpp>

#include <iostream>

#include "ISO/14819/Message.h"
#include "ISO/14819/MessageMemory.h"
#include "ISO/14819/TrafficMessageChannel.h"

/**
 * Test message drop if receive time invalid.
 */
BOOST_AUTO_TEST_CASE(InvalidReceiveTime)
{
    /* define message */
    ISO14819::Message message;
    BOOST_CHECK(message.receiveTime.valid() == false);

    /* send to message memory */
    ISO14819::MessageMemory messageMemory;
    messageMemory.insertMessage(message);
    BOOST_CHECK(messageMemory.list().size() == 0);
}

/**
 * Test message drop if incomplete Inter-Road message.
 */
BOOST_AUTO_TEST_CASE(IncompleteInterRoadMessage)
{
    /* define message */
    ISO14819::Message message;
    message.receiveTime.setModifiedJulianDay(20000); // some valid date
    message.setLocation(0xFC00); // 64512

    /* send to message memory */
    ISO14819::MessageMemory messageMemory;
    messageMemory.insertMessage(message);
    BOOST_CHECK(messageMemory.list().size() == 0);
}

/**
 * Test message drop if encrypted message with no LTNBE.
 */
BOOST_AUTO_TEST_CASE(EncryptedWithoutLTNBE)
{
    /* define message */
    ISO14819::Message message;
    message.receiveTime.setModifiedJulianDay(20000); // some valid date
    message.systemInformation.setLocationTableNumber(0);
    message.systemInformation.encryptionInfo.setLocationTableNumberBeforeEncryption(0);

    /* send to message memory */
    ISO14819::MessageMemory messageMemory;
    messageMemory.insertMessage(message);
    BOOST_CHECK(messageMemory.list().size() == 0);
}

/**
 * Decode full cancellation of all messages from that RDS-TMC service.
 */
BOOST_AUTO_TEST_CASE(DecodeCancelMessagesService)
{
    ISO14819::MessageMemory messageMemory;
    BOOST_CHECK(messageMemory.list().size() == 0);

    /* message with SID=1 */
    ISO14819::Message message1;
    message1.receiveTime.setModifiedJulianDay(20000); // some valid date
    message1.systemInformation.setLocationTableNumber(1);
    message1.systemInformation.setServiceId(1);
    message1.setEvent(1);
    message1.setLocation(1);
    messageMemory.insertMessage(message1);
    BOOST_CHECK(messageMemory.list().size() == 1);

    /* message with SID=2 */
    ISO14819::Message message2;
    message2.receiveTime.setModifiedJulianDay(20000); // some valid date
    message2.systemInformation.setLocationTableNumber(1);
    message2.systemInformation.setServiceId(2);
    message2.setEvent(2);
    message2.setLocation(2);
    messageMemory.insertMessage(message2);
    BOOST_CHECK(messageMemory.list().size() == 2);

    /* message with SID=3 */
    ISO14819::Message message3;
    message3.receiveTime.setModifiedJulianDay(20000); // some valid date
    message3.systemInformation.setLocationTableNumber(1);
    message3.systemInformation.setServiceId(3);
    message3.setEvent(3);
    message3.setLocation(3);
    messageMemory.insertMessage(message3);
    BOOST_CHECK(messageMemory.list().size() == 3);

    /* insert cancel message */
    ISO14819::Message message;
    message.receiveTime.setModifiedJulianDay(20000); // some valid date
    message.systemInformation.setLocationTableNumber(1);
    message.systemInformation.setServiceId(2);
    message.setEvent(2047);
    message.setLocation(65535);
    messageMemory.insertMessage(message);

    /* send to message memory */
    BOOST_REQUIRE(messageMemory.list().size() == 2);
    BOOST_CHECK(messageMemory.list().at(0).systemInformation.serviceId() == 1);
    BOOST_CHECK(messageMemory.list().at(1).systemInformation.serviceId() == 3);
}

/**
 * Decode full cancellation of all messages with that location code
 */
BOOST_AUTO_TEST_CASE(DecodeCancelMessagesLocationCode)
{
    ISO14819::MessageMemory messageMemory;
    BOOST_CHECK(messageMemory.list().size() == 0);

    /* message with SID=1 */
    ISO14819::Message message1;
    message1.receiveTime.setModifiedJulianDay(20000); // some valid date
    message1.systemInformation.setLocationTableNumber(1);
    message1.systemInformation.setServiceId(1);
    message1.setEvent(1);
    message1.setLocation(1);
    messageMemory.insertMessage(message1);
    BOOST_CHECK(messageMemory.list().size() == 1);

    /* message with SID=2 */
    ISO14819::Message message2;
    message2.receiveTime.setModifiedJulianDay(20000); // some valid date
    message2.systemInformation.setLocationTableNumber(1);
    message2.systemInformation.setServiceId(1);
    message2.setEvent(2);
    message2.setLocation(2);
    messageMemory.insertMessage(message2);
    BOOST_CHECK(messageMemory.list().size() == 2);

    /* message with SID=3 */
    ISO14819::Message message3;
    message3.receiveTime.setModifiedJulianDay(20000); // some valid date
    message3.systemInformation.setLocationTableNumber(1);
    message3.systemInformation.setServiceId(1);
    message3.setEvent(3);
    message3.setLocation(3);
    messageMemory.insertMessage(message3);
    BOOST_CHECK(messageMemory.list().size() == 3);

    /* insert cancel message */
    ISO14819::Message message;
    message.receiveTime.setModifiedJulianDay(20000); // some valid date
    message.systemInformation.setLocationTableNumber(1);
    message.systemInformation.setServiceId(1);
    message.setEvent(2047);
    message.setLocation(2);
    messageMemory.insertMessage(message);

    /* send to message memory */
    BOOST_REQUIRE(messageMemory.list().size() == 2);
    BOOST_CHECK(messageMemory.list().at(0).location() == 1);
    BOOST_CHECK(messageMemory.list().at(1).location() == 3);
}

/**
 * Decode full cancellation of all messages with an event code in the same update class
 */
BOOST_AUTO_TEST_CASE(DecodeCancelMessagesUpdateClass)
{
    ISO14819::MessageMemory messageMemory;
    BOOST_CHECK(messageMemory.list().size() == 0);

    /* message with SID=1 */
    ISO14819::Message message1;
    message1.receiveTime.setModifiedJulianDay(20000); // some valid date
    message1.systemInformation.setLocationTableNumber(1);
    message1.systemInformation.setServiceId(1);
    message1.setEvent(1);
    message1.setLocation(1);
    message1.eventListEntry.setUpdateClass(1);
    messageMemory.insertMessage(message1);
    BOOST_CHECK(messageMemory.list().size() == 1);

    /* message with SID=2 */
    ISO14819::Message message2;
    message2.receiveTime.setModifiedJulianDay(20000); // some valid date
    message2.systemInformation.setLocationTableNumber(1);
    message2.systemInformation.setServiceId(1);
    message2.setEvent(2);
    message2.setLocation(2);
    message2.eventListEntry.setUpdateClass(2);
    messageMemory.insertMessage(message2);
    BOOST_CHECK(messageMemory.list().size() == 2);

    /* message with SID=3 */
    ISO14819::Message message3;
    message3.receiveTime.setModifiedJulianDay(20000); // some valid date
    message3.systemInformation.setLocationTableNumber(1);
    message3.systemInformation.setServiceId(1);
    message3.setEvent(3);
    message3.setLocation(3);
    message3.eventListEntry.setUpdateClass(3);
    messageMemory.insertMessage(message3);
    BOOST_CHECK(messageMemory.list().size() == 3);

    /* insert cancel message */
    ISO14819::Message message;
    message.receiveTime.setModifiedJulianDay(20000); // some valid date
    message.systemInformation.setLocationTableNumber(1);
    message.systemInformation.setServiceId(1);
    message.setEvent(2);
    message.setLocation(65535);
    message.eventListEntry.setNature('S');
    message.eventListEntry.setUpdateClass(2);
    messageMemory.insertMessage(message);

    /* send to message memory */
    BOOST_REQUIRE(messageMemory.list().size() == 2);
    BOOST_CHECK(messageMemory.list().at(0).eventListEntry.updateClass() == 1);
    BOOST_CHECK(messageMemory.list().at(1).eventListEntry.updateClass() == 3);
}

/**
 * Test overwrite of messages in case:
 * has the same primary location, drawn from the same location table, as the existing message OR the
 * special location code 65535
 */
BOOST_AUTO_TEST_CASE(OverwriteMessage1)
{
    ISO14819::MessageMemory messageMemory;
    BOOST_CHECK(messageMemory.list().size() == 0);

    /* insert message */
    ISO14819::Message message;
    message.receiveTime.setModifiedJulianDay(20000); // some valid date
    message.systemInformation.setLocationTableNumber(1);
    message.systemInformation.setServiceId(1);
    message.setEvent(1);
    message.setLocation(1);
    message.eventListEntry.setNature('I');
    messageMemory.insertMessage(message);
    BOOST_REQUIRE(messageMemory.list().size() == 1);
    BOOST_CHECK(messageMemory.list().at(0).eventListEntry.nature() == 'I');

    /* same location code => overwrite */
    message.eventListEntry.setNature('F');
    messageMemory.insertMessage(message);
    BOOST_REQUIRE(messageMemory.list().size() == 1);
    BOOST_CHECK(messageMemory.list().at(0).eventListEntry.nature() == 'F');

    /* different location table number => insert */
    message.systemInformation.setLocationTableNumber(2);
    messageMemory.insertMessage(message);
    BOOST_REQUIRE(messageMemory.list().size() == 2);
}

/**
 * Test overwrite of messages in case:
 * has the same value of the direction bit
 */
BOOST_AUTO_TEST_CASE(OverwriteMessage2)
{
    ISO14819::MessageMemory messageMemory;
    BOOST_CHECK(messageMemory.list().size() == 0);

    /* insert message */
    ISO14819::Message message;
    message.receiveTime.setModifiedJulianDay(20000); // some valid date
    message.systemInformation.setLocationTableNumber(1);
    message.systemInformation.setServiceId(1);
    message.setEvent(1);
    message.setLocation(1);
    message.setDirection(0);
    message.eventListEntry.setNature('I');
    messageMemory.insertMessage(message);
    BOOST_REQUIRE(messageMemory.list().size() == 1);
    BOOST_CHECK(messageMemory.list().at(0).direction() == 0);
    BOOST_CHECK(messageMemory.list().at(0).eventListEntry.nature() == 'I');

    /* same direction => overwrite */
    message.eventListEntry.setNature('F');
    messageMemory.insertMessage(message);
    BOOST_REQUIRE(messageMemory.list().size() == 1);
    BOOST_CHECK(messageMemory.list().at(0).direction() == 0);
    BOOST_CHECK(messageMemory.list().at(0).eventListEntry.nature() == 'F');

    /* different direction */
    message.setDirection(1);
    message.eventListEntry.setNature('S');
    messageMemory.insertMessage(message);
    BOOST_REQUIRE(messageMemory.list().size() == 2);
    BOOST_CHECK(messageMemory.list().at(0).direction() == 0);
    BOOST_CHECK(messageMemory.list().at(0).eventListEntry.nature() == 'F');
    BOOST_CHECK(messageMemory.list().at(1).direction() == 1);
    BOOST_CHECK(messageMemory.list().at(1).eventListEntry.nature() == 'S');
}

/**
 * Test overwrite of messages in case:
 * contains an event that belongs to the same update class as any event (a multi-group message may have
 * more than one event) in the existing message
 */
BOOST_AUTO_TEST_CASE(OverwriteMessage3)
{
    ISO14819::MessageMemory messageMemory;
    BOOST_CHECK(messageMemory.list().size() == 0);

    /* insert message */
    ISO14819::Message message;
    message.receiveTime.setModifiedJulianDay(20000); // some valid date
    message.systemInformation.setLocationTableNumber(1);
    message.systemInformation.setServiceId(1);
    message.setEvent(1);
    message.setLocation(1);
    message.setDirection(0);
    message.eventListEntry.setUpdateClass(1);
    message.eventListEntry.setNature('I');
    messageMemory.insertMessage(message);
    BOOST_REQUIRE(messageMemory.list().size() == 1);
    BOOST_CHECK(messageMemory.list().at(0).eventListEntry.updateClass() == 1);
    BOOST_CHECK(messageMemory.list().at(0).eventListEntry.nature() == 'I');

    /* same direction => overwrite */
    message.eventListEntry.setNature('F');
    messageMemory.insertMessage(message);
    BOOST_REQUIRE(messageMemory.list().size() == 1);
    BOOST_CHECK(messageMemory.list().at(0).eventListEntry.updateClass() == 1);
    BOOST_CHECK(messageMemory.list().at(0).eventListEntry.nature() == 'F');

    /* different direction */
    message.eventListEntry.setUpdateClass(2);
    message.eventListEntry.setNature('S');
    messageMemory.insertMessage(message);
    BOOST_REQUIRE(messageMemory.list().size() == 2);
    BOOST_CHECK(messageMemory.list().at(0).eventListEntry.updateClass() == 1);
    BOOST_CHECK(messageMemory.list().at(0).eventListEntry.nature() == 'F');
    BOOST_CHECK(messageMemory.list().at(1).eventListEntry.updateClass() == 2);
    BOOST_CHECK(messageMemory.list().at(1).eventListEntry.nature() == 'S');
}

/**
 * Test overwrite of messages in case:
 * comes from the same RDS-TMC service. (i.e. a service with the same LTN and SID) as the existing
 * message
 */
BOOST_AUTO_TEST_CASE(OverwriteMessage4)
{
    ISO14819::MessageMemory messageMemory;
    BOOST_CHECK(messageMemory.list().size() == 0);

    /* insert message */
    ISO14819::Message message;
    message.receiveTime.setModifiedJulianDay(20000); // some valid date
    message.systemInformation.setLocationTableNumber(1);
    message.systemInformation.setServiceId(1);
    message.setEvent(1);
    message.setLocation(1);
    message.setDirection(0);
    message.eventListEntry.setUpdateClass(1);
    message.eventListEntry.setNature('I');
    messageMemory.insertMessage(message);
    BOOST_REQUIRE(messageMemory.list().size() == 1);
    BOOST_CHECK(messageMemory.list().at(0).systemInformation.serviceId() == 1);
    BOOST_CHECK(messageMemory.list().at(0).eventListEntry.nature() == 'I');

    /* same direction => overwrite */
    message.eventListEntry.setNature('F');
    messageMemory.insertMessage(message);
    BOOST_REQUIRE(messageMemory.list().size() == 1);
    BOOST_CHECK(messageMemory.list().at(0).systemInformation.serviceId() == 1);
    BOOST_CHECK(messageMemory.list().at(0).eventListEntry.nature() == 'F');

    /* different direction */
    message.systemInformation.setServiceId(2);
    message.eventListEntry.setNature('S');
    messageMemory.insertMessage(message);
    BOOST_REQUIRE(messageMemory.list().size() == 2);
    BOOST_CHECK(messageMemory.list().at(0).systemInformation.serviceId() == 1);
    BOOST_CHECK(messageMemory.list().at(0).eventListEntry.nature() == 'F');
    BOOST_CHECK(messageMemory.list().at(1).systemInformation.serviceId() == 2);
    BOOST_CHECK(messageMemory.list().at(1).eventListEntry.nature() == 'S');
}

/**
 * Test overwrite of messages in case:
 * if the message relates to a forecast event (update classes 32 - 39), the same duration.
 */
BOOST_AUTO_TEST_CASE(OverwriteMessage5)
{
    ISO14819::MessageMemory messageMemory;
    BOOST_CHECK(messageMemory.list().size() == 0);

    /* insert message */
    ISO14819::Message message;
    message.receiveTime.setModifiedJulianDay(20000); // some valid date
    message.systemInformation.setLocationTableNumber(1);
    message.systemInformation.setServiceId(1);
    message.setEvent(1);
    message.setLocation(1);
    message.setDirection(0);
    message.eventListEntry.setDurationType('D');
    message.setDurationPersistence(1);
    message.eventListEntry.setUpdateClass(1);
    message.eventListEntry.setNature('I');
    messageMemory.insertMessage(message);
    BOOST_REQUIRE(messageMemory.list().size() == 1);
    BOOST_CHECK(messageMemory.list().at(0).durationPersistence() == 1);
    BOOST_CHECK(messageMemory.list().at(0).eventListEntry.nature() == 'I');

    /* same direction => overwrite */
    message.eventListEntry.setNature('F');
    messageMemory.insertMessage(message);
    BOOST_REQUIRE(messageMemory.list().size() == 1);
    BOOST_CHECK(messageMemory.list().at(0).durationPersistence() == 1);
    BOOST_CHECK(messageMemory.list().at(0).eventListEntry.nature() == 'F');

    /* different direction */
    message.setDurationPersistence(2);
    messageMemory.insertMessage(message);
    BOOST_REQUIRE(messageMemory.list().size() == 2);
    BOOST_CHECK(messageMemory.list().at(0).durationPersistence() == 1);
    BOOST_CHECK(messageMemory.list().at(0).eventListEntry.nature() == 'F');
    BOOST_CHECK(messageMemory.list().at(1).durationPersistence() == 2);
    BOOST_CHECK(messageMemory.list().at(1).eventListEntry.nature() == 'F');
}

/**
 * Test update of messages and drop after persistence time.
 */
BOOST_AUTO_TEST_CASE(UpdateMessagesPersistenceTime)
{
    ISO14819::MessageMemory messageMemory;
    BOOST_CHECK(messageMemory.list().size() == 0);

    /* set clock */
    ISO62106::RdsTime currentTime;
    currentTime.setModifiedJulianDay(20000);
    currentTime.setTime(12, 0); // 12:00

    /* insert message */
    ISO14819::Message message;
    message.receiveTime = currentTime;
    message.systemInformation.setLocationTableNumber(1);
    message.systemInformation.setServiceId(1);
    message.setEvent(1);
    message.setLocation(1);
    message.setDurationPersistence(1); // 15 minutes
    messageMemory.insertMessage(message);
    BOOST_REQUIRE(messageMemory.list().size() == 1);

    /* update at 12:14 */
    currentTime.setTime(12, 14);
    messageMemory.updateMessages(currentTime);
    BOOST_REQUIRE(messageMemory.list().size() == 1);

    /* update at 12:15 => delete message */
    currentTime.setTime(12, 15);
    messageMemory.updateMessages(currentTime);
    BOOST_REQUIRE(messageMemory.list().size() == 0);
}

/**
 * Test update of messages and drop after explicit stop time.
 */
BOOST_AUTO_TEST_CASE(UpdateMessagesStopTime)
{
    ISO14819::MessageMemory messageMemory;
    BOOST_CHECK(messageMemory.list().size() == 0);

    /* set clock */
    ISO62106::RdsTime currentTime;
    currentTime.setModifiedJulianDay(20000);
    currentTime.setTime(12, 0); // 12:00

    /* set stop time */
    ISO62106::RdsTime stopTime;
    stopTime = currentTime;
    stopTime.setTime(12, 10); // 12:10

    /* insert message */
    ISO14819::Message message;
    message.receiveTime = currentTime;
    message.systemInformation.setLocationTableNumber(1);
    message.systemInformation.setServiceId(1);
    message.setEvent(1);
    message.setLocation(1);
    message.setDurationPersistence(1); // 15 minutes
    message.stopTime = stopTime;
    messageMemory.insertMessage(message);
    BOOST_REQUIRE(messageMemory.list().size() == 1);

    /* update at 12:9 */
    currentTime.setTime(12, 9);
    messageMemory.updateMessages(currentTime);
    BOOST_REQUIRE(messageMemory.list().size() == 1);

    /* update at 12:10 => delete message */
    currentTime.setTime(12, 10);
    messageMemory.updateMessages(currentTime);
    BOOST_REQUIRE(messageMemory.list().size() == 0);
}

/**
 * Test update of messages and update of decrement time.
 */
BOOST_AUTO_TEST_CASE(UpdateMessagesDecrementTime)
{
    ISO14819::MessageMemory messageMemory;
    BOOST_CHECK(messageMemory.list().size() == 0);

    /* set clock */
    ISO62106::RdsTime currentTime;
    currentTime.setModifiedJulianDay(20000);
    currentTime.setTime(12, 0); // 12:00

    /* insert message */
    ISO14819::Message message;
    message.receiveTime = currentTime;
    message.systemInformation.setLocationTableNumber(1);
    message.systemInformation.setServiceId(1);
    message.setEvent(1);
    message.setLocation(1);
    message.setDurationPersistence(6); // 4 hours persistence time
    messageMemory.insertMessage(message);
    BOOST_REQUIRE(messageMemory.list().size() == 1);
    BOOST_CHECK(messageMemory.list().at(0).durationPersistence() == 6);

    /* update at 12:59 */
    currentTime.setTime(12, 59);
    messageMemory.updateMessages(currentTime);
    BOOST_REQUIRE(messageMemory.list().size() == 1);
    BOOST_CHECK(messageMemory.list().at(0).durationPersistence() == 6);

    /* update at 13:00 => decrement persistence */
    currentTime.setTime(13, 0);
    messageMemory.updateMessages(currentTime);
    BOOST_REQUIRE(messageMemory.list().size() == 1);
    BOOST_CHECK(messageMemory.list().at(0).durationPersistence() == 5);
}

/**
 * Test acceptance of a test message.
 */
BOOST_AUTO_TEST_CASE(TestMessage)
{
    ISO14819::MessageMemory messageMemory;
    BOOST_CHECK(messageMemory.list().size() == 0);

    /* set clock */
    ISO62106::RdsTime currentTime;
    currentTime.setModifiedJulianDay(20000);
    currentTime.setTime(12, 0); // 12:00

    /* insert message */
    ISO14819::Message message;
    message.setApplicationId(ISO14819::TrafficMessageChannel::applicationIdTest);
    BOOST_CHECK(message.applicationId() == ISO14819::TrafficMessageChannel::applicationIdTest);
    BOOST_CHECK(message.testMessage());
    message.receiveTime = currentTime;
    message.systemInformation.setLocationTableNumber(1);
    message.systemInformation.setServiceId(1);
    message.setEvent(1);
    message.setLocation(1);
    BOOST_CHECK(messageMemory.acceptTestMessages() == false);
    messageMemory.insertMessage(message);
    BOOST_REQUIRE(messageMemory.list().size() == 0);

    /* correct message */
    messageMemory.setAcceptTestMessages(true);
    BOOST_CHECK(messageMemory.acceptTestMessages() == true);
    messageMemory.insertMessage(message);
    BOOST_REQUIRE(messageMemory.list().size() == 1);
}
