/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#define BOOST_TEST_MODULE LocationCodeList
#if !defined(WIN32)
#define BOOST_TEST_DYN_LINK
#endif
#include <boost/test/unit_test.hpp>

#include "ISO/14819/LocationCodeList.h"
#include "ISO/14819/LocationCodeLists.h"

/**
 * Check location codes from German LCL.
 */
BOOST_AUTO_TEST_CASE(LocationCodeList)
{
    ISO14819::LocationCodeList locationCodeList(58, 1);

    /* 1 COUNTRIES */
    std::vector<ISO14819::LocationCodeList::Country> countries = locationCodeList.countries();
    BOOST_REQUIRE(countries.size() == 1);
    BOOST_REQUIRE(countries[0].countryId == 58);
    BOOST_CHECK(countries[0].extendedCountryCode == 0xE0);
    BOOST_CHECK(countries[0].countryCode == 0xD);
    BOOST_CHECK(countries[0].name == "Germany");

    /* 2 LOCATIONDATASETS */
    std::vector<ISO14819::LocationCodeList::LocationDataSet> locationDataSets = locationCodeList.locationDataSets();
    BOOST_REQUIRE(locationDataSets.size() == 1);
    BOOST_REQUIRE(locationDataSets[0].countryId == 58);
    BOOST_REQUIRE(locationDataSets[0].tableCode == 1);
    BOOST_CHECK(locationDataSets[0].comment == "");
    BOOST_CHECK(locationDataSets[0].version == "15.1");
    BOOST_CHECK(locationDataSets[0].versionDescription == "Release BASt LCL15.1.D");

    /* 3 LOCATIONCODES */
    ISO14819::LocationCodeList::LocationCode locationCode = locationCodeList.locationCode(58, 1, 63487);
    BOOST_REQUIRE(locationCode.countryId == 58);
    BOOST_REQUIRE(locationCode.tableCode == 1);
    BOOST_REQUIRE(locationCode.locationCode == 63487);
    BOOST_CHECK(locationCode.allocated == false);

    /* 4 CLASSES */
    std::vector<ISO14819::LocationCodeList::Class> classes = locationCodeList.classes();
    BOOST_REQUIRE(classes.size() == 3);
    BOOST_CHECK(classes[0].typeClass == 'A');
    BOOST_CHECK(classes[1].typeClass == 'L');
    BOOST_CHECK(classes[2].typeClass == 'P');

    /* 5 TYPES */
    ISO14819::LocationCodeList::Type type = locationCodeList.type('P', 6);
    BOOST_REQUIRE(type.typeClass == 'P');
    BOOST_REQUIRE(type.typeCode == 6);
    BOOST_CHECK(type.typeDescription == "Other isolated POI");
    BOOST_CHECK(type.nationalTypeCode == "6");
    BOOST_CHECK(type.nationalTypeDescription == "Sonstige isolierte POI");

    /* 6 SUBTYPES */
    ISO14819::LocationCodeList::Subtype subType = locationCodeList.subtype('P', 6, 14);
    BOOST_REQUIRE(subType.typeClass == 'P');
    BOOST_REQUIRE(subType.typeCode == 6);
    BOOST_REQUIRE(subType.subtypeCode == 14);
    BOOST_CHECK(subType.subtypeDescription == "Place name");
    BOOST_CHECK(subType.nationalSubtypeCode == "14");
    BOOST_CHECK(subType.nationalSubtypeDescription == "Benannter Punkt");

    /* 7 LANGUAGES */
    std::vector<ISO14819::LocationCodeList::Language> languages = locationCodeList.languages();
    BOOST_REQUIRE(languages.size() == 1);
    BOOST_REQUIRE(languages[0].countryId == 58);
    BOOST_CHECK(languages[0].languageId == 1);
    BOOST_CHECK(languages[0].language == "German");

    /* 8 EUROROADNO */
    ISO14819::LocationCodeList::EuroRoadNo euroRoadNo = locationCodeList.euroRoadNo(""); // @todo no data
    BOOST_REQUIRE(euroRoadNo.europeanRoadNumber == "");
    BOOST_REQUIRE(euroRoadNo.comment == "");

    /* 9 NAMES */
    ISO14819::LocationCodeList::Name name = locationCodeList.name(58, 1, 30291);
    BOOST_REQUIRE(name.countryId == 58);
    BOOST_REQUIRE(name.languageId == 1);
    BOOST_REQUIRE(name.nameId == 30291);
    BOOST_CHECK(name.name == "Ürzig");
    BOOST_CHECK(name.comment == "");

    /* 10 NAMETRANSLATIONS */
    ISO14819::LocationCodeList::NameTranslation nameTranslation = locationCodeList.nameTranslation(0, 0); // @todo no data
    BOOST_REQUIRE(nameTranslation.countryId == 0);
    BOOST_CHECK(nameTranslation.languageId == 0);
    BOOST_REQUIRE(nameTranslation.nameId == 0);
    BOOST_CHECK(nameTranslation.translation == "");

    /* 11 SUBTYPETRANSLATIONS */
    ISO14819::LocationCodeList::SubTypeTranslation subTypeTranslation = locationCodeList.subTypeTranslation(0, 0, 0, 0, 0); // @todo no data
    BOOST_REQUIRE(subTypeTranslation.countryId == 0);
    BOOST_REQUIRE(subTypeTranslation.languageId == 0);
    BOOST_REQUIRE(subTypeTranslation.typeClass == 0);
    BOOST_REQUIRE(subTypeTranslation.typeCode == 0);
    BOOST_REQUIRE(subTypeTranslation.subtypeCode == 0);
    BOOST_REQUIRE(subTypeTranslation.translation == "");

    /* 12 ERNO_BELONGS_TO_CO */
    std::vector<ISO14819::LocationCodeList::EuroRoadNoBelongsToCountry> euroRoadNoBelongsToCountries = locationCodeList.euroRoadNoBelongsToCountries(""); // @todo no data
    BOOST_REQUIRE(euroRoadNoBelongsToCountries.size() == 0);
    ISO14819::LocationCodeList::EuroRoadNoBelongsToCountry euroRoadNoBelongsToCountry; // @todo no data, hence we create some
    euroRoadNoBelongsToCountries.push_back(euroRoadNoBelongsToCountry);
    BOOST_CHECK(euroRoadNoBelongsToCountries[0].countryId == 0);
    BOOST_CHECK(euroRoadNoBelongsToCountries[0].europeanRoadNumber == "");

    /* 13 ADMINISTRATIVEAREA */
    ISO14819::LocationCodeList::AdministrativeArea administrativeArea = locationCodeList.administrativeArea(58, 1, 60823);
    BOOST_REQUIRE(administrativeArea.countryId == 58);
    BOOST_REQUIRE(administrativeArea.tableCode == 1);
    BOOST_REQUIRE(administrativeArea.locationCode == 60823);
    BOOST_CHECK(administrativeArea.typeClass == 'A');
    BOOST_CHECK(administrativeArea.typeCode == 11);
    BOOST_CHECK(administrativeArea.subtypeCode == 0);
    BOOST_CHECK(administrativeArea.name == 13193);
    BOOST_CHECK(administrativeArea.upwardAreaReference == 2219);

    /* 14 OTHERAREAS */
    ISO14819::LocationCodeList::OtherArea otherArea = locationCodeList.otherArea(58, 1, 60759);
    BOOST_REQUIRE(otherArea.countryId == 58);
    BOOST_REQUIRE(otherArea.tableCode == 1);
    BOOST_REQUIRE(otherArea.locationCode == 60759);
    BOOST_CHECK(otherArea.typeClass == 'A');
    BOOST_CHECK(otherArea.typeCode == 5);
    BOOST_CHECK(otherArea.subtypeCode == 2);
    BOOST_CHECK(otherArea.name1 == 23313);
    BOOST_CHECK(otherArea.adminAreaReference == 633);

    /* 15 ROADS */
    ISO14819::LocationCodeList::Road road = locationCodeList.road(58, 1, 61454);
    BOOST_REQUIRE(road.countryId == 58);
    BOOST_REQUIRE(road.tableCode == 1);
    BOOST_REQUIRE(road.locationCode == 61454);
    BOOST_CHECK(road.typeClass == 'L');
    BOOST_CHECK(road.typeCode == 2);
    BOOST_CHECK(road.subtypeCode == 2);
    BOOST_CHECK(road.roadNumber == "GFS1R");
    BOOST_CHECK(road.roadName == 19848);
    BOOST_CHECK(road.name1 == 0);
    BOOST_CHECK(road.name2 == 0);
    BOOST_CHECK(road.adminAreaReference == 13829);
    BOOST_CHECK(road.roadNetworkLevel == 0);

    /* 16 ROAD_NETWORK_LEVEL_TYPES */
    ISO14819::LocationCodeList::RoadNetworkLevelType roadNetworkLevelType = locationCodeList.roadNetworkLevelType(0); // @todo no data
    BOOST_REQUIRE(roadNetworkLevelType.roadNetworkLevel == 0);
    BOOST_CHECK(roadNetworkLevelType.roadNetworkLevelDescription == "");
    BOOST_CHECK(roadNetworkLevelType.nationalRoadNetworkLevelDescription == "");

    /* 17 SEGMENTS */
    ISO14819::LocationCodeList::Segment segment = locationCodeList.segment(58, 1, 61362);
    BOOST_REQUIRE(segment.countryId == 58);
    BOOST_REQUIRE(segment.tableCode == 1);
    BOOST_REQUIRE(segment.locationCode == 61362);
    BOOST_CHECK(segment.typeClass == 'L');
    BOOST_CHECK(segment.typeCode == 3);
    BOOST_CHECK(segment.subtypeCode == 0);
    BOOST_CHECK(segment.roadNumber == "S2384");
    BOOST_CHECK(segment.roadName == 0);
    BOOST_CHECK(segment.name1 == 21792);
    BOOST_CHECK(segment.name2 == 11602);
    BOOST_CHECK(segment.roadReference == 13805);
    BOOST_CHECK(segment.segmentReference == 0);
    BOOST_CHECK(segment.adminAreaReference == 705);

    /* 18 SOFFSETS */
    ISO14819::LocationCodeList::SOffset sOffset = locationCodeList.sOffset(58, 1, 61362);
    BOOST_REQUIRE(sOffset.countryId == 58);
    BOOST_REQUIRE(sOffset.tableCode == 1);
    BOOST_REQUIRE(sOffset.locationCode == 61362);
    BOOST_CHECK(sOffset.negativeOffset == 13806);
    BOOST_CHECK(sOffset.positiveOffset == 0);

    /* 19 SEG_HAS_ERNO */
    ISO14819::LocationCodeList::SegHasEuroRoadNo segHasEuroRoadNo = locationCodeList.segHasEuroRoadNo(0, 0, 0); // @todo no data
    BOOST_REQUIRE(segHasEuroRoadNo.countryId == 0);
    BOOST_REQUIRE(segHasEuroRoadNo.tableCode == 0);
    BOOST_REQUIRE(segHasEuroRoadNo.locationCode == 0);
    BOOST_CHECK(segHasEuroRoadNo.europeanRoadNumber == "");

    /* 20 POINTS */
    ISO14819::LocationCodeList::Point point = locationCodeList.point(58, 1, 61455);
    BOOST_REQUIRE(point.countryId == 58);
    BOOST_REQUIRE(point.tableCode == 1);
    BOOST_REQUIRE(point.locationCode == 61455);
    BOOST_CHECK(point.typeClass == 'P');
    BOOST_CHECK(point.typeCode == 1);
    BOOST_CHECK(point.subtypeCode == 15);
    BOOST_CHECK(point.junctionNumber == "");
    BOOST_CHECK(point.roadName == 29939);
    BOOST_CHECK(point.name1 == 523);
    BOOST_CHECK(point.name2 == 0);
    BOOST_CHECK(point.adminAreaReference == 13829);
    BOOST_CHECK(point.otherAreaReference == 0);
    BOOST_CHECK(point.segmentReference == 0);
    BOOST_CHECK(point.roadReference == 61454);
    BOOST_CHECK(point.inPos == 1);
    BOOST_CHECK(point.inNeg == 1);
    BOOST_CHECK(point.outPos == 1);
    BOOST_CHECK(point.outNeg == 1);
    BOOST_CHECK(point.presentPos == 1);
    BOOST_CHECK(point.presentNeg == 1);
    BOOST_CHECK(point.diversionPos == "");
    BOOST_CHECK(point.diversionNeg == "");
    BOOST_CHECK(point.xCoord == "+01177120");
    BOOST_CHECK(point.yCoord == "+4835320");
    BOOST_CHECK(point.interruptsRoad == 0);
    BOOST_CHECK(point.urban == true);

    /* 21 POFFSETS */
    ISO14819::LocationCodeList::POffset pOffset = locationCodeList.pOffset(58, 1, 61455);
    BOOST_REQUIRE(pOffset.countryId == 58);
    BOOST_REQUIRE(pOffset.tableCode == 1);
    BOOST_REQUIRE(pOffset.locationCode == 61455);
    BOOST_CHECK(pOffset.negativeOffset == 61084);
    BOOST_CHECK(pOffset.positiveOffset == 61081);

    /* 22 INTERSECTIONS */
    ISO14819::LocationCodeList::Intersection intersection = locationCodeList.intersection(58, 1, 61455);
    BOOST_REQUIRE(intersection.countryId == 58);
    BOOST_REQUIRE(intersection.tableCode == 1);
    BOOST_REQUIRE(intersection.locationCode == 61455);
    BOOST_CHECK(intersection.intersectionCountryId == 58);
    BOOST_CHECK(intersection.intersectionTableCode == 1);
    BOOST_CHECK(intersection.intersectionLocationCode == 29618);

    /* View TYPESUNION */
    ISO14819::LocationCodeList::TypeUnion typeUnion = locationCodeList.typeUnion('P', 6, 14);
    BOOST_REQUIRE(typeUnion.typeClass == 'P');
    BOOST_REQUIRE(typeUnion.typeCode == 6);
    BOOST_REQUIRE(typeUnion.subtypeCode == 14);
    BOOST_CHECK(typeUnion.description == "Place name");
    BOOST_CHECK(typeUnion.nationalCode == "14");
    BOOST_CHECK(typeUnion.nationalDescription == "Benannter Punkt");

    /* View LOCATIONSUNION */
    ISO14819::LocationCodeList::LocationUnion locationUnion = locationCodeList.locationUnion(58, 1, 61455);
    BOOST_REQUIRE(locationUnion.countryId == 58);
    BOOST_REQUIRE(locationUnion.tableCode == 1);
    BOOST_REQUIRE(locationUnion.locationCode == 61455);
    BOOST_CHECK(locationUnion.typeClass == 'P');
    BOOST_CHECK(locationUnion.typeCode == 1);
    BOOST_CHECK(locationUnion.subtypeCode == 15);
    BOOST_CHECK(locationUnion.number == "");
    BOOST_CHECK(locationUnion.roadName == 29939);
    BOOST_CHECK(locationUnion.name1 == 523);
    BOOST_CHECK(locationUnion.name2 == 0);
    BOOST_CHECK(locationUnion.adminAreaReference == 13829);
    BOOST_CHECK(locationUnion.otherAreaReference == 0);
    BOOST_CHECK(locationUnion.segmentReference == 0);
    BOOST_CHECK(locationUnion.roadReference == 61454);
    BOOST_CHECK(locationUnion.urban == true);

    /* View OFFSETSUNION */
    ISO14819::LocationCodeList::OffsetUnion offsetUnion = locationCodeList.offsetUnion(58, 1, 61455);
    BOOST_REQUIRE(offsetUnion.countryId == 58);
    BOOST_REQUIRE(offsetUnion.tableCode == 1);
    BOOST_REQUIRE(offsetUnion.locationCode == 61455);
    BOOST_CHECK(offsetUnion.negativeOffset == 61084);
    BOOST_CHECK(offsetUnion.positiveOffset == 61081);
}

/**
 * Tests that the origin function works with some known data.
 */
BOOST_AUTO_TEST_CASE(Origin)
{
    ISO14819::LocationCodeList locationCodeList(58, 1);
    ISO14819::LocationCodeList::LocationUnion locationUnion;

    /* A1 -> Administrative Area */
    locationUnion = locationCodeList.locationUnion(58, 1, 34196);
    BOOST_CHECK(locationUnion.typeClass == 'A');
    BOOST_CHECK(locationUnion.typeCode == 1);
    BOOST_CHECK(locationUnion.origin() == 'A');

    /* A2 -> not given in 58,1 */
    // BOOST_CHECK(locationUnion.typeClass == 'A');
    // BOOST_CHECK(locationUnion.typeCode == 2);

    /* A3 -> Administrative Area */
    locationUnion = locationCodeList.locationUnion(58, 1, 13);
    BOOST_CHECK(locationUnion.typeClass == 'A');
    BOOST_CHECK(locationUnion.typeCode == 3);
    BOOST_CHECK(locationUnion.origin() == 'A');

    /* A4 -> is not defined */
    // BOOST_CHECK(locationUnion.typeClass == 'A');
    // BOOST_CHECK(locationUnion.typeCode == 4);

    /* A5 -> Administrative Area */
    locationUnion = locationCodeList.locationUnion(58, 1, 60759);
    BOOST_CHECK(locationUnion.typeClass == 'A');
    BOOST_CHECK(locationUnion.typeCode == 5);
    BOOST_CHECK(locationUnion.origin() == 'A');

    /* A6 -> Other Area */
    locationUnion = locationCodeList.locationUnion(58, 1, 55965);
    BOOST_CHECK(locationUnion.typeClass == 'A');
    BOOST_CHECK(locationUnion.typeCode == 6);
    BOOST_CHECK(locationUnion.origin() == 'O');

    /* A7 -> Administrative Area */
    locationUnion = locationCodeList.locationUnion(58, 1, 271);
    BOOST_CHECK(locationUnion.typeClass == 'A');
    BOOST_CHECK(locationUnion.typeCode == 7);
    BOOST_CHECK(locationUnion.origin() == 'A');

    /* A8 -> Administrative Area */
    locationUnion = locationCodeList.locationUnion(58, 1, 303);
    BOOST_CHECK(locationUnion.typeClass == 'A');
    BOOST_CHECK(locationUnion.typeCode == 8);
    BOOST_CHECK(locationUnion.origin() == 'A');

    /* A9 -> Administrative Area */
    locationUnion = locationCodeList.locationUnion(58, 1, 58413);
    BOOST_CHECK(locationUnion.typeClass == 'A');
    BOOST_CHECK(locationUnion.typeCode == 9);
    BOOST_CHECK(locationUnion.origin() == 'A');

    /* A10 -> Administrative Area */
    locationUnion = locationCodeList.locationUnion(58, 1, 61456);
    BOOST_CHECK(locationUnion.typeClass == 'A');
    BOOST_CHECK(locationUnion.typeCode == 10);
    BOOST_CHECK(locationUnion.origin() == 'A');

    /* A11 -> Administrative Area */
    locationUnion = locationCodeList.locationUnion(58, 1, 60823);
    BOOST_CHECK(locationUnion.typeClass == 'A');
    BOOST_CHECK(locationUnion.typeCode == 11);
    BOOST_CHECK(locationUnion.origin() == 'A');

    /* A12 -> Other Area */
    locationUnion = locationCodeList.locationUnion(58, 1, 55728);
    BOOST_CHECK(locationUnion.typeClass == 'A');
    BOOST_CHECK(locationUnion.typeCode == 12);
    BOOST_CHECK(locationUnion.origin() == 'O');

    /* L1 -> Road */
    locationUnion = locationCodeList.locationUnion(58, 1, 61446);
    BOOST_CHECK(locationUnion.typeClass == 'L');
    BOOST_CHECK(locationUnion.typeCode == 1);
    BOOST_CHECK(locationUnion.origin() == 'R');

    /* L2 -> Road */
    locationUnion = locationCodeList.locationUnion(58, 1, 61454);
    BOOST_CHECK(locationUnion.typeClass == 'L');
    BOOST_CHECK(locationUnion.typeCode == 2);
    BOOST_CHECK(locationUnion.origin() == 'R');

    /* L3 -> Segment */
    locationUnion = locationCodeList.locationUnion(58, 1, 61362);
    BOOST_CHECK(locationUnion.typeClass == 'L');
    BOOST_CHECK(locationUnion.typeCode == 3);
    BOOST_CHECK(locationUnion.origin() == 'S');

    /* L4 -> not given in 58,1 */
    // BOOST_CHECK(locationUnion.typeClass == 'L');
    // BOOST_CHECK(locationUnion.typeCode == 4);

    /* L5 -> Road */
    locationUnion = locationCodeList.locationUnion(58, 1, 61450);
    BOOST_CHECK(locationUnion.typeClass == 'L');
    BOOST_CHECK(locationUnion.typeCode == 5);
    BOOST_CHECK(locationUnion.origin() == 'R');

    /* L6 -> Road */
    locationUnion = locationCodeList.locationUnion(58, 1, 53094);
    BOOST_CHECK(locationUnion.typeClass == 'L');
    BOOST_CHECK(locationUnion.typeCode == 6);
    BOOST_CHECK(locationUnion.origin() == 'R');

    /* L7 -> not given in 58,1 */
    // BOOST_CHECK(locationUnion.typeClass == 'L');
    // BOOST_CHECK(locationUnion.typeCode == 7);

    /* L8 -> not given in 58,1 */
    // BOOST_CHECK(locationUnion.typeClass == 'L');
    // BOOST_CHECK(locationUnion.typeCode == 8);

    /* P1 -> Point */
    locationUnion = locationCodeList.locationUnion(58, 1, 61455);
    BOOST_CHECK(locationUnion.typeClass == 'P');
    BOOST_CHECK(locationUnion.typeCode == 1);
    BOOST_CHECK(locationUnion.origin() == 'P');

    /* P2 -> Point */
    locationUnion = locationCodeList.locationUnion(58, 1, 47273);
    BOOST_CHECK(locationUnion.typeClass == 'P');
    BOOST_CHECK(locationUnion.typeCode == 2);
    BOOST_CHECK(locationUnion.origin() == 'P');

    /* P3 -> Point */
    locationUnion = locationCodeList.locationUnion(58, 1, 61452);
    BOOST_CHECK(locationUnion.typeClass == 'P');
    BOOST_CHECK(locationUnion.typeCode == 3);
    BOOST_CHECK(locationUnion.origin() == 'P');

    /* P4 -> not given in 58,1 */
    // BOOST_CHECK(locationUnion.typeClass == 'P');
    // BOOST_CHECK(locationUnion.typeCode == 4);

    /* P5 -> Point */
    locationUnion = locationCodeList.locationUnion(58, 1, 61078);
    BOOST_CHECK(locationUnion.typeClass == 'P');
    BOOST_CHECK(locationUnion.typeCode == 5);
    BOOST_CHECK(locationUnion.origin() == 'P');

    /* P6 -> Point */
    locationUnion = locationCodeList.locationUnion(58, 1, 60822);
    BOOST_CHECK(locationUnion.typeClass == 'P');
    BOOST_CHECK(locationUnion.typeCode == 6);
    BOOST_CHECK(locationUnion.origin() == 'P');
}

/**
 * Test the LocationCodeLists class.
 */
BOOST_AUTO_TEST_CASE(LocationCodeLists)
{
    ISO14819::LocationCodeList & lcl1 = ISO14819::locationCodeLists().locationCodeList(58, 1);
    BOOST_CHECK(lcl1.locationDataSets().at(0).version == "15.1");

    uint16_t countryId = ISO14819::locationCodeLists().countryId(0xE0, 0xD);
    BOOST_CHECK(countryId == 58);

    ISO14819::LocationCodeList & lcl2 = ISO14819::locationCodeLists().locationCodeList(0xE0, 0xD, 1);
    BOOST_CHECK(lcl2.locationDataSets().at(0).version == "15.1");
}

/**
 * Test reaction to failed load of LocationCodeList.
 */
BOOST_AUTO_TEST_CASE(LocationCodeList_Load_Failed)
{
    ISO14819::LocationCodeList lcl1(1, 1);
    BOOST_CHECK(lcl1.locationDataSets().size() == 0);

    ISO14819::LocationCodeList & lcl2 = ISO14819::locationCodeLists().locationCodeList(1, 1); // unable to load, go back to 0,0
    BOOST_CHECK(lcl2.classes().size() == 3);

    uint16_t countryId = ISO14819::locationCodeLists().countryId(0x01, 0x1);
    BOOST_CHECK(countryId == 0);
}
