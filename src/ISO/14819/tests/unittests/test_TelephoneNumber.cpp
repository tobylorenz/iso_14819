/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#define BOOST_TEST_MODULE TelephoneNumber
#if !defined(WIN32)
#define BOOST_TEST_DYN_LINK
#endif
#include <boost/test/unit_test.hpp>

#include "ISO/14819/Message.h"
#include "ISO/14819/TelephoneNumber.h"

/**
 * Example from the specification.
 */
BOOST_AUTO_TEST_CASE(specExample)
{
    ISO14819::Message message;
    message.pushFreeFormat( 15,  4); // label = 15
    message.pushFreeFormat(  1,  6); // sublabel = 1

    /* 555-TRAFFIC */
    message.pushFreeFormat(  5,  4); // 5
    message.pushFreeFormat(  5,  4); // 5
    message.pushFreeFormat(  5,  4); // 5
    message.pushFreeFormat( 13,  4); // <alpha>
    message.pushFreeFormat( 28,  5); // -
    message.pushFreeFormat( 20,  5); // T
    message.pushFreeFormat( 18,  5); // R
    message.pushFreeFormat(  1,  5); // A
    message.pushFreeFormat(  6,  5); // F
    message.pushFreeFormat(  6,  5); // F
    message.pushFreeFormat(  9,  5); // I
    message.pushFreeFormat(  3,  5); // C
    message.pushFreeFormat( 31,  5); // <end>

    /* Time unit of call cost */
    message.pushFreeFormat(  2,  3); // per min

    /* Numerical value of call cost */
    message.pushFreeFormat(  2,  2); // :100
    message.pushFreeFormat(120, 14); // 120
    message.pushFreeFormat(  1,  1); // in front

    /* Currency lookup reference */
    message.pushFreeFormat( 49,  8); // "GBR" or "£"

    /* tests */
    BOOST_REQUIRE(message.optionalMessageContent().size() == 1);
    BOOST_REQUIRE(message.optionalMessageContent().at(0).label() == 15);
    BOOST_REQUIRE(message.optionalMessageContent().at(0).data() == 1);
    BOOST_CHECK(message.telNumInformation.telephoneNumber() == "555-TRAFFIC");
    BOOST_CHECK(message.telNumInformation.ivrOptionNumber() == "");
    BOOST_CHECK(message.telNumInformation.timeUnit() == ISO14819::TelephoneNumber::TimeUnit::TimePerMinute);
    BOOST_CHECK(message.telNumInformation.cost() == 120);
    BOOST_CHECK(message.telNumInformation.decimalMultiplier() == 2);
    BOOST_CHECK(message.telNumInformation.currencyInFront() == true);
    BOOST_CHECK(message.telNumInformation.currencyReference() == 49);
}

/**
 * More detailed test for full coverage of TelephoneNumber class.
 */
BOOST_AUTO_TEST_CASE(DetailedNumber)
{
    ISO14819::Message message;
    message.pushFreeFormat( 15,  4); // label = 15
    message.pushFreeFormat(  2,  6); // sublabel = 2

    /* 555-TRAFFIC */
    message.pushFreeFormat(  1,  4); // 1
    message.pushFreeFormat(  2,  4); // 2
    message.pushFreeFormat(  3,  4); // 3
    message.pushFreeFormat( 13,  4); // <alpha>
    message.pushFreeFormat( 27,  5); // <space>
    message.pushFreeFormat(  1,  5); // A
    message.pushFreeFormat(  2,  5); // B
    message.pushFreeFormat(  3,  5); // C
    message.pushFreeFormat( 27,  5); // <space>
    message.pushFreeFormat(  0,  5); // <switch to numbers>
    message.pushFreeFormat(  4,  4); // 4
    message.pushFreeFormat(  5,  4); // 5
    message.pushFreeFormat(  6,  4); // 6
    message.pushFreeFormat( 14,  4); // <option numbers follow>
    message.pushFreeFormat(  1,  4); // 1
    message.pushFreeFormat(  2,  4); // 2
    message.pushFreeFormat( 13,  4); // <alpha>
    message.pushFreeFormat( 27,  5); // <space>
    message.pushFreeFormat( 28,  5); // "-" (dash)
    message.pushFreeFormat( 27,  5); // <space>
    message.pushFreeFormat(  4,  5); // D
    message.pushFreeFormat(  5,  5); // E
    message.pushFreeFormat(  6,  5); // F
    message.pushFreeFormat( 29,  5); // <option letters follow>
    message.pushFreeFormat(  7,  5); // G
    message.pushFreeFormat( 30,  5); // <option numbers follow>
    message.pushFreeFormat(  3,  4); // 3
    message.pushFreeFormat( 15,  4); // <end>

    /* Time unit of call cost */
    message.pushFreeFormat(  2,  3); // per min

    /* Numerical value of call cost */
    message.pushFreeFormat(  2,  2); // :100
    message.pushFreeFormat(120, 14); // 120
    message.pushFreeFormat(  1,  1); // in front

    /* Currency lookup reference */
    message.pushFreeFormat( 49,  8); // "GBR" or "£"

    /* tests */
    BOOST_REQUIRE(message.optionalMessageContent().size() == 1);
    BOOST_REQUIRE(message.optionalMessageContent().at(0).label() == 15);
    BOOST_REQUIRE(message.optionalMessageContent().at(0).data() == 2);
    BOOST_CHECK(message.telNumReport.telephoneNumber() == "123 ABC 456");
    BOOST_CHECK(message.telNumReport.ivrOptionNumber() == "12 - DEFG3");
    BOOST_CHECK(message.telNumReport.timeUnit() == ISO14819::TelephoneNumber::TimeUnit::TimePerMinute);
    BOOST_CHECK(message.telNumReport.cost() == 120);
    BOOST_CHECK(message.telNumReport.decimalMultiplier() == 2);
    BOOST_CHECK(message.telNumReport.currencyInFront() == true);
    BOOST_CHECK(message.telNumReport.currencyReference() == 49);
}

/**
 * Only one currency is given as example. Test for it.
 */
BOOST_AUTO_TEST_CASE(Currency)
{
    BOOST_CHECK(ISO4217::currencies().at(49).alphabeticCode == "GBP");
    BOOST_CHECK(ISO4217::currencies().at(49).unicode == "£");
}

/**
 * Test getter/setters.
 */
BOOST_AUTO_TEST_CASE(GetSet)
{
    ISO14819::TelephoneNumber tel;

    /* Telephone number */
    BOOST_CHECK(tel.telephoneNumber() == "");
    tel.setTelephoneNumber("1234");
    BOOST_CHECK(tel.telephoneNumber() == "1234");

    /* IVR option number */
    BOOST_CHECK(tel.ivrOptionNumber() == "");
    tel.setIvrOptionNumber("1234");
    BOOST_CHECK(tel.ivrOptionNumber() == "1234");

    /* Time unit of call cost */
    BOOST_CHECK(tel.timeUnit() == 0);
    tel.setTimeUnit(7);
    BOOST_CHECK(tel.timeUnit() == 7);

    /* Cost */
    BOOST_CHECK(tel.cost() == 0);
    tel.setCost(0x3fff);
    BOOST_CHECK(tel.cost() == 0x3fff);

    /* Decimal Multiplier */
    BOOST_CHECK(tel.decimalMultiplier() == 0);
    tel.setDecimalMultiplier(3);
    BOOST_CHECK(tel.decimalMultiplier() == 3);

    /* Currency Position */
    BOOST_CHECK(tel.currencyInFront() == false);
    tel.setCurrencyInFront(true);
    BOOST_CHECK(tel.currencyInFront() == true);

    /* Currency Symbol (reference to ISO 4217:2008) */
    BOOST_CHECK(tel.currencyReference() == 0);
    tel.setCurrencyReference(0xff);
    BOOST_CHECK(tel.currencyReference() == 0xff);
}
