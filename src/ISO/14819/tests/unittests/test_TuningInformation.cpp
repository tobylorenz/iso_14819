/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#define BOOST_TEST_MODULE TuningInformation
#if !defined(WIN32)
#define BOOST_TEST_DYN_LINK
#endif
#include <boost/test/unit_test.hpp>
#include <iostream>

#include <ISO/62106.h>

#include "ISO/14819/TrafficMessageChannel.h"
#include "ISO/14819/TuningInformation.h"

/** RDS Receiver for the tests */
static ISO62106::RdsReceiver rdsReceiver;

/**
 * Prepare the environment
 */
BOOST_AUTO_TEST_CASE(TestInit)
{
    /* register ISO14819 handler */
    ISO14819::TrafficMessageChannel::registerHandler();

    /* register TMC on 8A */
    rdsReceiver.decode(0xD392, (3 << 12) | (0 << 11) | (8 << 1) | (0 << 0), (0 << 14) | (1 << 6), 0xCD46);
    BOOST_CHECK(ISO62106::rdsProgram(0xD392).openDataApplications.value().at(8).at(0) == 0xCD46);

    /* set a valid date */
    ISO62106::rdsProgram(0xD392).clockTime.decode(20000, 12, 0, 0, 0);
    BOOST_CHECK(ISO62106::rdsProgram(0xD392).clockTime.value().modifiedJulianDay() == 20000);
    BOOST_CHECK(ISO62106::rdsProgram(0xD392).clockTime.value().hour() == 12);
    BOOST_CHECK(ISO62106::rdsProgram(0xD392).clockTime.value().minute() == 0);
}

/**
 * Tuning Information - address 4+5 (Service Provider Name)
 */
BOOST_AUTO_TEST_CASE(TuningInformationServiceProviderName)
{
    /* 0x14: C1..4 */
    rdsReceiver.decode(0xD392, 0x8414, 0x4142, 0x4344);
    rdsReceiver.decode(0xD392, 0x8414, 0x4142, 0x4344);
    BOOST_CHECK(ISO14819::trafficMessageChannel(0xD392).tuningInformation.serviceProviderNameUtf8() == "ABCD");
    BOOST_CHECK(ISO14819::trafficMessageChannel(0xD392).tuningInformation.serviceProviderNameUcs2().length() == 4 + 1);

    /* 0x15: C5..8 */
    rdsReceiver.decode(0xD392, 0x8415, 0x4546, 0x4748);
    rdsReceiver.decode(0xD392, 0x8415, 0x4546, 0x4748);
    BOOST_CHECK(ISO14819::trafficMessageChannel(0xD392).tuningInformation.serviceProviderNameUtf8() == "ABCDEFGH");
    BOOST_CHECK(ISO14819::trafficMessageChannel(0xD392).tuningInformation.serviceProviderNameUcs2().length() == 8 + 1);
}

/**
 * Tuning Information - address 6
 */
BOOST_AUTO_TEST_CASE(TuningInformation6)
{
    /* 0x16: AF(ON), AF(ON), PI(ON) */
    /* IEC 62106 method A example 1 */
    rdsReceiver.decode(0xD392, 0x8416, ((224 + 5) << 8) | (1 << 0), 0xD394); // #5 AF1
    rdsReceiver.decode(0xD392, 0x8416, ((224 + 5) << 8) | (1 << 0), 0xD394);
    rdsReceiver.decode(0xD392, 0x8416, (2 << 8) | (3 << 0), 0xD395); // AF2 AF3
    rdsReceiver.decode(0xD392, 0x8416, (2 << 8) | (3 << 0), 0xD395);
    rdsReceiver.decode(0xD392, 0x8416, (4 << 8) | (5 << 0), 0xD396); // AF4 AF5
    rdsReceiver.decode(0xD392, 0x8416, (4 << 8) | (5 << 0), 0xD396);
    /* repeat */
    rdsReceiver.decode(0xD392, 0x8416, ((224 + 5) << 8) | (1 << 0), 0xD397); // #5 AF1
    rdsReceiver.decode(0xD392, 0x8416, ((224 + 5) << 8) | (1 << 0), 0xD397);
    rdsReceiver.decode(0xD392, 0x8416, (2 << 8) | (3 << 0), 0xD398); // AF2 AF3
    rdsReceiver.decode(0xD392, 0x8416, (2 << 8) | (3 << 0), 0xD398);
    rdsReceiver.decode(0xD392, 0x8416, (4 << 8) | (5 << 0), 0xD399); // AF4 AF5
    rdsReceiver.decode(0xD392, 0x8416, (4 << 8) | (5 << 0), 0xD399);

    /* check AF(ON) */
    std::vector<ISO62106::AlternativeFrequencies::MappedFrequency> mf = ISO14819::trafficMessageChannel(0xD392).tuningInformation.alternativeFrequencies.mappedFrequencies()[0];
    BOOST_REQUIRE(mf.size() == 5);
    BOOST_CHECK(mf[0].frequency == 87600);
    BOOST_CHECK(mf[0].sameProgramme == true);
    BOOST_CHECK(mf[1].frequency == 87700);
    BOOST_CHECK(mf[1].sameProgramme == true);
    BOOST_CHECK(mf[2].frequency == 87800);
    BOOST_CHECK(mf[2].sameProgramme == true);
    BOOST_CHECK(mf[3].frequency == 87900);
    BOOST_CHECK(mf[3].sameProgramme == true);
    BOOST_CHECK(mf[4].frequency == 88000);
    BOOST_CHECK(mf[4].sameProgramme == true);

    /* check PI(ON) */
    BOOST_CHECK(ISO14819::trafficMessageChannel(0xD392).tuningInformation.programmeIdListAri0.piCodes().size() == 6);
}

/**
 * Tuning Information - address 7
 */
BOOST_AUTO_TEST_CASE(TuningInformation7)
{
    /* 0x17: TF(TN), MF(ON), PI(ON) */
    rdsReceiver.decode(0xD392, 0x8417, (1 << 8) | (2 << 0), 0xD397);
    rdsReceiver.decode(0xD392, 0x8417, (1 << 8) | (2 << 0), 0xD397);
    rdsReceiver.decode(0xD392, 0x8417, (3 << 8) | (4 << 0), 0xD397);
    rdsReceiver.decode(0xD392, 0x8417, (3 << 8) | (4 << 0), 0xD397);

    /* check */
    std::map<uint8_t, std::vector<uint8_t>> mappedFrequencies = ISO14819::trafficMessageChannel(0xD392).tuningInformation.mappedFrequencies();
    BOOST_REQUIRE(mappedFrequencies.size() == 2);
    BOOST_REQUIRE(mappedFrequencies[1].size() == 1);
    BOOST_CHECK(mappedFrequencies[1][0] == 2);
    BOOST_REQUIRE(mappedFrequencies[3].size() == 1);
    BOOST_CHECK(mappedFrequencies[3][0] == 4);

    /* repeat without second group */
    rdsReceiver.decode(0xD392, 0x8417, (1 << 8) | (2 << 0), 0xD397);
    rdsReceiver.decode(0xD392, 0x8417, (1 << 8) | (2 << 0), 0xD397);
    mappedFrequencies = ISO14819::trafficMessageChannel(0xD392).tuningInformation.mappedFrequencies();
    BOOST_CHECK(mappedFrequencies.size() == 2);
    rdsReceiver.decode(0xD392, 0x8417, (1 << 8) | (2 << 0), 0xD397);
    rdsReceiver.decode(0xD392, 0x8417, (1 << 8) | (2 << 0), 0xD397);
    mappedFrequencies = ISO14819::trafficMessageChannel(0xD392).tuningInformation.mappedFrequencies();
    BOOST_CHECK(mappedFrequencies.size() == 1);
}

/**
 * Tuning Information - address 8
 */
BOOST_AUTO_TEST_CASE(TuningInformation8)
{
    /* example taken from the Alert-C Coding Handbook Chapter 5.6.2 */

    /* 0x18: PI(ON), PI(ON) */
    rdsReceiver.decode(0xD392, 0x8418, 0xD303, 0xD313);
    rdsReceiver.decode(0xD392, 0x8418, 0xD303, 0xD313);
    rdsReceiver.decode(0xD392, 0x8418, 0xD322, 0xD333);
    rdsReceiver.decode(0xD392, 0x8418, 0xD322, 0xD333);
    rdsReceiver.decode(0xD392, 0x8418, 0xD342, 0xD353);
    rdsReceiver.decode(0xD392, 0x8418, 0xD342, 0xD353);
    rdsReceiver.decode(0xD392, 0x8418, 0xD363, 0xD372);
    rdsReceiver.decode(0xD392, 0x8418, 0xD363, 0xD372);
    rdsReceiver.decode(0xD392, 0x8418, 0xD382, 0xD392);
    rdsReceiver.decode(0xD392, 0x8418, 0xD382, 0xD392);
    rdsReceiver.decode(0xD392, 0x8418, 0xD3A3, 0xD3B2);
    rdsReceiver.decode(0xD392, 0x8418, 0xD3A3, 0xD3B2);
    rdsReceiver.decode(0xD392, 0x8418, 0xD3C2, 0xD3D2);
    rdsReceiver.decode(0xD392, 0x8418, 0xD3C2, 0xD3D2);
    rdsReceiver.decode(0xD392, 0x8418, 0xD3E3, 0xD3F3);
    rdsReceiver.decode(0xD392, 0x8418, 0xD3E3, 0xD3F3);

    /* check */
    std::set<uint16_t> piCodes = ISO14819::trafficMessageChannel(0xD392).tuningInformation.programmeIdListAri1.piCodes();
    BOOST_CHECK(piCodes.size() == 16);
}

/**
 * Tuning Information - address 9
 */
BOOST_AUTO_TEST_CASE(TuningInformation9)
{
    /* 0x19: LTN(ON), MGS(ON), SID(ON), PI(ON) */
    rdsReceiver.decode(0xD392, 0x8419, (0x21 << 10) | (0x9 << 6) | (0x21 << 0), 0xD393);
    rdsReceiver.decode(0xD392, 0x8419, (0x21 << 10) | (0x9 << 6) | (0x21 << 0), 0xD393);

    /* check */
    BOOST_CHECK(ISO14819::trafficMessageChannel(0xD393).systemInformation.locationTableNumber() == 0x21);
    BOOST_CHECK(ISO14819::trafficMessageChannel(0xD393).systemInformation.messageGeographicalScope.international() == 1);
    BOOST_CHECK(ISO14819::trafficMessageChannel(0xD393).systemInformation.messageGeographicalScope.national() == 0);
    BOOST_CHECK(ISO14819::trafficMessageChannel(0xD393).systemInformation.messageGeographicalScope.regional() == 0);
    BOOST_CHECK(ISO14819::trafficMessageChannel(0xD393).systemInformation.messageGeographicalScope.urban() == 1);
    BOOST_CHECK(ISO14819::trafficMessageChannel(0xD393).systemInformation.serviceId() == 0x21);
}

/**
 * Tuning Information - RFUs (0..3, a..f)
 */
BOOST_AUTO_TEST_CASE(TuningInformationRFUs)
{
    /* 0x10..0x13: RFU */
    rdsReceiver.decode(0xD392, 0x8410, 0, 0);
    rdsReceiver.decode(0xD392, 0x8410, 0, 0);
    rdsReceiver.decode(0xD392, 0x8411, 0, 0);
    rdsReceiver.decode(0xD392, 0x8411, 0, 0);
    rdsReceiver.decode(0xD392, 0x8412, 0, 0);
    rdsReceiver.decode(0xD392, 0x8412, 0, 0);
    rdsReceiver.decode(0xD392, 0x8413, 0, 0);
    rdsReceiver.decode(0xD392, 0x8413, 0, 0);
    // Nothing to test here...

    /* 0x1a..0x1f: RFU */
    rdsReceiver.decode(0xD392, 0x841a, 0, 0);
    rdsReceiver.decode(0xD392, 0x841a, 0, 0);
    rdsReceiver.decode(0xD392, 0x841b, 0, 0);
    rdsReceiver.decode(0xD392, 0x841b, 0, 0);
    rdsReceiver.decode(0xD392, 0x841c, 0, 0);
    rdsReceiver.decode(0xD392, 0x841c, 0, 0);
    rdsReceiver.decode(0xD392, 0x841d, 0, 0);
    rdsReceiver.decode(0xD392, 0x841d, 0, 0);
    rdsReceiver.decode(0xD392, 0x841e, 0, 0);
    rdsReceiver.decode(0xD392, 0x841e, 0, 0);
    rdsReceiver.decode(0xD392, 0x841f, 0, 0);
    rdsReceiver.decode(0xD392, 0x841f, 0, 0);
    // Nothing to test here...
}

/**
 * Test ProgrammeIdList
 */
BOOST_AUTO_TEST_CASE(ProgrammeIdList)
{
    ISO14819::ProgrammeIdList piList;

    /* send in some codes */
    piList.piCodeReceived(1);
    BOOST_CHECK(piList.piCodes().size() == 1);
    piList.piCodeReceived(2);
    BOOST_CHECK(piList.piCodes().size() == 2);
    piList.piCodeReceived(3);
    BOOST_CHECK(piList.piCodes().size() == 3);
    piList.piCodeReceived(4);
    BOOST_CHECK(piList.piCodes().size() == 4);

    /* send in some codes */
    piList.piCodeReceived(1);
    piList.piCodeReceived(2);
    piList.piCodeReceived(3);
    piList.piCodeReceived(4);
    BOOST_CHECK(piList.piCodes().size() == 4);

    /* send in some codes */
    piList.piCodeReceived(1);
    piList.piCodeReceived(2);
    piList.piCodeReceived(3);
    BOOST_CHECK(piList.piCodes().size() == 4); // doesn't forget the old code so fast

    /* send in some codes */
    piList.piCodeReceived(1);
    piList.piCodeReceived(2);
    piList.piCodeReceived(3);
    BOOST_CHECK(piList.piCodes().size() == 3); // but now it should be forgotten

    /* send in some codes */
    piList.piCodeReceived(1);
    piList.piCodeReceived(2);
    piList.piCodeReceived(3);
    piList.piCodeReceived(4);
    BOOST_CHECK(piList.piCodes().size() == 4); // it should instantly add new codes
}
