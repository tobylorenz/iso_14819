/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "SystemInformation.h"

#include <cassert>

namespace ISO14819 {

SystemInformation::SystemInformation() :
    messageGeographicalScope(),
    enhancedMode(),
    encryptionInfo(),
    m_countryCode(0),
    m_extendedCountryCode(0),
    m_locationTableNumber(0),
    m_alternativeFrequencyIndicator(false),
    m_mode(0),
    m_gap(0),
    m_serviceId(0),
    m_locationTableCountryCode(0),
    m_locationTableExtendedCountryCode(0)
{
}

uint8_t SystemInformation::countryCode() const
{
    return m_countryCode;
}

void SystemInformation::setCountryCode(uint8_t value)
{
    /* check */
    assert(value <= 0xf); // 4 bit

    /* set */
    m_countryCode = value;
}

uint8_t SystemInformation::extendedCountryCode() const
{
    return m_extendedCountryCode;
}

void SystemInformation::setExtendedCountryCode(uint8_t value)
{
    /* check */
    assert(value <= 0xff); // 8 bit

    /* set */
    m_extendedCountryCode = value;
}

uint8_t SystemInformation::locationTableNumber() const
{
    return m_locationTableNumber;
}

void SystemInformation::setLocationTableNumber(uint8_t value)
{
    /* check */
    assert(value <= 0x3f); // 6 bit

    /* set */
    m_locationTableNumber = value;
}

bool SystemInformation::alternativeFrequencyIndicator() const
{
    return m_alternativeFrequencyIndicator;
}

void SystemInformation::setAlternativeFrequencyIndicator(bool value)
{
    /* check */
    // assert(value <= 1); // 1 bit

    /* set */
    m_alternativeFrequencyIndicator = value;
}

uint8_t SystemInformation::mode() const
{
    return m_mode;
}

void SystemInformation::setMode(uint8_t value)
{
    /* check */
    // assert(value <= 1); // 1 bit

    /* set */
    m_mode = value;
}

uint8_t SystemInformation::gap() const
{
    return m_gap;
}

void SystemInformation::setGap(uint8_t value)
{
    /* check */
    assert(value <= 3); // 2 bit

    /* set */
    m_gap = value;
}

uint8_t SystemInformation::serviceId() const
{
    return m_serviceId;
}

void SystemInformation::setServiceId(uint8_t value)
{
    /* check */
    assert(value <= 0x3f); // 6 bit

    /* set */
    m_serviceId = value;
}

uint8_t SystemInformation::locationTableCountryCode() const
{
    return m_locationTableCountryCode;
}

void SystemInformation::setLocationTableCountryCode(uint8_t value)
{
    /* check */
    assert(value <= 0xf); // 4 bit

    /* set */
    m_locationTableCountryCode = value;
}

uint8_t SystemInformation::locationTableExtendedCountryCode() const
{
    return m_locationTableExtendedCountryCode;
}

void SystemInformation::setLocationTableExtendedCountryCode(uint8_t value)
{
    /* check */
    assert(value <= 0xff); // 8 bit

    /* set */
    m_locationTableExtendedCountryCode = value;
}

}
