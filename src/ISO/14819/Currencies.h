/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include <map>
#include <string>

#include "iso_14819_export.h"
#include "platform.h"

namespace ISO4217 {

/** ISO 4217:2008 Currency */
struct ISO_14819_EXPORT Currency {
    /** 3 alpha characters */
    std::string alphabeticCode;

    /** actual desired local character */
    std::string unicode;
};

/**
 * Return reference to ISO 4217:2008 currency list.
 *
 * @return Reference to ISO 4217:2008 currency list
 */
extern const ISO_14819_EXPORT std::map<uint8_t, Currency> & currencies(); /* singleton */

}
