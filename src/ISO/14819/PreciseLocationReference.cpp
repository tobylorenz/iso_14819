/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "PreciseLocationReference.h"

#include <cassert>

namespace ISO14819 {

PreciseLocationReference::PreciseLocationReference() :
    m_distance(0),
    m_accuracy(0),
    m_reliability(0),
    m_dynamics(0)
{
}

uint16_t PreciseLocationReference::distance() const
{
    return m_distance;
}

void PreciseLocationReference::setDistance(uint16_t distance)
{
    /* check */
    assert(distance <= 0x3ff);

    m_distance = distance;
}

uint8_t PreciseLocationReference::accuracy() const
{
    return m_accuracy;
}

void PreciseLocationReference::setAccuracy(uint8_t accuracy)
{
    /* check */
    assert(accuracy <= 0x3);

    m_accuracy = accuracy;
}

uint8_t PreciseLocationReference::reliability() const
{
    return m_reliability;
}

void PreciseLocationReference::setReliability(uint8_t reliability)
{
    /* check */
    assert(reliability <= 0x1);

    m_reliability = reliability;
}

uint8_t PreciseLocationReference::dynamics() const
{
    return m_dynamics;
}

void PreciseLocationReference::setDynamics(uint8_t dynamics)
{
    /* check */
    assert(dynamics <= 0x3);

    m_dynamics = dynamics;
}

}
