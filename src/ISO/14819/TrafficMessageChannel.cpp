/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "TrafficMessageChannel.h"

#include <cassert>

#include "MessageMemory.h"

namespace ISO14819 {

TrafficMessageChannel::TrafficMessageChannel(uint16_t pi) :
    rdsProgram(ISO62106::rdsProgram(pi)),
    systemInformation(),
    tuningInformation(rdsProgram),
    m_message(nullptr),
    m_lastContinuityIndex(0),
    m_lastGroupSequenceIndicator(0),
    m_lastX(0),
    m_lastY(0),
    m_lastZ(0),
    m_decodeCnt(0)
{
}

void TrafficMessageChannel::decodeA(uint16_t pi, uint16_t aid, uint8_t x, uint16_t y, uint16_t z)
{
    /* check */
    assert(pi <= 0xffff);
    assert(aid <= 0xffff);
    assert(x <= 0x1f);
    assert(y <= 0xffff);
    assert(z <= 0xffff);
    if ((aid != applicationIdV1) &&
            (aid != applicationIdV2) &&
            (aid != applicationIdTest)) {
        return;
    }

    /* assign RDS program and TMC */
    TrafficMessageChannel & currentTrafficMessageChannel = trafficMessageChannel(pi);

    /* validation (identical reception at least twice) */
    if ((currentTrafficMessageChannel.m_lastX != x) ||
            (currentTrafficMessageChannel.m_lastY != y) ||
            (currentTrafficMessageChannel.m_lastZ != z)) {
        currentTrafficMessageChannel.m_lastX = x;
        currentTrafficMessageChannel.m_lastY = y;
        currentTrafficMessageChannel.m_lastZ = z;
        currentTrafficMessageChannel.m_decodeCnt = 1;
    } else {
        currentTrafficMessageChannel.m_decodeCnt++;
    }

    /* at least two receptions */
    if (currentTrafficMessageChannel.m_decodeCnt < 2) {
        return;
    }

    /* decode */
    // X4/T(uning): 0=User Message, 1=Tuning Information
    // X3/F(inal): 0=multi-group message, 1=single-group message
    switch (x) {
    case 0x00: {
        /* Encryption */
        uint8_t variantCode = (y >> 13) & 0x7;
        if (variantCode == 0) {
            uint8_t test = (y >> 11) & 0x3;
            uint8_t sid = (y >> 5) & 0x3f;
            uint8_t encid = (y >> 0) & 0x1f;
            uint8_t ltnbe = (z >> 10) & 0x3f;
            uint16_t rfu = (z >> 0) & 0x1ff;
            if (rfu) {
                decodeIssue.emit("decodeA: Encryption RFU set");
            }
            currentTrafficMessageChannel.decodeEncryptedEag(test, sid, encid, ltnbe);
        } else {
            /* 1..7: Reserved for future use */
            decodeIssue.emit("decodeA: Encryption variantCodes 1..7 are reserved for future use");
        }
    }
    break;

    case 0x01:
    case 0x02:
    case 0x03:
    case 0x04:
    case 0x05:
    case 0x06: {
        /* ALERT-C multi-group */
        bool first = (y >> 15) & 1; // Y15
        if (first) {
            /* First group */
            uint8_t continuityIndex = x & 7;
            bool direction = (y >> 14) & 1;
            uint8_t extent = (y >> 11) & 7;
            uint16_t event = y & 0x7ff;
            uint16_t location = z;
            currentTrafficMessageChannel.decodeMultiFirst(aid, continuityIndex, direction, extent, event, location);
        } else {
            /* Subsequent groups */
            uint8_t continuityIndex = x & 7;
            bool secondGroupIndicator = (y >> 14) & 1;
            uint8_t groupSequenceId = (y >> 12) & 3;
            uint32_t freeFormat = static_cast<uint32_t>((y & 0xfff) << 16) | z;
            currentTrafficMessageChannel.decodeMultiSubsequent(continuityIndex, secondGroupIndicator, groupSequenceId, freeFormat);
        }
    }
    break;

    case 0x07:
        /* Reserved for future use */
        decodeIssue.emit("decodeA: 0x07 is reserved for future use");
        break;

    case 0x08:
    case 0x09:
    case 0x0a:
    case 0x0b:
    case 0x0c:
    case 0x0d:
    case 0x0e:
    case 0x0f: {
        /* ALERT-C single-group */
        uint8_t durationPersistence = x & 7;
        bool diversionAdvice = y >> 15;
        bool direction = (y >> 14) & 1;
        uint8_t extent = (y >> 11) & 7;
        uint16_t event = y & 0x7ff;
        uint16_t location = z;
        currentTrafficMessageChannel.decodeSingle(aid, durationPersistence, diversionAdvice, direction, extent, event, location);
    }
    break;

    case 0x10:
    case 0x11:
    case 0x12:
    case 0x13:
        /* Reserved for future use */
        decodeIssue.emit("decodeA: 0x10..0x13 are reserved for future use");
        break;

    case 0x14:
    case 0x15:
    case 0x16:
    case 0x17:
    case 0x18:
    case 0x19: {
        /* ALERT-C tuning information */
        uint8_t address = x & 0xf;
        currentTrafficMessageChannel.decodeTuningInformation(address, y, z);
    }
    break;

    case 0x1a:
    case 0x1b:
    case 0x1c:
    case 0x1d:
    case 0x1e:
    case 0x1f:
        /* Reserved for future use */
        decodeIssue.emit("decodeA: 0x1a..0x1f are reserved for future use");
        break;
    }
}

void TrafficMessageChannel::decodeIdent(uint16_t pi, uint16_t aid, uint16_t msg)
{
    /* check */
    assert(pi <= 0xffff);
    assert(aid <= 0xffff);
    assert(msg <= 0xffff);
    if ((aid != applicationIdV1) &&
            (aid != applicationIdV2) &&
            (aid != applicationIdTest)) {
        return;
    }

    /* assign RDS program and TMC */
    ISO62106::RdsProgram & rdsProgram = ISO62106::rdsProgram(pi);
    TrafficMessageChannel & currentTrafficMessageChannel = trafficMessageChannel(pi);
    currentTrafficMessageChannel.systemInformation.setCountryCode(rdsProgram.programmeIdentification.countryCode());
    currentTrafficMessageChannel.systemInformation.setExtendedCountryCode(rdsProgram.extendedCountryCode.value());

    /* evaluate variant */
    uint8_t variantCode = (msg >> 14) & 3;
    switch (variantCode) {
    case 0: {
        uint8_t rfu = (msg >> 12) & 0x3;
        if (rfu) {
            decodeIssue.emit("decodeIdent: variant code 0 RFU set");
        }
        currentTrafficMessageChannel.systemInformation.setLocationTableNumber((msg >> 6) & 0x3f);
        currentTrafficMessageChannel.systemInformation.setAlternativeFrequencyIndicator((msg >> 5) & 1);
        currentTrafficMessageChannel.systemInformation.setMode((msg >> 4) & 1);
        currentTrafficMessageChannel.systemInformation.messageGeographicalScope.setInternational((msg >> 3) & 1);
        currentTrafficMessageChannel.systemInformation.messageGeographicalScope.setNational((msg >> 2) & 1);
        currentTrafficMessageChannel.systemInformation.messageGeographicalScope.setRegional((msg >> 1) & 1);
        currentTrafficMessageChannel.systemInformation.messageGeographicalScope.setUrban((msg >> 0) & 1);
    }
    break;
    case 1:
        currentTrafficMessageChannel.systemInformation.setGap((msg >> 12) & 3);
        currentTrafficMessageChannel.systemInformation.setServiceId((msg >> 6) & 0x3f);
        if (currentTrafficMessageChannel.systemInformation.mode()) {
            currentTrafficMessageChannel.systemInformation.enhancedMode.setActivityTime((msg >> 4) & 0x3);
            currentTrafficMessageChannel.systemInformation.enhancedMode.setWindowTime((msg >> 2) & 0x3);
            currentTrafficMessageChannel.systemInformation.enhancedMode.setDelayTime((msg >> 0) & 0x3);
        } else {
            uint8_t rfu = (msg >> 4) & 0x3;
            if (rfu) {
                decodeIssue.emit("decodeIdent: variant code 1 RFU set");
            }
            currentTrafficMessageChannel.systemInformation.setLocationTableCountryCode(msg & 0xf);
        }
        break;
    case 2: {
        uint8_t rfu = (msg >> 8) & 0x3f;
        if (rfu) {
            decodeIssue.emit("decodeIdent: variant code 2 RFU set");
        }
        currentTrafficMessageChannel.systemInformation.setLocationTableExtendedCountryCode(msg & 0xff);
    }
    break;
    case 3: {
        /* nothing is documented for variant code 3 */
        decodeIssue.emit("decodeIdent: variant code 3 is undefined");
    }
    break;
    }
}

void TrafficMessageChannel::registerHandler()
{
    /* register ODA */
    ISO62106::OpenDataApplications::onDecodeA.connect(sigc::ptr_fun(decodeA));
    ISO62106::OpenDataApplications::onDecodeIdent.connect(sigc::ptr_fun(decodeIdent));
}

void TrafficMessageChannel::decodeSingle(uint16_t applicationId, uint8_t durationPersistence, uint8_t diversionAdvice, uint8_t direction, uint8_t extent, uint16_t event, uint16_t location)
{
    /* check */
    assert(durationPersistence <= 0x7);
    assert(diversionAdvice <= 0x1);
    assert(direction <= 0x1);
    assert(extent <= 0x7);
    assert(event <= 0x7ff);
    assert(location <= 0xffff);

    /* handle previously received message */
    if (m_message) {
        pushToMessageMemory();
    }

    /* Set continuity index to 1
     * Otherwise a multi/single/multi combination with multi having same CI doesn't work */
    m_lastContinuityIndex = 0;

    /* set data */
    m_message = new Message;
    m_message->setApplicationId(applicationId);
    m_message->setGroupsExpected(1);
    m_message->incGroupsReceived();
    m_message->setDurationPersistence(durationPersistence);
    m_message->setDiversionAdvice(diversionAdvice);
    m_message->setDirection(direction);
    m_message->setExtent(extent);
    m_message->setEvent(event);
    m_message->setLocation(location);
    m_message->systemInformation = systemInformation;
    m_message->receiveTime = rdsProgram.clockTime.value();

    /* handle message */
    pushToMessageMemory();
}

void TrafficMessageChannel::decodeTuningInformation(uint8_t address, uint16_t blk3, uint16_t blk4)
{
    /* check */
    assert((address >= 0x4) && (address <= 0x9));
    assert(blk3 <= 0xffff);
    assert(blk4 <= 0xffff);

    /* evaluate */
    switch (address) {
    case 4:
        /* character 1..4 */
        tuningInformation.setServiceProviderName(0, ISO62106::basicCharSet.at((blk3 >> 8) & 0xff));
        tuningInformation.setServiceProviderName(1, ISO62106::basicCharSet.at((blk3 >> 0) & 0xff));
        tuningInformation.setServiceProviderName(2, ISO62106::basicCharSet.at((blk4 >> 8) & 0xff));
        tuningInformation.setServiceProviderName(3, ISO62106::basicCharSet.at((blk4 >> 0) & 0xff));
        break;
    case 5:
        /* character 5..8 */
        tuningInformation.setServiceProviderName(4, ISO62106::basicCharSet.at((blk3 >> 8) & 0xff));
        tuningInformation.setServiceProviderName(5, ISO62106::basicCharSet.at((blk3 >> 0) & 0xff));
        tuningInformation.setServiceProviderName(6, ISO62106::basicCharSet.at((blk4 >> 8) & 0xff));
        tuningInformation.setServiceProviderName(7, ISO62106::basicCharSet.at((blk4 >> 0) & 0xff));
        break;
    case 6:
        /* AF (ON), AF (ON) */
        tuningInformation.alternativeFrequencies.decode((blk3 >> 8) & 0xff, (blk3 >> 0) & 0xff);
        /* PI (ON with AFI=0) */
        tuningInformation.programmeIdListAri0.piCodeReceived(blk4);
        break;
    case 7:
        /* Tuning freq (TN), Mapped freq (ON) */
        tuningInformation.mappedFrequencyReceived((blk3 >> 8) & 0xff, (blk3 >> 0) & 0xff);
        /* PI (ON with AFI=0) */
        tuningInformation.programmeIdListAri0.piCodeReceived(blk4);
        break;
    case 8:
        /* PI (ON with AFI=1) */
        tuningInformation.programmeIdListAri1.piCodeReceived(blk3);
        /* PI (ON with AFI=1) */
        tuningInformation.programmeIdListAri1.piCodeReceived(blk4);
        break;
    case 9:
        /* LTN (ON), MGS(ON), SID(ON) */
        tuningInformation.setSystemInformationOtherNetwork(
            (blk3 >> 10) & 0x3f,
            (blk3 >> 6) & 0xf,
            blk3 & 0x3f,
            blk4);
        /* PI (ON with AFI=1) */
        tuningInformation.programmeIdListAri1.piCodeReceived(blk4);
        break;
    }
}

void TrafficMessageChannel::decodeMultiFirst(uint16_t applicationId, uint8_t continuityIndex, uint8_t direction, uint8_t extent, uint16_t event, uint16_t location)
{
    /* check */
    assert(continuityIndex <= 0x7);
    assert(direction <= 0x1);
    assert(extent <= 0x7);
    assert(event <= 0x7ff);
    assert(location <= 0xffff);

    /* check for new continuity index */
    if (m_lastContinuityIndex == continuityIndex) {
        /* this is just a duplicate multi first message */
        return;
    }
    m_lastContinuityIndex = continuityIndex;

    /* handle previously received message */
    if (m_message) {
        /* this message is incomplete */
        pushToMessageMemory();
    }

    /* set data */
    m_message = new Message;
    m_message->setApplicationId(applicationId);
    // m_message->setGroupsExpected(0); // the expected size is encoded in the second group
    m_message->incGroupsReceived();
    m_message->setDirection(direction);
    m_message->setExtent(extent);
    m_message->setEvent(event);
    m_message->setLocation(location);
    m_message->setDurationPersistence(7); /* set persistence period to midnight on the day after message receipt */
    m_message->systemInformation = systemInformation;
    m_message->receiveTime = rdsProgram.clockTime.value();
}

void TrafficMessageChannel::decodeMultiSubsequent(uint8_t continuityIndex, uint8_t secondGroupIndicator, uint8_t groupSequenceId, uint32_t freeFormat)
{
    /* check */
    assert(continuityIndex <= 0x7);
    assert(secondGroupIndicator <= 0x1);
    assert(groupSequenceId <= 0x3);
    assert(freeFormat <= 0x7ffffff);

    /* return if there is no first multi-message */
    if (!m_message) {
        return;
    }

    /* ci should be identical to the ci of the first message */
    if (m_lastContinuityIndex != continuityIndex) {
        /* this message is incomplete */
        pushToMessageMemory();
        return;
    }

    /* possible values of sgi and gsi for N=5..2: */
    /* N=5: MF, MS(sgi=1, gsi=3), MS(sgi=0, gsi=2), MS(sgi=0, gsi=1), MS(sgi=0, gsi=0) */
    /* N=4: MF, MS(sgi=1, gsi=2), MS(sgi=0, gsi=1), MS(sgi=0, gsi=0) */
    /* N=3: MF, MS(sgi=1, gsi=1), MS(sgi=0, gsi=0) */
    /* N=2: MF, MS(sgi=1, gsi=0) */
    m_message->incGroupsReceived();
    if (secondGroupIndicator == 1) {
        m_message->setGroupsExpected(groupSequenceId + 2);

        /* inter-road message have the primary location coded in (y11..z12) */
        /* only the remaining 12 bits (z11..z0) are then used for free format */
        if ((m_message->location() >= 64512) && (m_message->location() <= 65532)) {
            /* LTCC and LTN are encoded in primary location */
            uint8_t interRoadLocationTableCountryCode = (m_message->location() >> 6) & 0xf;
            uint8_t interRoadLocationTableNumber = (m_message->location() >> 0) & 0x3f;

            /* primary location is replaced with first 16 bits of free format message */
            uint16_t interRoadLocation = static_cast<uint16_t>(freeFormat >> 12);

            /* set this INTER-ROAD location */
            m_message->setInterRoadLocation(interRoadLocationTableCountryCode, interRoadLocationTableNumber, interRoadLocation);

            /* process free format (z11..z0) */
            m_message->pushFreeFormat(freeFormat & 0x0fff, 12);
        } else {
            /* process free format (y11..z0) */
            m_message->pushFreeFormat(freeFormat, 28);
        }
    } else if (m_lastGroupSequenceIndicator - 1 == groupSequenceId) {
        /* groups 3..5 */
        /* process free format (y11..z0) */
        m_message->pushFreeFormat(freeFormat, 28);
    }
    m_lastGroupSequenceIndicator = groupSequenceId;

    /* handle message */
    if (groupSequenceId == 0) {
        pushToMessageMemory();
    }
}

void TrafficMessageChannel::decodeEncryptedEag(uint8_t test, uint8_t serviceId, uint8_t encryptionId, uint8_t locationTableNumberBeforeEncryption)
{
    /* check */
    assert(test <= 0x3);
    assert(serviceId <= 0x3f);
    assert(encryptionId <= 0x1f);
    assert(locationTableNumberBeforeEncryption <= 0x3f);

    /* save information */
    systemInformation.encryptionInfo.setTest(test);
    systemInformation.setServiceId(serviceId);
    systemInformation.encryptionInfo.setEncryptionId(encryptionId);
    systemInformation.encryptionInfo.setLocationTableNumberBeforeEncryption(locationTableNumberBeforeEncryption);
}

void TrafficMessageChannel::pushToMessageMemory()
{
    /* first process the Optional Message Contents */
    m_message->processOptionalMessageContents();

    /* push to Message Memory */
    messageMemory().insertMessage(*m_message);

    /* delete message, make room for the next */
    delete m_message;
    m_message = nullptr;
}

sigc::signal<void, std::string> decodeIssue;

std::map<uint16_t, TrafficMessageChannel> & trafficMessageChannel()
{
    /* singleton */
    static std::map<uint16_t, TrafficMessageChannel> list;
    return list;
}

TrafficMessageChannel & trafficMessageChannel(uint16_t pi)
{
    /* create entry */
    if (trafficMessageChannel().count(pi) == 0) {
        TrafficMessageChannel tmc(pi);
        trafficMessageChannel().emplace(std::make_pair(pi, tmc));
    }

    return trafficMessageChannel().at(pi); // [] is not possible as there is no default constructor
}

}
