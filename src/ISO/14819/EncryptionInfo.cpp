/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "EncryptionInfo.h"

#include <cassert>

namespace ISO14819 {

EncryptionInfo::EncryptionInfo() :
    m_tableId(0),
    m_encryptionId(0),
    m_locationTableNumberBeforeEncryption(0),
    m_test(0)
{
}

uint8_t EncryptionInfo::tableId() const
{
    return m_tableId;
}

void EncryptionInfo::setTableId(uint8_t value)
{
    /* check */
    assert(value <= 0x7); // 3 bit

    /* set */
    m_tableId = value;
}

uint8_t EncryptionInfo::encryptionId() const
{
    return m_encryptionId;
}

void EncryptionInfo::setEncryptionId(uint8_t value)
{
    /* check */
    assert(value <= 0x1f); // 5 bit

    /* set */
    m_encryptionId = value;
}

uint8_t EncryptionInfo::locationTableNumberBeforeEncryption() const
{
    return m_locationTableNumberBeforeEncryption;
}

void EncryptionInfo::setLocationTableNumberBeforeEncryption(uint8_t value)
{
    /* check */
    assert(value <= 0x3f); // 6 bit

    /* set */
    m_locationTableNumberBeforeEncryption = value;
}

uint8_t EncryptionInfo::test() const
{
    return m_test;
}

void EncryptionInfo::setTest(uint8_t value)
{
    /* check */
    assert(value <= 0x3); // 2 bit

    /* set */
    m_test = value;
}

}
