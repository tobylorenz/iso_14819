/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "LocationCodeLists.h"

#include <cassert>

namespace ISO14819 {

LocationCodeLists::LocationCodeLists() :
    m_locationCodeLists() // default
{
    /* load default LCL */
    Key key = std::make_pair(0, 0);
    m_locationCodeLists[key] = LocationCodeList(0, 0);
}

LocationCodeList & LocationCodeLists::locationCodeList(uint16_t countryId, uint8_t tableCode)
{
    /* check */
    // assert(countryId <= 0xffff);
    assert(tableCode <= 0x3f);

    /* if not loaded yet, try to load it now */
    Key key = std::make_pair(countryId, tableCode);
    if (m_locationCodeLists.count(key) == 0) {
        /* try to load it */
        LocationCodeList lcl(countryId, tableCode);
        if (lcl.locationDataSets().size() > 0) {
            m_locationCodeLists[key] = lcl;
        }
    }

    /* return lcl if loaded */
    if (m_locationCodeLists.count(key) == 0) {
        const Key defaultKey = std::make_pair(0, 0);
        return m_locationCodeLists.at(defaultKey);
    } else {
        return m_locationCodeLists.at(key);
    }
}

uint16_t LocationCodeLists::countryId(uint8_t extendedCountryCode, uint8_t countryCode) const
{
    const Key defaultKey = std::make_pair(0, 0);
    for (auto & country : m_locationCodeLists.at(defaultKey).countries()) {
        if ((country.extendedCountryCode == extendedCountryCode) && (country.countryCode == countryCode)) {
            return country.countryId;
        }
    }

    return 0;
}

LocationCodeList & LocationCodeLists::locationCodeList(uint8_t extendedCountryCode, uint8_t countryCode, uint8_t tableCode)
{
    /* check */
    // assert(extendedCountryCode <= 0xff);
    assert(countryCode <= 0xf);
    assert(tableCode <= 0x3f);

    /* return the LCL if available */
    return locationCodeList(countryId(extendedCountryCode, countryCode), tableCode);
}

LocationCodeLists & locationCodeLists()
{
    /* singleton */
    static LocationCodeLists object;
    return object;
}

}
