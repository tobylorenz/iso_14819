/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "Currencies.h"
#include "TelephoneNumber.h"

#include <cassert>

namespace ISO14819 {

TelephoneNumber::TelephoneNumber() :
    m_telephoneNumber(),
    m_ivrOptionNumber(),
    m_timeUnit(TimeUnit::TimeFree),
    m_cost(0),
    m_decimalMultiplier(0),
    m_currencyInFront(false),
    m_currencyReference(0),
    m_state(State::TelephoneNumber),
    m_alphaValueMode(false)
{
}

std::string TelephoneNumber::telephoneNumber() const
{
    return m_telephoneNumber;
}

void TelephoneNumber::setTelephoneNumber(std::string value)
{
    m_telephoneNumber = value;
}

std::string TelephoneNumber::ivrOptionNumber() const
{
    return m_ivrOptionNumber;
}

void TelephoneNumber::setIvrOptionNumber(std::string value)
{
    m_ivrOptionNumber = value;
}

uint8_t TelephoneNumber::timeUnit() const
{
    return m_timeUnit;
}

void TelephoneNumber::setTimeUnit(uint8_t value)
{
    /* check */
    assert(value <= 7); // 3 bit

    /* set */
    m_timeUnit = value;
}

uint16_t TelephoneNumber::cost() const
{
    return m_cost;
}

void TelephoneNumber::setCost(uint16_t value)
{
    /* check */
    assert(m_cost <= 0x3fff); // 14 bit

    /* set */
    m_cost = value;
}

uint8_t TelephoneNumber::decimalMultiplier() const
{
    return m_decimalMultiplier;
}

void TelephoneNumber::setDecimalMultiplier(uint8_t value)
{
    /* check */
    assert(value <= 3); // 2 bit

    /* set */
    m_decimalMultiplier = value;
}

bool TelephoneNumber::currencyInFront() const
{
    return m_currencyInFront;
}

void TelephoneNumber::setCurrencyInFront(bool value)
{
    m_currencyInFront = value;
}

uint8_t TelephoneNumber::currencyReference() const
{
    return m_currencyReference;
}

void TelephoneNumber::setCurrencyReference(uint8_t value)
{
    m_currencyReference = value;
}

bool TelephoneNumber::pull(BitFifo & fifo)
{
    bool readMore = true;
    while (readMore) {
        switch (m_state) {
        case State::TelephoneNumber:
        case State::IvrOptionNumber:
            if (m_alphaValueMode == false) {
                if (fifo.size() >= 4) {
                    appendNumericalValue(fifo.pull(4) & 0xf);
                } else {
                    readMore = false;
                }
            } else {
                if (fifo.size() >= 5) {
                    appendAlphaValue(fifo.pull(5) & 0x1f);
                } else {
                    readMore = false;
                }
            }
            break;
        case State::TimeUnit:
            if (fifo.size() >= 3) {
                m_timeUnit = fifo.pull(3) & 0x7;
                m_state = State::Multiplier;
            } else {
                readMore = false;
            }
            break;
        case State::Multiplier:
            if (fifo.size() >= 2) {
                m_decimalMultiplier = fifo.pull(2) & 0x3;
                m_state = State::Cost;
            } else {
                readMore = false;
            }
            break;
        case State::Cost:
            if (fifo.size() >= 14) {
                m_cost = fifo.pull(14) & 0x3fff;
                m_state = State::Pos;
            } else {
                readMore = false;
            }
            break;
        case State::Pos:
            if (fifo.size() >= 1) {
                m_currencyInFront = fifo.pull(1) & 0x1;
                m_state = State::Currency;
            } else {
                readMore = false;
            }
            break;
        case State::Currency:
            if (fifo.size() >= 8) {
                m_currencyReference = fifo.pull(8) & 0xff;
                m_state = State::Complete;
            } else {
                readMore = false;
            }
            break;
        case State::Complete:
            readMore = false;
            break;
        }
    }

    return (m_state == State::Complete);
}

void TelephoneNumber::appendNumericalValue(uint8_t data)
{
    /* check */
    assert(data <= 0xf);

    switch (data & 0xf) {
    case 0:
    case 1:
    case 2:
    case 3:
    case 4:
    case 5:
    case 6:
    case 7:
    case 8:
    case 9:
    case 10:
    case 11:
    case 12: {
        char c = static_cast<char>('0' + data);
        if (m_state == State::TelephoneNumber) {
            m_telephoneNumber += c;
        } else {
            m_ivrOptionNumber += c;
        }
    }
    break;
    case 13:
        m_alphaValueMode = true;
        break;
    case 14:
        assert(state == State::TelephoneNumber);
        m_state = State::IvrOptionNumber;
        break;
    case 15:
        m_state = State::TimeUnit;
        break;
    }
}

void TelephoneNumber::appendAlphaValue(uint8_t data)
{
    /* check */
    assert(data <= 0x1f);

    switch (data & 0x1f) {
    case 0:
        m_alphaValueMode = false;
        break;
    case 1:
    case 2:
    case 3:
    case 4:
    case 5:
    case 6:
    case 7:
    case 8:
    case 9:
    case 10:
    case 11:
    case 12:
    case 13:
    case 14:
    case 15:
    case 16:
    case 17:
    case 18:
    case 19:
    case 20:
    case 21:
    case 22:
    case 23:
    case 24:
    case 25:
    case 26: {
        char c = static_cast<char>('A' + data - 1);
        if (m_state == State::TelephoneNumber) {
            m_telephoneNumber += c;
        } else {
            m_ivrOptionNumber += c;
        }
    }
    break;
    case 27: {
        if (m_state == State::TelephoneNumber) {
            m_telephoneNumber += ' ';
        } else {
            m_ivrOptionNumber += ' ';
        }
    }
    break;
    case 28: {
        if (m_state == State::TelephoneNumber) {
            m_telephoneNumber += '-';
        } else {
            m_ivrOptionNumber += '-';
        }
    }
    break;
    case 29:
        assert(state == State::TelephoneNumber);
        m_state = State::IvrOptionNumber;
        m_alphaValueMode = true;
        break;
    case 30:
        assert(state == State::TelephoneNumber);
        m_state = State::IvrOptionNumber;
        m_alphaValueMode = false;
        break;
    case 31:
        m_state = State::TimeUnit;
        break;
    }
}

}
