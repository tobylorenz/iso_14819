/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include <cstdint>

#include "iso_14819_export.h"
#include "platform.h"

namespace ISO14819 {

/** encryption information */
class ISO_14819_EXPORT EncryptionInfo
{
public:
    explicit EncryptionInfo();

    /**
     * Get Service Key table ID (SID)
     *
     * @return Service Key table ID (SID)
     */
    uint8_t tableId() const;

    /**
     * Set Service Key table ID (SID)
     *
     * @param[in] value Service Key table ID (SID)
     */
    void setTableId(uint8_t value);

    /**
     * Get Encryption Identifier (ENCID)
     *
     * @return Encryption Identifier (ENCID)
     */
    uint8_t encryptionId() const;

    /**
     * Set Encryption Identifier (ENCID)
     *
     * @param[in] value Encryption Identifier (ENCID)
     */
    void setEncryptionId(uint8_t value);

    /**
     * Get Location Table Number Before Encryption (LTNBE)
     *
     * @return Location Table Number Before Encryption (LTNBE)
     */
    uint8_t locationTableNumberBeforeEncryption() const;

    /**
     * Set Location Table Number Before Encryption (LTNBE)
     *
     * @param[in] value Location Table Number Before Encryption (LTNBE)
     */
    void setLocationTableNumberBeforeEncryption(uint8_t value);

    /**
     * Get Test
     *
     * @return Test
     */
    uint8_t test() const;

    /**
     * Set Test
     *
     * @param[in] value Test
     */
    void setTest(uint8_t value);

private:
    /** Service Key table ID (SID) */
    uint8_t m_tableId : 3;

    /** Encryption Identifier (ENCID) */
    uint8_t m_encryptionId : 5;

    /**
     * Location Table Number Before Encryption (LTNBE)
     */
    uint8_t m_locationTableNumberBeforeEncryption : 6;

    /**
     * Test
     *
     *   - 0: not encrypted
     *   - 1: use pre-advised key
     *   - 2: rfu
     *   - 3: full encrypted
     */
    uint8_t m_test : 2;
};

}
