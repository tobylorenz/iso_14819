/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "MessageGeographicalScope.h"

#include <cassert>

namespace ISO14819 {

MessageGeographicalScope::MessageGeographicalScope() :
    m_international(false),
    m_national(false),
    m_regional(false),
    m_urban(false)
{
}

bool MessageGeographicalScope::international() const
{
    return m_international;
}

void MessageGeographicalScope::setInternational(bool value)
{
    m_international = value;
}

bool MessageGeographicalScope::national() const
{
    return m_national;
}

void MessageGeographicalScope::setNational(bool value)
{
    m_national = value;
}

bool MessageGeographicalScope::regional() const
{
    return m_regional;
}

void MessageGeographicalScope::setRegional(bool value)
{
    m_regional = value;
}

bool MessageGeographicalScope::urban() const
{
    return m_urban;
}

void MessageGeographicalScope::setUrban(bool value)
{
    m_urban = value;
}

}
