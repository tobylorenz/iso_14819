/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include <array>
#include <cstdint>
#include <string>
#include <map>

#include <ISO/62106.h>

#include "ProgrammeIdList.h"

#include "iso_14819_export.h"
#include "platform.h"

namespace ISO14819 {

/** tuning information */
class ISO_14819_EXPORT TuningInformation
{
public:
    explicit TuningInformation(const ISO62106::RdsProgram & rdsProgram);

    /* Address 4+5 (SPN) */

    /**
     * Set Service Provider Name (SPN) character.
     *
     * @param[in] position position
     * @param[in] character character UCS-2 encoded
     */
    void setServiceProviderName(uint8_t position, char16_t character);

    /**
     * Return Service Provider Name (SPN) UCS-2 encoded.
     *
     * @return Service Provider Name (SPN) UCS-2 encoded
     */
    std::u16string serviceProviderNameUcs2() const;

    /**
     * Return Service Provider Name (SPN) UTF-8 encoded.
     *
     * @return Service Provider Name (SPN) UTF-8 encoded
     */
    std::string serviceProviderNameUtf8() const;

    /* Address 6+7 (AFI=0) */

    /** Alternative Frequencies Other Network AF(ON) */
    ISO62106::AlternativeFrequencies alternativeFrequencies;

    /**
     * Return list of mapped frequencies
     *
     * @return List of mapped frequencies
     */
    std::map<uint8_t, std::vector<uint8_t>> mappedFrequencies() const;

    /**
     * Received a mapped frequency for the tuning frequency.
     *
     * @param[in] tuningFreq tuning frequency
     * @param[in] mappedFreq mapped frequency
     */
    void mappedFrequencyReceived(uint8_t tuningFreq, uint8_t mappedFreq);

    /** Programme Identification Other Network PI(ON) with ARI=0 */
    ProgrammeIdList programmeIdListAri0;

    /* Address 8+9 (AFI=1) */

    /** Programme Identification Other Network PI(ON) with ARI=1 */
    ProgrammeIdList programmeIdListAri1;

    /**
     * Provide PI(ON) code with different system information.
     *
     * @param[in] ltn Location Table Number
     * @param[in] mgs Message Geographical Scope
     * @param[in] sid Service Id
     * @param[in] pi PI code
     */
    void setSystemInformationOtherNetwork(uint8_t ltn, uint8_t mgs, uint8_t sid, uint16_t pi);

private:
    /** Service Provider Name (SPN) UCS-2 */
    std::array<char16_t, 8> m_serviceProviderName;

    /**
     * Mapped Frequencies (MF)
     *
     * The values are Tuning Frequency (TF), Mapped Frequency (MF), reception count.
     *
     * See also ISO62106::EnhancedOtherNetworks::MappedFrequencies.
     */
    std::map<uint8_t, std::map<uint8_t, uint8_t>> m_mappedFrequencies;

    /** max reception count for mapped frequencies */
    uint8_t m_maxReceptionCount;
};

}
