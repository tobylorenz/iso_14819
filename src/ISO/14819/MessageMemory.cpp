/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "MessageMemory.h"

#include <algorithm>
#include <cassert>

#include "EventList.h"
#include "EncryptionKeys.h"
#include "TrafficMessageChannel.h"

namespace ISO14819 {

MessageMemory::MessageMemory() :
    m_acceptTestMessages(false),
    m_list()
{
}

bool MessageMemory::acceptTestMessages() const
{
    return m_acceptTestMessages;
}

void MessageMemory::setAcceptTestMessages(bool value)
{
    m_acceptTestMessages = value;
}

void MessageMemory::clear()
{
    m_list.clear();
}

std::vector<Message> MessageMemory::list() const
{
    return m_list;
}

void MessageMemory::insertMessage(Message & message)
{
    /* if CT invalid */
    if (!message.receiveTime.valid()) {
        return;
    }

    /* if it was a test message */
    if ((!m_acceptTestMessages) && message.testMessage()) {
        return;
    }

    /* if INTER-ROAD message with only first message */
    if ((message.location() >= 64512) && (message.location() <= 65532)) {
        /* The TrafficMessageChannel class already replaces the location with the one stored
         * in the second message, so if it still says INTER-ROAD, then the second message
         * was not received and we need to stop here. */
        return;
    }

    /* decrypt location */
    if (message.systemInformation.locationTableNumber() == 0) {
        /* don't handle encrypted messages without knowing the encryption key */
        if (message.systemInformation.encryptionInfo.locationTableNumberBeforeEncryption() == 0) {
            return;
        }

        message.systemInformation.setLocationTableNumber(message.systemInformation.encryptionInfo.locationTableNumberBeforeEncryption());
        decryptLocationCode(message);
    }

    /* handle special messages */
    if ((message.event() == 2047) && (message.location() == 65535)) {
        /* full cancellation of all messages from that RDS-TMC service */
        cancelMessagesService(message);
        return;
    }
    if (message.event() == 2047) {
        /* full cancellation of all messages with that location code */
        cancelMessagesLocationCode(message);
        return;
    }
    if ((message.eventListEntry.nature() == 'S') && (message.location() == 65535)) {
        /* full cancellation of all messages with an event code in the same update class */
        cancelMessagesUpdateClass(message);
        return;
    }

    /* a new message overwrites an existing stored message if it ... */
    std::vector<Message>::iterator it = find_if(m_list.begin(), m_list.end(), [&message](const Message & memMessage) {
        /* has the same primary location, drawn from the same location table, as the existing message OR the
         * special location code 65535 AND; */
        if (!(((memMessage.location() == message.location()) &&
                (memMessage.locationTableNumber() == message.locationTableNumber())) ||
                (message.location() == 65535))) {
            return false;
        }

        /* has the same value of the direction bit AND; */
        if (!(memMessage.direction() == message.direction())) {
            return false;
        }

        /* contains an event that belongs to the same update class as any event (a multi-group message may have
         * more than one event) in the existing message, AND; */
        if (!message.sameUpdateClasses(memMessage)) {
            return false;
        }

        /* comes from the same RDS-TMC service. (i.e. a service with the same LTN and SID) as the existing
         * message AND; */
        if (!message.sameService(memMessage)) {
            return false;
        }

        /* if the message relates to a forecast event (update classes 32 - 39), the same duration. */
        if ((message.eventListEntry.nature() == 'F')) {
            if (!(memMessage.durationPersistence() == message.durationPersistence())) {
                return false;
            }
        }

        return true;
    });

    /* message found */
    if (it != m_list.end()) {
        if (message.complete()) {
            /* update the existing message content with the content from the received complete message */
            *it = message;

            /* emit signal */
            onMessageUpdation.emit(message);
        }
    } else {
        /* insert the message as new */
        m_list.push_back(message);

        /* emit signal */
        onMessageInsertion.emit(message);
    }
}

void MessageMemory::updateMessages(ISO62106::RdsTime currentTime)
{
    /* delete message if no refresh has been received within a specified persistence period */
    for (;;) {
        std::vector<Message>::iterator it = find_if(m_list.begin(), m_list.end(), [&currentTime](const Message & memMessage) {
            return memMessage.persistenceTime <= currentTime;
        });
        if (it == m_list.end()) {
            break;
        }
        onMessageDeletion.emit(*it);
        m_list.erase(it);
    }

    /* detailed stop-time */
    for (;;) {
        std::vector<Message>::iterator it = find_if(m_list.begin(), m_list.end(), [&currentTime](const Message & memMessage) {
            return (memMessage.stopTime.valid()) &&
                   (memMessage.stopTime <= currentTime);
        });
        if (it == m_list.end()) {
            break;
        }
        onMessageDeletion.emit(*it);
        m_list.erase(it);
    }

    /* decrement time */
    for (Message & memMessage : m_list) {
        if ((memMessage.decrementTime.valid()) &&
                (memMessage.decrementTime <= currentTime)) {
            memMessage.decrementDurationPersistence();

            /* emit signal */
            onMessageUpdation.emit(memMessage);
        }
    }
}

void MessageMemory::decryptLocationCode(Message & message)
{
    /* get encryption keys */
    std::array<uint8_t, 3> keyTable;
    switch (message.systemInformation.encryptionInfo.test()) {
    case 0:
        /* no encryption */
        keyTable.fill(0);
        break;
    case 1:
        /* encrypt using encryption parameters pre-advised by the service provider */
        keyTable = encryptionKeys().keyTablePreAdvised();
        break;
    case 2:
        /* rfu */
        keyTable.fill(0); // we just use no encryption here and continue on
        decodeIssue.emit("decryptLocationCode: 2 is RFU");
        break;
    case 3:
        /* full encryption */
        keyTable = encryptionKeys().keyTable(message.systemInformation.encryptionInfo.tableId(), message.systemInformation.encryptionInfo.encryptionId());
        break;
    }
    uint8_t xorValue = keyTable.at(0);
    uint8_t startBit = keyTable.at(1);
    uint8_t rotateLeft = keyTable.at(2);

    /* decrypt */
    uint16_t decryptedLocation = message.location();
    decryptedLocation ^= (xorValue << startBit);
    decryptedLocation = static_cast<uint16_t>(decryptedLocation << rotateLeft) | (decryptedLocation >> (16 - rotateLeft));
    message.setLocation(decryptedLocation);
}

void MessageMemory::cancelMessagesService(Message & message)
{
    /* check */
    assert(message.event == 2047);
    assert(message.locationCode == 65535);

    /* full cancellation of all messages from that RDS-TMC service */
    for (;;) {
        std::vector<Message>::iterator it = find_if(m_list.begin(), m_list.end(), [&message](const Message & memMessage) {
            return (message.sameService(memMessage));
        });
        if (it == m_list.end()) {
            break;
        }
        onMessageDeletion.emit(*it);
        m_list.erase(it);
    }
}

void MessageMemory::cancelMessagesLocationCode(Message & message)
{
    /* check */
    assert(message.event == 2047);

    /* full cancellation of all messages with that location code */
    for (;;) {
        std::vector<Message>::iterator it = find_if(m_list.begin(), m_list.end(), [&message](const Message & memMessage) {
            return (message.sameService(memMessage) &&
                    (message.location() == memMessage.location()));
        });
        if (it == m_list.end()) {
            break;
        }
        onMessageDeletion.emit(*it);
        m_list.erase(it);
    }
}

void MessageMemory::cancelMessagesUpdateClass(Message & message)
{
    /* check */
    assert(message.eventListEntry.nature() == 'S');
    assert(message.location == 65535);

    /* full cancellation of all messages with an event code in the same update class */
    for (;;) {
        std::vector<Message>::iterator it = find_if(m_list.begin(), m_list.end(), [&message](const Message & memMessage) {
            return (message.sameService(memMessage) &&
                    message.sameUpdateClasses(memMessage));
        });
        if (it == m_list.end()) {
            break;
        }
        onMessageDeletion.emit(*it);
        m_list.erase(it);
    }
}

MessageMemory & messageMemory()
{
    /* singleton */
    static MessageMemory list;
    return list;
}

sigc::signal<void, Message> MessageMemory::onMessageInsertion;

sigc::signal<void, Message> MessageMemory::onMessageUpdation;

sigc::signal<void, Message> MessageMemory::onMessageDeletion;

}
