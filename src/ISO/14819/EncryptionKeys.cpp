/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "EncryptionKeys.h"

#include <cassert>

namespace ISO14819 {

EncryptionKeys::EncryptionKeys() :
    m_keyTablePreAdvised(),
    m_keyTable()
{
}

std::array<uint8_t, 3> EncryptionKeys::keyTablePreAdvised() const
{
    return m_keyTablePreAdvised;
}

void EncryptionKeys::setKeyTablePreAdvised(uint8_t xorValue, uint8_t startBit, uint8_t rotateLeft)
{
    m_keyTablePreAdvised[0] = xorValue;
    m_keyTablePreAdvised[1] = startBit;
    m_keyTablePreAdvised[2] = rotateLeft;
}

std::array<uint8_t, 3> EncryptionKeys::keyTable(uint8_t encryptionTableId, uint8_t encryptionId) const
{
    return m_keyTable.at(encryptionTableId).at(encryptionId);
}

void EncryptionKeys::setKeyTable(uint8_t encryptionTableId, uint8_t encryptionId, uint8_t xorValue, uint8_t startBit, uint8_t rotateLeft)
{
    /* check */
    assert(encryptionTableId <= 7);
    assert(encryptionId <= 31);

    /* set */
    m_keyTable[encryptionTableId][encryptionId][0] = xorValue;
    m_keyTable[encryptionTableId][encryptionId][1] = startBit;
    m_keyTable[encryptionTableId][encryptionId][2] = rotateLeft;
}

EncryptionKeys & encryptionKeys()
{
    /* singleton */
    static EncryptionKeys list;
    return list;
}

}
