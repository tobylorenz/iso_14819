/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "OptionalMessageContent.h"

#include <cassert>
#include <array>

namespace ISO14819 {

OptionalMessageContent::OptionalMessageContent() :
    m_label(0),
    m_data(0)
{
}

uint8_t OptionalMessageContent::label() const
{
    return m_label;
}

void OptionalMessageContent::setLabel(uint8_t label)
{
    /* check */
    assert(label <= 0xf);

    m_label = label;
}

uint16_t OptionalMessageContent::data() const
{
    return m_data;
}

void OptionalMessageContent::setData(uint16_t data)
{
    /* check */
    //assert(data <= 0xffff);

    m_data = data;
}

uint8_t OptionalMessageContent::size(uint8_t label)
{
    /* check */
    assert(label <= 0xf);

    static const std::array<uint8_t, 16> dataFieldSize = {{3, 3, 5, 5, 5, 8, 8, 8, 8, 11, 16, 16, 16, 16, 0, 6}};
    return dataFieldSize.at(label);
}

uint8_t OptionalMessageContent::lengthOfRouteAffected() const
{
    /* check */
    assert(m_label == 2);
    assert(m_data <= 0x1f); // 5 bit

    uint8_t retVal = 0;
    switch (m_data) {
    case 0:
        /* Problem extends for mre than 100 km */
        retVal = 0;
        break;
    case 1:
    case 2:
    case 3:
    case 4:
    case 5:
    case 6:
    case 7:
    case 8:
    case 9:
    case 10:
        /* Length of problem from 1 to 10 km (1 km interval) */
        retVal = static_cast<uint8_t>(m_data);
        break;
    case 11:
    case 12:
    case 13:
    case 14:
    case 15:
        /* Length of problem from 12 to 20 km (2 km interval) */
        retVal = static_cast<uint8_t>(12 + (m_data - 11) * 2);
        break;
    case 16:
    case 17:
    case 18:
    case 19:
    case 20:
    case 21:
    case 22:
    case 23:
    case 24:
    case 25:
    case 26:
    case 27:
    case 28:
    case 29:
    case 30:
    case 31:
        /* Length of problem from 25 to 100 km (5 km interval) */
        retVal = static_cast<uint8_t>(25 + (m_data - 16) * 5);
        break;
    }
    return retVal;
}

uint8_t OptionalMessageContent::speedLimit() const
{
    /* check */
    assert(m_label == 3);
    assert(m_data <= 0x1f); // 5 bit

    return  static_cast<uint8_t>(m_data * 5); // 0..155 km/h
}

PreciseLocationReference OptionalMessageContent::preciseLocationReference() const
{
    /* check */
    assert(m_label == 12);
    assert(m_data <= 0xffff); // 16 bit

    PreciseLocationReference retVal;
    retVal.setDistance(m_data & 0x3ff);
    retVal.setAccuracy((m_data >> 11) & 0x3);
    retVal.setReliability((m_data >> 13) & 0x1);
    retVal.setDynamics((m_data >> 14) & 0x3);
    return retVal;
}

}
