/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include <map>
#include <string>

#include "iso_14819_export.h"
#include "platform.h"

namespace ISO14819 {

/**
 * FIFO (first-in-first-out) buffer on bit level
 *
 * This FIFO buffer works on bit level.
 * Push adds bits to the FIFO. Pull removes bits from the FIFO.
 * It is intended for the free format bits resp. the Optional Message Content.
 */
class ISO_14819_EXPORT BitFifo
{
public:
    explicit BitFifo();

    /**
     * Return size in bits
     *
     * @return size in bits
     */
    uint8_t size() const;

    /**
     * Push data into the bit fifo.
     *
     * @param[in] data data
     * @param[in] size size in bits
     */
    void push(uint32_t data, uint8_t size);

    /**
     * Pull data from the bit fifo.
     *
     * @param[in] size size in bits
     * @return data
     */
    uint16_t pull(uint8_t size);

private:
    /** remaining bits */
    uint32_t m_data;

    /** number of bits used in freeFormatBits field */
    uint8_t m_size;
};

}
