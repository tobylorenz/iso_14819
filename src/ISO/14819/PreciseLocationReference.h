/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include <cstdint>

#include "iso_14819_export.h"
#include "platform.h"

namespace ISO14819 {

/** Precise Location Reference */
class ISO_14819_EXPORT PreciseLocationReference
{
public:
    explicit PreciseLocationReference();

    /**
     * get distance
     *
     * @return distance
     */
    uint16_t distance() const;

    /**
     * set distance
     *
     * @param[in] distance distance
     */
    void setDistance(uint16_t distance);

    /**
     * get accuracy
     *
     * @return accuracy
     */
    uint8_t accuracy() const;

    /**
     * set accuracy
     *
     * @param[in] accuracy accuracy
     */
    void setAccuracy(uint8_t accuracy);

    /**
     * get reliability
     *
     * @return reliability
     */
    uint8_t reliability() const;

    /**
     * set reliability
     *
     * @param[in] reliability reliability
     */
    void setReliability(uint8_t reliability);

    /**
     * get dynamics
     *
     * @return dynamics
     */
    uint8_t dynamics() const;

    /**
     * set dynamics
     *
     * @param[in] dynamics dynamics
     */
    void setDynamics(uint8_t dynamics);

private:
    /** Coding of the hazard distance (D1), reliability, status and direction */
    uint16_t m_distance : 10; // 0..1023 calculates to 0..102.3km (Resolution: 100m)

    /**
     * Accuracy
     *
     *   - 0: <=100m
     *   - 1: <=500m
     *   - 2: <=1km
     *   - 3: >1km
     */
    uint8_t m_accuracy : 2;

    /**
     * Reliability
     *
     *   - 0: reliable
     *   - 1: approximate (confirmation desired)
     */
    uint8_t m_reliability : 1;

    /**
     * Dynamics
     *
     *   - 0: static (e.g. road works, black ice, hazard point is stationary)
     *   - 1: dynamic, approaching (e.g. tail of traffic jam queue, hazard point moves towards the secondary location)
     *   - 2: dynamic, receding (e.g. tail of traffic jam queue, hazard point moves towards the primary location)
     *   - 3: dynamics and movement unknown (hazard point may be moving in unknown direction)
     */
    uint8_t m_dynamics; // status and direction
};

}
