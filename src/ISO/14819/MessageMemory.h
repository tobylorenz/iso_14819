/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include <cstdint>
#include <vector>

#include <ISO/62106.h>

#include "Message.h"

#include "iso_14819_export.h"
#include "platform.h"

namespace ISO14819 {

/**
 * @brief Message Memory
 *
 * This class contains all the Message Memory with all current TMC messages.
 */
class ISO_14819_EXPORT MessageMemory
{
public:
    explicit MessageMemory();

    /**
     * Get accept test messages.
     *
     * @return true if test messages are accepted, false otherwise
     */
    bool acceptTestMessages() const;

    /**
     * Set accept test messages.
     *
     * @param[in] value true to accept test messages, false otherwise
     */
    void setAcceptTestMessages(bool value);

    /**
     * Clear message memory.
     */
    void clear();

    /**
     * Return message memory.
     *
     * @return message memory
     */
    std::vector<Message> list() const;

    /**
     * @brief Insert or update message based on message reception.
     * @param[in] message Message
     *
     * This function handles the current TMC message.
     * It also extracts the optional message contents from the free format fields.
     */
    void insertMessage(Message & message);

    /**
     * @brief Update or delete messages based on timer signal.
     *
     * This maintains the received message, by
     *   - update of duration codes
     *   - update of time stamps
     *   - deletion on expiry
     *
     * @param[in] currentTime current time
     */
    void updateMessages(ISO62106::RdsTime currentTime);

    /**
     * decrypt the message location.
     *
     * @param[in] message message
     */
    void decryptLocationCode(Message & message);

    /**
     * full cancellation of all messages from that RDS-TMC service
     *
     * @param[in] message message
     */
    void cancelMessagesService(Message & message);

    /**
     * full cancellation of all messages with that location code
     *
     * @param[in] message message
     */
    void cancelMessagesLocationCode(Message & message);

    /**
     * full cancellation of all messages with an event code in the same update class
     *
     * @param[in] message message
     */
    void cancelMessagesUpdateClass(Message & message);

    /** message was inserted */
    static sigc::signal<void, Message> onMessageInsertion;

    /** message was updated */
    static sigc::signal<void, Message> onMessageUpdation;

    /** message was deleted */
    static sigc::signal<void, Message> onMessageDeletion;

private:
    /** accept test messages */
    bool m_acceptTestMessages;

    /** message memory */
    std::vector<Message> m_list;
};

/**
 * Return message memory.
 *
 * @return Reference to message memory
 */
extern ISO_14819_EXPORT MessageMemory & messageMemory(); /* singleton */

}
