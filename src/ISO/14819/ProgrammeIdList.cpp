/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "ProgrammeIdList.h"

#include <cassert>

namespace ISO14819 {

ProgrammeIdList::ProgrammeIdList() :
    m_piReceptionCount(),
    m_maxReceptionCount(0)
{
}

void ProgrammeIdList::piCodeReceived(uint16_t piCode)
{
    /* increase reception count */
    uint8_t newCount = m_piReceptionCount[piCode]++;
    m_maxReceptionCount = std::max(m_maxReceptionCount, newCount);

    /* devide count by 2 and if count reaches 0 delete entry */
    if (m_maxReceptionCount >= 2) {
        for (auto pair = m_piReceptionCount.begin(); pair != m_piReceptionCount.end(); ) {
            pair->second /= 2;

            /* delete pair if count is zero */
            if (pair->second == 0) {
                m_piReceptionCount.erase(pair++);
            } else {
                ++pair;
            }
        }
    }
    m_maxReceptionCount /= 2;
}

std::set<uint16_t> ProgrammeIdList::piCodes() const
{
    std::set<uint16_t> retVal;
    for (auto & pair : m_piReceptionCount) {
        retVal.insert(pair.first);
    }
    return retVal;
}

}
