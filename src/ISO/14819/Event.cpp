/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "Event.h"

#include <cassert>

namespace ISO14819 {

Event::Event() :
    m_nature(0),
    m_quantifier(0),
    m_durationType(0),
    m_spoken(false),
    m_directionality(0),
    m_urgency(0),
    m_updateClass(0),
    m_phraseCodes()
{
}

char Event::nature() const
{
    return m_nature;
}

void Event::setNature(char value)
{
    /* check */
    assert((value == 'I') || (value == 'F') || (value == 'S'));
    if ((m_updateClass >= 32) && (m_updateClass <= 39)) {
        assert(value == 'F');
    }

    /* set value */
    m_nature = value;
}

uint8_t Event::quantifier() const
{
    return m_quantifier;
}

void Event::setQuantifier(uint8_t value)
{
    /* check */
    assert(value <= 12);

    /* set value */
    m_quantifier = value;
}

char Event::durationType() const
{
    return m_durationType;
}

void Event::setDurationType(char value)
{
    /* check */
    assert((value == 'D') || (value == 'L'));

    /* set value */
    m_durationType = value;
}

void Event::switchDurationType()
{
    /* check */
    assert((value == 'D') || (value == 'L'));

    /* switch */
    m_durationType = (m_durationType == 'D') ? 'L' : 'D';
}

bool Event::spoken() const
{
    return m_spoken;
}

void Event::setSpoken(bool value)
{
    /* check */

    /* set value */
    m_spoken = value;
}

void Event::switchSpoken()
{
    m_spoken = !m_spoken;
}

uint8_t Event::directionality() const
{
    return m_directionality;
}

void Event::setDirectionality(uint8_t value)
{
    /* check */
    assert((value >= 1) && (value <= 2));

    /* set value */
    m_directionality = value;
}

void Event::switchDirectionality()
{
    /* check */
    assert((value >= 1) && (value <= 2));

    /* set value */
    m_directionality = (m_directionality == 1) ? 2 : 1;
}

char Event::urgency() const
{
    return m_urgency;
}

void Event::setUrgency(char value)
{
    /* check */
    assert((value == 'N') || (value == 'U') || (value == 'X'));

    /* set value */
    m_urgency = value;
}

void Event::increaseUrgency()
{
    /* check */
    assert((value == 'N') || (value == 'U') || (value == 'X'));

    /* increase value */
    switch (m_urgency) {
    case 'N':
        m_urgency = 'U';
        break;
    case 'U':
        m_urgency = 'X';
        break;
    case 'X':
        m_urgency = 'N';
        break;
    }
}

void Event::decreaseUrgency()
{
    /* check */
    assert((value == 'N') || (value == 'U') || (value == 'X'));

    /* increase value */
    switch (m_urgency) {
    case 'N':
        m_urgency = 'X';
        break;
    case 'U':
        m_urgency = 'N';
        break;
    case 'X':
        m_urgency = 'U';
        break;
    }
}

uint8_t Event::updateClass() const
{
    return m_updateClass;
}

void Event::setUpdateClass(uint8_t value)
{
    /* check */
    assert((value >= 1) && (value <= 39));
    if (m_nature == 'F') {
        assert((m_value >= 32) && (m_value <= 39));
    }

    /* set value */
    m_updateClass = value;
}

std::string Event::phraseCodes() const
{
    return m_phraseCodes;
}

void Event::setPhraseCodes(std::string value)
{
    /* check */
    assert(!value.empty());

    /* set value */
    m_phraseCodes = value;
}

}
