/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include <cstdint>
#include <string>
#include <map>

#include <sqlite3.h>

#include "iso_14819_export.h"
#include "platform.h"

namespace ISO14819 {

/**
 * @brief Structure for TMC Event List entry type from EL/FEL tables
 *
 * This type contains the language-independent constants of a TMC event list entry.
 */
class ISO_14819_EXPORT Event
{
public:
    explicit Event();

    /**
     * Get nature
     *
     * @return nature
     */
    char nature() const;

    /**
     * Set nature
     *
     * @param[in] value nature
     */
    void setNature(char value);

    /**
     * Get quantifier
     *
     * @return quantifier
     */
    uint8_t quantifier() const;

    /**
     * Set quantifier
     *
     * @param[in] value quantifier
     */
    void setQuantifier(uint8_t value);

    /**
     * Get duration
     *
     * @return duration
     */
    char durationType() const;

    /**
     * Set duration
     *
     * @param[in] value duration
     */
    void setDurationType(char value);

    /**
     * Default dynamic or longer-lasting provision interchanged
     */
    void switchDurationType();

    /**
     * Get spoken
     *
     * @return spoken
     */
    bool spoken() const;

    /**
     * Set spoken
     *
     * @param[in] value spoken
     */
    void setSpoken(bool value);

    /**
     * Default spoken or unspoken duration interchanged
     */
    void switchSpoken();

    /**
     * Get directionality
     *
     * @return directionality
     */
    uint8_t directionality() const;

    /**
     * Set directionality
     *
     * @param[in] value directionality
     */
    void setDirectionality(uint8_t value);

    /**
     * Default directionality of message changed
     */
    void switchDirectionality();

    /**
     * Get urgengy
     *
     * @return urgency
     */
    char urgency() const;

    /**
     * Set urgency
     *
     * @param[in] value urgency
     */
    void setUrgency(char value);

    /**
     * Default urgency increased by one level
     *
     * @note The increase wraps/rotates around to the lowest level.
     */
    void increaseUrgency();

    /**
     * Default urgency reduced by one level
     *
     * @note The decrease wraps/rotates around to the highest level.
     */
    void decreaseUrgency();

    /**
     * Get update class
     *
     * @return update class
     */
    uint8_t updateClass() const;

    /**
     * Set update class
     *
     * @param[in] value update class
     */
    void setUpdateClass(uint8_t value);

    /**
     * Get phrase codes
     *
     * @return phrase codes
     */
    std::string phraseCodes() const;

    /**
     * Set phrase codes
     *
     * @param[in] value phrase codes
     */
    void setPhraseCodes(std::string value);

private:
    /** Nature: 'I'=Info 'F'=Forecast 'S'=Silent */
    char m_nature;

    /** Quantifier: 0..12 */
    uint8_t m_quantifier : 4;

    /** Duration Type: 'D'=Dynamic 'L'=Longer-lasting */
    char m_durationType;

    /** Spoken (part of Duration Type) */
    bool m_spoken : 1;

    /** Directionality: 1..2 */
    uint8_t m_directionality : 2;

    /** Urgency: 'N'=Normal 'U'=Urgent 'X'=Extremely Urgent */
    char m_urgency;

    /** Update Class: 1..31=EL,32..39=FEL */
    uint8_t m_updateClass : 6;

    /** Phrase Codes (References) */
    std::string m_phraseCodes;
};

}
