/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include <cstdint>

#include <ISO/62106.h>

#include "EncryptionInfo.h"
#include "EnhancedMode.h"
#include "MessageGeographicalScope.h"

#include "iso_14819_export.h"
#include "platform.h"

namespace ISO14819 {

/**
 * @brief System Information
 *
 * This class stores received System Information.
 */
class ISO_14819_EXPORT SystemInformation
{
public:
    explicit SystemInformation();

    /**
     * Get RDS Country Code (CC)
     *
     * @return RDS Country Code (CC)
     */
    uint8_t countryCode() const;

    /**
     * Set RDS Country Code (CC)
     *
     * @param[in] value RDS Country Code (CC)
     */
    void setCountryCode(uint8_t value);

    /**
     * Get RDS Extended Country Code (ECC)
     *
     * @return RDS Extended Country Code (ECC)
     */
    uint8_t extendedCountryCode() const;

    /**
     * Set RDS Extended Country Code (ECC)
     *
     * @param[in] value RDS Extended Country Code (ECC)
     */
    void setExtendedCountryCode(uint8_t value);

    /**
     * Get location table number (LTN)
     *
     * @return location table number (LTN)
     */
    uint8_t locationTableNumber() const;

    /**
     * Set location table number (LTN)
     *
     * @param[in] value location table number (LTN)
     */
    void setLocationTableNumber(uint8_t value);

    /**
     * Get alternative frequency indicator (AFI)
     *
     * @return alternative frequency indicator (AFI)
     */
    bool alternativeFrequencyIndicator() const;

    /**
     * Set alternative frequency indicator (AFI)
     *
     * @param[in] value alternative frequency indicator (AFI)
     */
    void setAlternativeFrequencyIndicator(bool value);

    /**
     * Get mode: 0=basic 1=enhanced
     *
     * @return mode: 0=basic 1=enhanced
     */
    uint8_t mode() const;

    /**
     * Set mode: 0=basic 1=enhanced
     *
     * @param[in] value mode: 0=basic 1=enhanced
     */
    void setMode(uint8_t value);

    /**
     * Get gap (groups)
     *
     * @return gap (groups)
     */
    uint8_t gap() const;

    /**
     * Set gap (groups)
     *
     * @param[in] value gap (groups)
     */
    void setGap(uint8_t value);

    /**
     * Get service identifier (SID)
     *
     * @return service identifier (SID)
     */
    uint8_t serviceId() const;

    /**
     * Set service identifier (SID)
     *
     * @param[in] value service identifier (SID)
     */
    void setServiceId(uint8_t value);

    /**
     * Get Location Table Country Code (LTCC)
     *
     * @return Location Table Country Code (LTCC)
     */
    uint8_t locationTableCountryCode() const;

    /**
     * Set Location Table Country Code (LTCC)
     *
     * @param[in] value Location Table Country Code (LTCC)
     */
    void setLocationTableCountryCode(uint8_t value);

    /**
     * Get Location Table Extended Country Code (LTECC)
     *
     * @return Location Table Extended Country Code (LTECC)
     */
    uint8_t locationTableExtendedCountryCode() const;

    /**
     * Set Location Table Extended Country Code (LTECC)
     *
     * @param[in] value Location Table Extended Country Code (LTECC)
     */
    void setLocationTableExtendedCountryCode(uint8_t value);

    /** message geographical scope (MGS) */
    MessageGeographicalScope messageGeographicalScope;

    /** Enhanced Mode */
    EnhancedMode enhancedMode;

    /** encryption information */
    EncryptionInfo encryptionInfo;

private:
    /** RDS Country Code (CC) */
    uint8_t m_countryCode : 4;

    /** RDS Extended Country Code (ECC) */
    uint8_t m_extendedCountryCode : 8;

    /** location table number (LTN) */
    uint8_t m_locationTableNumber : 6;

    /** alternative frequency indicator (AFI) */
    bool m_alternativeFrequencyIndicator : 1;

    /** mode: 0=basic 1=enhanced */
    uint8_t m_mode : 1;

    /** gap (groups) */
    uint8_t m_gap : 2; // 0..3 ==> 3, 5, 8, 11

    /** service identifier (SID) */
    uint8_t m_serviceId : 6;

    /** Location Table Country Code (LTCC) */
    uint8_t m_locationTableCountryCode : 4;

    /** Location Table Extended Country Code (LTECC) */
    uint8_t m_locationTableExtendedCountryCode : 8;
};

}
