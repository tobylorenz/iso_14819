/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "TuningInformation.h"

#include <cassert>
#include <codecvt>
#include <cstring>
#include <locale>

#include "TrafficMessageChannel.h"

namespace ISO14819 {

TuningInformation::TuningInformation(const ISO62106::RdsProgram & rdsProgram) :
    alternativeFrequencies(rdsProgram),
    programmeIdListAri0(),
    programmeIdListAri1(),
    m_serviceProviderName(),
    m_mappedFrequencies(),
    m_maxReceptionCount(0)
{
}

void TuningInformation::setServiceProviderName(uint8_t position, char16_t character)
{
    /* check */
    assert(position <= 7);

    /* set */
    m_serviceProviderName[position] = character;
}

std::u16string TuningInformation::serviceProviderNameUcs2() const
{
    /* convert u16string to wstring */
    std::u16string retVal;
    for (auto c = m_serviceProviderName.begin(); c != m_serviceProviderName.end() && *c; c++) {
        retVal.push_back(*c);
    }

    /* null termination */
    retVal.push_back(0);

    return retVal;
}

std::string TuningInformation::serviceProviderNameUtf8() const
{
    std::wstring_convert<std::codecvt_utf8<char16_t>, char16_t> converter;
    std::string retVal = converter.to_bytes(m_serviceProviderName.begin(), m_serviceProviderName.end());
    retVal.resize(strnlen(retVal.data(), retVal.size()));
    return retVal;
}

std::map<uint8_t, std::vector<uint8_t>> TuningInformation::mappedFrequencies() const
{
    std::map<uint8_t, std::vector<uint8_t>> retVal;
    for (auto & tfMf : m_mappedFrequencies) {
        for (auto & mfCnt : tfMf.second) {
            retVal[tfMf.first].push_back(mfCnt.first);
        }
    }
    return retVal;
}

void TuningInformation::mappedFrequencyReceived(uint8_t tuningFreq, uint8_t mappedFreq)
{
    /* increase reception count */
    uint8_t newCount = m_mappedFrequencies[tuningFreq][mappedFreq]++;
    m_maxReceptionCount = std::max(m_maxReceptionCount, newCount);

    /* devide count by 2 and if count reaches 0 delete entry */
    if (m_maxReceptionCount >= 2) {
        for (auto tfMf = m_mappedFrequencies.begin(); tfMf != m_mappedFrequencies.end(); ) {
            for (auto mfCnt = tfMf->second.begin(); mfCnt != tfMf->second.end(); ) {
                mfCnt->second /= 2;

                /* delete mapped frequency if count is zero */
                if (mfCnt->second == 0) {
                    tfMf->second.erase(mfCnt++);
                } else {
                    ++mfCnt;
                }
            }

            /* delete tuning frequency if empty */
            if (tfMf->second.size() == 0) {
                m_mappedFrequencies.erase(tfMf++);
            } else {
                ++tfMf;
            }
        }
    }
    m_maxReceptionCount /= 2;
}

void TuningInformation::setSystemInformationOtherNetwork(uint8_t ltn, uint8_t mgs, uint8_t sid, uint16_t pi)
{
    /* check */
    assert(ltn <= 0x3f);
    assert(mgs <= 0xf);
    assert(sid <= 0x3f);

    /* enhance other network */
    trafficMessageChannel(pi).systemInformation.setLocationTableNumber(ltn & 0x3f);
    trafficMessageChannel(pi).systemInformation.messageGeographicalScope.setInternational((mgs >> 3) & 1);
    trafficMessageChannel(pi).systemInformation.messageGeographicalScope.setNational((mgs >> 2) & 1);
    trafficMessageChannel(pi).systemInformation.messageGeographicalScope.setRegional((mgs >> 1) & 1);
    trafficMessageChannel(pi).systemInformation.messageGeographicalScope.setUrban((mgs >> 0) & 1);
    trafficMessageChannel(pi).systemInformation.setServiceId(sid & 0x3f);
}

}
