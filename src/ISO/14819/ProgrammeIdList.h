/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include <cstdint>
#include <map>
#include <set>

#include "iso_14819_export.h"
#include "platform.h"

namespace ISO14819 {

/** Programme Identification Other Network PI(ON) */
class ISO_14819_EXPORT ProgrammeIdList
{
public:
    explicit ProgrammeIdList();

    /**
     * Signal a received PI code.
     *
     * When a PI code was not received for some time, it get's deleted automatically.
     *
     * @param[in] piCode PI code
     */
    void piCodeReceived(uint16_t piCode);

    /**
     * Return all received PI codes.
     *
     * @return PI codes
     */
    std::set<uint16_t> piCodes() const;

private:
    /** PI Codes and how often they were received */
    std::map<uint16_t, uint8_t> m_piReceptionCount;

    /** max reception count */
    uint8_t m_maxReceptionCount;
};

}
