/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "BitFifo.h"

#include <cassert>

namespace ISO14819 {

BitFifo::BitFifo() :
    m_data(0),
    m_size(0)
{
}

uint8_t BitFifo::size() const
{
    return m_size;
}

void BitFifo::push(uint32_t data, uint8_t size)
{
    /* check */
    assert(m_size + size <= 32);

    /* get mask */
    uint32_t mask = (1 << size) - 1; // eg. 3 => 1000b => 0111b
    assert((data & ~mask) == 0); // all bits above mask must be 0

    /* push data */
    m_data = (m_data << size) | (data & mask);
    m_size += size;
}

uint16_t BitFifo::pull(uint8_t size)
{
    /* check */
    assert(size <= 16);
    assert(size <= m_size);

    /* get mask */
    uint32_t mask = (1 << size) - 1; // eg. 3 => 1000b => 0111b

    /* pull data */
    m_size -= size;
    uint16_t retVal = static_cast<uint16_t>((m_data >> m_size) & mask);

    return retVal;
}

}
