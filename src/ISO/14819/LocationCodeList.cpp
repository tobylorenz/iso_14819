/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "LocationCodeList.h"

#include <cassert>
#include <iomanip>
#include <sstream>

#include "DatabaseHelpers.h"

namespace ISO14819 {

LocationCodeList::Country::Country() :
    countryId(0),
    extendedCountryCode(0),
    countryCode(0),
    name()
{
}

LocationCodeList::LocationDataSet::LocationDataSet() :
    countryId(0),
    tableCode(0),
    comment(),
    version(),
    versionDescription()
{
}

LocationCodeList::LocationCode::LocationCode() :
    countryId(0),
    tableCode(0),
    locationCode(0),
    allocated(false)
{
}

LocationCodeList::Class::Class() :
    typeClass(0)
{
}

LocationCodeList::Type::Type() :
    typeClass(0),
    typeCode(0),
    typeDescription(),
    nationalTypeCode(),
    nationalTypeDescription()
{
}

LocationCodeList::Subtype::Subtype() :
    typeClass(0),
    typeCode(0),
    subtypeCode(0),
    subtypeDescription(),
    nationalSubtypeCode(),
    nationalSubtypeDescription()
{
}

LocationCodeList::Language::Language() :
    countryId(0),
    languageId(0),
    language()
{
}

LocationCodeList::EuroRoadNo::EuroRoadNo() :
    europeanRoadNumber(),
    comment()
{
}

LocationCodeList::Name::Name() :
    countryId(0),
    languageId(0),
    nameId(),
    name(),
    comment()
{
}

LocationCodeList::NameTranslation::NameTranslation() :
    countryId(0),
    languageId(0),
    nameId(0),
    translation()
{
}

LocationCodeList::SubTypeTranslation::SubTypeTranslation() :
    countryId(0),
    languageId(0),
    typeClass(0),
    typeCode(0),
    subtypeCode(0),
    translation()
{
}

LocationCodeList::EuroRoadNoBelongsToCountry::EuroRoadNoBelongsToCountry() :
    countryId(0),
    europeanRoadNumber()
{
}

LocationCodeList::AdministrativeArea::AdministrativeArea() :
    countryId(0),
    tableCode(0),
    locationCode(0),
    typeClass(0),
    typeCode(0),
    subtypeCode(0),
    name(0),
    upwardAreaReference(0)
{
}

LocationCodeList::OtherArea::OtherArea() :
    countryId(0),
    tableCode(0),
    locationCode(0),
    typeClass(0),
    typeCode(0),
    subtypeCode(0),
    name1(0),
    adminAreaReference(0)
{
}

LocationCodeList::Road::Road() :
    countryId(0),
    tableCode(0),
    locationCode(0),
    typeClass(0),
    typeCode(0),
    subtypeCode(0),
    roadNumber(),
    roadName(0),
    name1(0),
    name2(0),
    adminAreaReference(0),
    roadNetworkLevel(0)
{
}

LocationCodeList::RoadNetworkLevelType::RoadNetworkLevelType() :
    roadNetworkLevel(0),
    roadNetworkLevelDescription(),
    nationalRoadNetworkLevelDescription()
{
}

LocationCodeList::Segment::Segment() :
    countryId(0),
    tableCode(0),
    locationCode(0),
    typeClass(0),
    typeCode(0),
    subtypeCode(0),
    roadNumber(),
    roadName(0),
    name1(0),
    name2(0),
    roadReference(0),
    segmentReference(0),
    adminAreaReference(0)
{
}

LocationCodeList::SOffset::SOffset() :
    countryId(0),
    tableCode(0),
    locationCode(0),
    negativeOffset(0),
    positiveOffset(0)
{
}

LocationCodeList::SegHasEuroRoadNo::SegHasEuroRoadNo() :
    countryId(0),
    tableCode(0),
    locationCode(0),
    europeanRoadNumber()
{
}

LocationCodeList::Point::Point() :
    countryId(0),
    tableCode(0),
    locationCode(0),
    typeClass(0),
    typeCode(0),
    subtypeCode(0),
    junctionNumber(),
    roadName(0),
    name1(0),
    name2(0),
    adminAreaReference(0),
    otherAreaReference(0),
    segmentReference(0),
    roadReference(0),
    inPos(false),
    inNeg(false),
    outPos(false),
    outNeg(false),
    presentPos(false),
    presentNeg(false),
    diversionPos(),
    diversionNeg(),
    xCoord(),
    yCoord(),
    interruptsRoad(false),
    urban(false)
{
}

LocationCodeList::POffset::POffset() :
    countryId(0),
    tableCode(0),
    locationCode(0),
    negativeOffset(0),
    positiveOffset(0)
{
}

LocationCodeList::Intersection::Intersection() :
    countryId(0),
    tableCode(0),
    locationCode(0),
    intersectionCountryId(0),
    intersectionTableCode(0),
    intersectionLocationCode(0)
{
}

LocationCodeList::TypeUnion::TypeUnion() :
    typeClass(0),
    typeCode(0),
    subtypeCode(0),
    description(),
    nationalCode(),
    nationalDescription()
{
}

LocationCodeList::LocationUnion::LocationUnion() :
    countryId(0),
    tableCode(0),
    locationCode(0),
    typeClass(0),
    typeCode(0),
    subtypeCode(0),
    number(),
    roadName(0),
    name1(0),
    name2(0),
    adminAreaReference(0),
    otherAreaReference(0),
    segmentReference(0),
    roadReference(0),
    urban(false)
{
}

LocationCodeList::OffsetUnion::OffsetUnion() :
    countryId(0),
    tableCode(0),
    locationCode(0),
    negativeOffset(0),
    positiveOffset(0)
{
}

LocationCodeList::LocationCodeList(uint16_t countryId, uint8_t tableCode) :
    m_filename(),
    m_database(nullptr),
    m_countryId(countryId),
    m_tableCode(tableCode)
{
    /* check */
    assert(countryId <= 0xffff);
    assert(tableCode <= 0x3f);

    /* create database filename */
    std::stringstream ss;
    ss
            << DATABASEDIR << "/lcl/"
            << std::dec << static_cast<uint16_t>(countryId)
            << "_"
            << std::dec << static_cast<uint16_t>(tableCode)
            << ".db";
    m_filename = ss.str();

    /* open database */
    if (sqlite3_open_v2(m_filename.c_str(), &m_database, SQLITE_OPEN_READONLY, nullptr) != SQLITE_OK) {
        if (m_database) {
            (void) sqlite3_close(m_database);
            m_database = nullptr;
        }
        return;
    }
}

std::vector<LocationCodeList::Country> LocationCodeList::countries() const
{
    /* prepare SQL statement */
    std::string sql = std::string("select CID,ECC,CCD,CNAME from COUNTRIES");

    /* execute SQL statement */
    std::vector<LocationCodeList::Country> list;
    sqlite3_stmt * stmt = nullptr;
    (void) sqlite3_prepare(m_database, sql.c_str(), static_cast<int>(sql.size()), &stmt, nullptr);
    while (sqlite3_step(stmt) == SQLITE_ROW) {
        LocationCodeList::Country entry;
        entry.countryId = static_cast<uint16_t>(sqlite3_column_int(stmt, 0));
        entry.extendedCountryCode = static_cast<uint8_t>(std::stoul(sqlite3string(stmt, 1), nullptr, 16));
        entry.countryCode = static_cast<uint8_t>(std::stoul(sqlite3string(stmt, 2), nullptr, 16));
        entry.name = sqlite3string(stmt, 3);
        list.push_back(entry);
    }
    (void) sqlite3_finalize(stmt);

    return list;
}

std::vector<LocationCodeList::LocationDataSet> LocationCodeList::locationDataSets() const
{
    /* prepare SQL statement */
    std::string sql = std::string("SELECT CID,TABCD,DCOMMENT,VERSION,VERSIONDESCRIPTION FROM LOCATIONDATASETS");

    /* execute SQL statement */
    std::vector<LocationCodeList::LocationDataSet> list;
    sqlite3_stmt * stmt = nullptr;
    (void) sqlite3_prepare(m_database, sql.c_str(), static_cast<int>(sql.size()), &stmt, nullptr);
    while (sqlite3_step(stmt) == SQLITE_ROW) {
        LocationCodeList::LocationDataSet entry;
        entry.countryId = static_cast<uint16_t>(sqlite3_column_int(stmt, 0));
        entry.tableCode = static_cast<uint8_t>(sqlite3_column_int(stmt, 1));
        entry.comment = sqlite3string(stmt, 2);
        entry.version = sqlite3string(stmt, 3);
        entry.versionDescription = sqlite3string(stmt, 4);
        list.push_back(entry);
    }
    (void) sqlite3_finalize(stmt);

    return list;
}

LocationCodeList::LocationCode LocationCodeList::locationCode(uint16_t countryId, uint8_t tableCode, uint16_t locationCode) const
{
    /* check */
    // assert(countryId <= 0xffff);
    assert(tableCode <= 0x3f);
    // assert(locationCode <= 0xffff);

    /* prepare SQL statement */
    std::string sql = std::string("SELECT CID,TABCD,LCD,ALLOCATED FROM LOCATIONCODES WHERE CID=")
                      + std::to_string(countryId) + " AND TABCD=" + std::to_string(tableCode) + " AND LCD="
                      + std::to_string(locationCode);

    /* execute SQL statement */
    LocationCodeList::LocationCode entry;
    sqlite3_stmt * stmt = nullptr;
    (void) sqlite3_prepare(m_database, sql.c_str(), static_cast<int>(sql.size()), &stmt, nullptr);
    if (sqlite3_step(stmt) == SQLITE_ROW) {
        entry.countryId = static_cast<uint16_t>(sqlite3_column_int(stmt, 0));
        entry.tableCode = static_cast<uint8_t>(sqlite3_column_int(stmt, 1));
        entry.locationCode = static_cast<uint16_t>(sqlite3_column_int(stmt, 2));
        entry.allocated = static_cast<uint8_t>(sqlite3_column_int(stmt, 3));
    }
    (void) sqlite3_finalize(stmt);

    return entry;
}

std::vector<LocationCodeList::Class> LocationCodeList::classes() const
{
    /* prepare SQL statement */
    std::string sql = std::string("select CLASS from CLASSES");

    /* execute SQL statement */
    std::vector<LocationCodeList::Class> list;
    sqlite3_stmt * stmt = nullptr;
    (void) sqlite3_prepare(m_database, sql.c_str(), static_cast<int>(sql.size()), &stmt, nullptr);
    while (sqlite3_step(stmt) == SQLITE_ROW) {
        LocationCodeList::Class entry;
        entry.typeClass = sqlite3string(stmt, 0)[0];
        list.push_back(entry);
    }
    (void) sqlite3_finalize(stmt);

    return list;
}

LocationCodeList::Type LocationCodeList::type(char typeClass, uint16_t typeCode) const
{
    /* check */
    assert((typeClass == 'A') || (typeClass == 'L') || (typeClass == 'P'));
    assert(typeCode <= 12);

    /* prepare SQL statement */
    std::string sql = std::string("SELECT CLASS,TCD,TDESC,TNATCD,TNATDESC FROM TYPES WHERE CLASS='")
                      + typeClass + "' AND TCD=" + std::to_string(typeCode);

    /* execute SQL statement */
    LocationCodeList::Type entry;
    sqlite3_stmt * stmt = nullptr;
    (void) sqlite3_prepare(m_database, sql.c_str(), static_cast<int>(sql.size()), &stmt, nullptr);
    if (sqlite3_step(stmt) == SQLITE_ROW) {
        entry.typeClass = sqlite3string(stmt, 0)[0];
        entry.typeCode = static_cast<uint16_t>(sqlite3_column_int(stmt, 1));
        entry.typeDescription = sqlite3string(stmt, 2);
        entry.nationalTypeCode = sqlite3string(stmt, 3);
        entry.nationalTypeDescription = sqlite3string(stmt, 4);
    }
    (void) sqlite3_finalize(stmt);

    return entry;
}

LocationCodeList::Subtype LocationCodeList::subtype(char typeClass, uint16_t typeCode, uint16_t subtypeCode) const
{
    /* check */
    assert((typeClass == 'A') || (typeClass == 'L') || (typeClass == 'P'));
    assert(typeCode <= 12);
    assert(subtypeCode <= 47);

    /* prepare SQL statement */
    std::string sql = std::string("SELECT CLASS,TCD,STCD,SDESC,SNATCODE,SNATDESC FROM SUBTYPES WHERE CLASS='")
                      + typeClass + "' AND TCD=" + std::to_string(typeCode) + " AND STCD=" + std::to_string(subtypeCode);

    /* execute SQL statement */
    LocationCodeList::Subtype entry;
    sqlite3_stmt * stmt = nullptr;
    (void) sqlite3_prepare(m_database, sql.c_str(), static_cast<int>(sql.size()), &stmt, nullptr);
    if (sqlite3_step(stmt) == SQLITE_ROW) {
        entry.typeClass = sqlite3string(stmt, 0)[0];
        entry.typeCode = static_cast<uint16_t>(sqlite3_column_int(stmt, 1));
        entry.subtypeCode = static_cast<uint16_t>(sqlite3_column_int(stmt, 2));
        entry.subtypeDescription = sqlite3string(stmt, 3);
        entry.nationalSubtypeCode = sqlite3string(stmt, 4);
        entry.nationalSubtypeDescription = sqlite3string(stmt, 5);
    }
    (void) sqlite3_finalize(stmt);

    return entry;
}

std::vector<LocationCodeList::Language> LocationCodeList::languages() const
{
    /* prepare SQL statement */
    std::string sql = std::string("select CID,LID,LANGUAGE from LANGUAGES");

    /* execute SQL statement */
    std::vector<LocationCodeList::Language> list;
    sqlite3_stmt * stmt = nullptr;
    (void) sqlite3_prepare(m_database, sql.c_str(), static_cast<int>(sql.size()), &stmt, nullptr);
    if (sqlite3_step(stmt) == SQLITE_ROW) {
        LocationCodeList::Language entry;
        entry.countryId = static_cast<uint16_t>(sqlite3_column_int(stmt, 0));
        entry.languageId = static_cast<uint8_t>(sqlite3_column_int(stmt, 1));
        entry.language = sqlite3string(stmt, 2);
        list.push_back(entry);
    }
    (void) sqlite3_finalize(stmt);

    return list;
}

LocationCodeList::EuroRoadNo LocationCodeList::euroRoadNo(std::string europeanRoadNumber) const
{
    /* check */
    assert(!europeanRoadNumber.empty());

    /* prepare SQL statement */
    std::string sql = std::string("SELECT ENO,ECOMMENT FROM EUROROADNO WHERE ENO=") + europeanRoadNumber;

    /* execute SQL statement */
    LocationCodeList::EuroRoadNo entry;
    sqlite3_stmt * stmt = nullptr;
    (void) sqlite3_prepare(m_database, sql.c_str(), static_cast<int>(sql.size()), &stmt, nullptr);
    if (sqlite3_step(stmt) == SQLITE_ROW) {
        entry.europeanRoadNumber = sqlite3string(stmt, 0);
        entry.comment = sqlite3string(stmt, 1);
    }
    (void) sqlite3_finalize(stmt);

    return entry;
}

LocationCodeList::Name LocationCodeList::name(uint16_t countryId, uint8_t languageId, uint16_t nameId) const
{
    /* check */
    // assert(countryId <= 0xffff);
    // assert(languageId <= 0xff);
    // assert(nameId <= 0xffff);

    /* prepare SQL statement */
    std::string sql = std::string("SELECT CID,LID,NID,NAME,NCOMMENT FROM NAMES WHERE CID=")
                      + std::to_string(countryId) + " AND LID=" + std::to_string(languageId) + " AND NID="
                      + std::to_string(nameId);

    /* execute SQL statement */
    LocationCodeList::Name entry;
    sqlite3_stmt * stmt = nullptr;
    (void) sqlite3_prepare(m_database, sql.c_str(), static_cast<int>(sql.size()), &stmt, nullptr);
    if (sqlite3_step(stmt) == SQLITE_ROW) {
        entry.countryId = static_cast<uint16_t>(sqlite3_column_int(stmt, 0));
        entry.languageId = static_cast<uint8_t>(sqlite3_column_int(stmt, 1));
        entry.nameId = static_cast<uint16_t>(sqlite3_column_int(stmt, 2));
        entry.name = sqlite3string(stmt, 3);
        entry.comment = sqlite3string(stmt, 4);
    }
    (void) sqlite3_finalize(stmt);

    return entry;
}

LocationCodeList::NameTranslation LocationCodeList::nameTranslation(uint16_t countryId, uint16_t nameId) const
{
    /* check */
    // assert(countryId <= 0xffff);
    // assert(nameId <= 0xffff);

    /* prepare SQL statement */
    std::string sql = std::string("SELECT CID,LID,NID,NTRANSLATION FROM NAMETRANSLATIONS WHERE CID=")
                      + std::to_string(countryId) + " AND NID=" + std::to_string(nameId);

    /* execute SQL statement */
    LocationCodeList::NameTranslation entry;
    sqlite3_stmt * stmt = nullptr;
    (void) sqlite3_prepare(m_database, sql.c_str(), static_cast<int>(sql.size()), &stmt, nullptr);
    if (sqlite3_step(stmt) == SQLITE_ROW) {
        entry.countryId = static_cast<uint16_t>(sqlite3_column_int(stmt, 0));
        entry.languageId = static_cast<uint8_t>(sqlite3_column_int(stmt, 1));
        entry.nameId = static_cast<uint16_t>(sqlite3_column_int(stmt, 2));
        entry.translation = sqlite3string(stmt, 3);
    }
    (void) sqlite3_finalize(stmt);

    return entry;
}

LocationCodeList::SubTypeTranslation LocationCodeList::subTypeTranslation(uint16_t countryId, uint8_t languageId, char typeClass, uint16_t typeCode, uint16_t subTypeCode) const
{
    /* check */
    // assert(countryId <= 0xffff);
    // assert(languageId <= 0xff);
    assert((typeClass == 'A') || (typeClass == 'L') || (typeClass == 'P'));
    assert(typeCode <= 12);
    assert(subTypeCode <= 47);

    /* prepare SQL statement */
    std::string sql = std::string("SELECT CID,LID,CLASS,TCD,STCD,STRANSLATION FROM SUBTYPETRANSLATIONS WHERE CID=")
                      + std::to_string(countryId) + " AND LID=" + std::to_string(languageId) + " AND CLASS='" + typeClass
                      + "' AND TCD=" + std::to_string(typeCode) + " AND STCD=" + std::to_string(subTypeCode);

    /* execute SQL statement */
    LocationCodeList::SubTypeTranslation entry;
    sqlite3_stmt * stmt = nullptr;
    (void) sqlite3_prepare(m_database, sql.c_str(), static_cast<int>(sql.size()), &stmt, nullptr);
    if (sqlite3_step(stmt) == SQLITE_ROW) {
        entry.countryId = static_cast<uint16_t>(sqlite3_column_int(stmt, 0));
        entry.languageId = static_cast<uint8_t>(sqlite3_column_int(stmt, 1));
        entry.typeClass = sqlite3string(stmt, 2)[0];
        entry.typeCode = static_cast<uint16_t>(sqlite3_column_int(stmt, 3));
        entry.subtypeCode = static_cast<uint16_t>(sqlite3_column_int(stmt, 4));
        entry.translation = sqlite3string(stmt, 5);
    }
    (void) sqlite3_finalize(stmt);

    return entry;
}

std::vector<LocationCodeList::EuroRoadNoBelongsToCountry> LocationCodeList::euroRoadNoBelongsToCountries(std::string europeanRoadNumber) const
{
    /* prepare SQL statement */
    std::string sql = std::string("select CID,ENO from ERNO_BELONGS_TO_CO WHERE ENO='") + europeanRoadNumber + "'";

    /* execute SQL statement */
    std::vector<LocationCodeList::EuroRoadNoBelongsToCountry> list;
    sqlite3_stmt * stmt = nullptr;
    (void) sqlite3_prepare(m_database, sql.c_str(), static_cast<int>(sql.size()), &stmt, nullptr);
    if (sqlite3_step(stmt) == SQLITE_ROW) {
        LocationCodeList::EuroRoadNoBelongsToCountry entry;
        entry.countryId = static_cast<uint8_t>(sqlite3_column_int(stmt, 0));
        entry.europeanRoadNumber = sqlite3string(stmt, 1);
        list.push_back(entry);
    }
    (void) sqlite3_finalize(stmt);

    return list;
}

LocationCodeList::AdministrativeArea LocationCodeList::administrativeArea(uint16_t countryId, uint8_t tableCode, uint16_t locationCode) const
{
    /* check */
    // assert(countryId <= 0xffff);
    assert(tableCode <= 0x3f);
    // assert(locationCode <= 0xffff);

    /* prepare SQL statement */
    std::string sql = std::string("SELECT CID,TABCD,LCD,CLASS,TCD,STCD,NID,POL_LCD FROM ADMINISTRATIVEAREA WHERE CID=")
                      + std::to_string(countryId) + " AND TABCD=" + std::to_string(tableCode) + " AND LCD="
                      + std::to_string(locationCode);

    /* execute SQL statement */
    LocationCodeList::AdministrativeArea entry;
    sqlite3_stmt * stmt = nullptr;
    (void) sqlite3_prepare(m_database, sql.c_str(), static_cast<int>(sql.size()), &stmt, nullptr);
    if (sqlite3_step(stmt) == SQLITE_ROW) {
        entry.countryId = static_cast<uint16_t>(sqlite3_column_int(stmt, 0));
        entry.tableCode = static_cast<uint8_t>(sqlite3_column_int(stmt, 1));
        entry.locationCode = static_cast<uint16_t>(sqlite3_column_int(stmt, 2));
        entry.typeClass = sqlite3string(stmt, 3)[0];
        entry.typeCode = static_cast<uint8_t>(sqlite3_column_int(stmt, 4));
        entry.subtypeCode = static_cast<uint8_t>(sqlite3_column_int(stmt, 5));
        entry.name = static_cast<uint16_t>(sqlite3_column_int(stmt, 6));
        entry.upwardAreaReference = static_cast<uint16_t>(sqlite3_column_int(stmt, 7));
    }
    (void) sqlite3_finalize(stmt);

    return entry;
}

LocationCodeList::OtherArea LocationCodeList::otherArea(uint16_t countryId, uint8_t tableCode, uint16_t locationCode) const
{
    /* check */
    // assert(countryId <= 0xffff);
    assert(tableCode <= 0x3f);
    // assert(locationCode <= 0xffff);

    /* prepare SQL statement */
    std::string sql = std::string("SELECT CID,TABCD,LCD,CLASS,TCD,STCD,NID,POL_LCD FROM OTHERAREAS WHERE CID=")
                      + std::to_string(countryId) + " AND TABCD=" + std::to_string(tableCode) + " AND LCD="
                      + std::to_string(locationCode);

    /* execute SQL statement */
    LocationCodeList::OtherArea entry;
    sqlite3_stmt * stmt = nullptr;
    (void) sqlite3_prepare(m_database, sql.c_str(), static_cast<int>(sql.size()), &stmt, nullptr);
    if (sqlite3_step(stmt) == SQLITE_ROW) {
        entry.countryId = static_cast<uint16_t>(sqlite3_column_int(stmt, 0));
        entry.tableCode = static_cast<uint8_t>(sqlite3_column_int(stmt, 1));
        entry.locationCode = static_cast<uint16_t>(sqlite3_column_int(stmt, 2));
        entry.typeClass = sqlite3string(stmt, 3)[0];
        entry.typeCode = static_cast<uint8_t>(sqlite3_column_int(stmt, 4));
        entry.subtypeCode = static_cast<uint8_t>(sqlite3_column_int(stmt, 5));
        entry.name1 = static_cast<uint16_t>(sqlite3_column_int(stmt, 6));
        entry.adminAreaReference = static_cast<uint16_t>(sqlite3_column_int(stmt, 7));
    }
    (void) sqlite3_finalize(stmt);

    return entry;
}

LocationCodeList::Road LocationCodeList::road(uint16_t countryId, uint8_t tableCode, uint16_t locationCode) const
{
    /* check */
    // assert(countryId <= 0xffff);
    assert(tableCode <= 0x3f);
    // assert(locationCode <= 0xffff);

    /* prepare SQL statement */
    std::string sql = std::string("SELECT CID,TABCD,LCD,CLASS,TCD,STCD,ROADNUMBER,RNID,N1ID,N2ID,POL_LCD,PES_LEV FROM ROADS WHERE CID=")
                      + std::to_string(countryId) + " AND TABCD=" + std::to_string(tableCode) + " AND LCD="
                      + std::to_string(locationCode);

    /* execute SQL statement */
    LocationCodeList::Road entry;
    sqlite3_stmt * stmt = nullptr;
    (void) sqlite3_prepare(m_database, sql.c_str(), static_cast<int>(sql.size()), &stmt, nullptr);
    if (sqlite3_step(stmt) == SQLITE_ROW) {
        entry.countryId = static_cast<uint16_t>(sqlite3_column_int(stmt, 0));
        entry.tableCode = static_cast<uint8_t>(sqlite3_column_int(stmt, 1));
        entry.locationCode = static_cast<uint16_t>(sqlite3_column_int(stmt, 2));
        entry.typeClass = sqlite3string(stmt, 3)[0];
        entry.typeCode = static_cast<uint8_t>(sqlite3_column_int(stmt, 4));
        entry.subtypeCode = static_cast<uint8_t>(sqlite3_column_int(stmt, 5));
        entry.roadNumber = sqlite3string(stmt, 6);
        entry.roadName = static_cast<uint16_t>(sqlite3_column_int(stmt, 7));
        entry.name1 = static_cast<uint16_t>(sqlite3_column_int(stmt, 8));
        entry.name2 = static_cast<uint16_t>(sqlite3_column_int(stmt, 9));
        entry.adminAreaReference = static_cast<uint16_t>(sqlite3_column_int(stmt, 10));
        entry.roadNetworkLevel = static_cast<uint8_t>(sqlite3_column_int(stmt, 11));
    }
    (void) sqlite3_finalize(stmt);

    return entry;
}

LocationCodeList::RoadNetworkLevelType LocationCodeList::roadNetworkLevelType(uint8_t roadNetworkLevel) const
{
    /* check */
    assert(roadNetworkLevel <= 15);

    /* prepare SQL statement */
    std::string sql = std::string("SELECT PES_LEV,PES_LEV_DESC,TDESC FROM ROAD_NETWORK_LEVEL_TYPES WHERE PES_LEV=")
                      + std::to_string(roadNetworkLevel);

    /* execute SQL statement */
    LocationCodeList::RoadNetworkLevelType entry;
    sqlite3_stmt * stmt = nullptr;
    (void) sqlite3_prepare(m_database, sql.c_str(), static_cast<int>(sql.size()), &stmt, nullptr);
    if (sqlite3_step(stmt) == SQLITE_ROW) {
        entry.roadNetworkLevel = static_cast<uint8_t>(sqlite3_column_int(stmt, 0));
        entry.roadNetworkLevelDescription = sqlite3string(stmt, 1);
        entry.nationalRoadNetworkLevelDescription = sqlite3string(stmt, 2);
    }
    (void) sqlite3_finalize(stmt);

    return entry;
}

LocationCodeList::Segment LocationCodeList::segment(uint16_t countryId, uint8_t tableCode, uint16_t locationCode) const
{
    /* check */
    // assert(countryId <= 0xffff);
    assert(tableCode <= 0x3f);
    // assert(locationCode <= 0xffff);

    /* prepare SQL statement */
    std::string sql = std::string("SELECT CID,TABCD,LCD,CLASS,TCD,STCD,ROADNUMBER,RNID,N1ID,N2ID,ROA_LCD,SEG_LCD,POL_LCD FROM SEGMENTS WHERE CID=")
                      + std::to_string(countryId) + " AND TABCD=" + std::to_string(tableCode) + " AND LCD="
                      + std::to_string(locationCode);

    /* execute SQL statement */
    LocationCodeList::Segment entry;
    sqlite3_stmt * stmt = nullptr;
    (void) sqlite3_prepare(m_database, sql.c_str(), static_cast<int>(sql.size()), &stmt, nullptr);
    if (sqlite3_step(stmt) == SQLITE_ROW) {
        entry.countryId = static_cast<uint16_t>(sqlite3_column_int(stmt, 0));
        entry.tableCode = static_cast<uint8_t>(sqlite3_column_int(stmt, 1));
        entry.locationCode = static_cast<uint16_t>(sqlite3_column_int(stmt, 2));
        entry.typeClass = sqlite3string(stmt, 3)[0];
        entry.typeCode = static_cast<uint8_t>(sqlite3_column_int(stmt, 4));
        entry.subtypeCode = static_cast<uint8_t>(sqlite3_column_int(stmt, 5));
        entry.roadNumber = sqlite3string(stmt, 6);
        entry.roadName = static_cast<uint16_t>(sqlite3_column_int(stmt, 7));
        entry.name1 = static_cast<uint16_t>(sqlite3_column_int(stmt, 8));
        entry.name2 = static_cast<uint16_t>(sqlite3_column_int(stmt, 9));
        entry.roadReference = static_cast<uint16_t>(sqlite3_column_int(stmt, 10));
        entry.segmentReference = static_cast<uint16_t>(sqlite3_column_int(stmt, 11));
        entry.adminAreaReference = static_cast<uint16_t>(sqlite3_column_int(stmt, 12));
    }
    (void) sqlite3_finalize(stmt);

    return entry;
}

LocationCodeList::SegHasEuroRoadNo LocationCodeList::segHasEuroRoadNo(uint16_t countryId, uint8_t tableCode, uint16_t locationCode) const
{
    /* check */
    // assert(countryId <= 0xffff);
    assert(tableCode <= 0x3f);
    // assert(locationCode <= 0xffff);

    /* prepare SQL statement */
    std::string sql = std::string("SELECT CID,TABCD,LCD,ENO FROM SEG_HAS_ERNO WHERE CID=")
                      + std::to_string(countryId) + " AND TABCD=" + std::to_string(tableCode) + " AND LCD="
                      + std::to_string(locationCode);

    /* execute SQL statement */
    LocationCodeList::SegHasEuroRoadNo entry;
    sqlite3_stmt * stmt = nullptr;
    (void) sqlite3_prepare(m_database, sql.c_str(), static_cast<int>(sql.size()), &stmt, nullptr);
    if (sqlite3_step(stmt) == SQLITE_ROW) {
        entry.countryId = static_cast<uint16_t>(sqlite3_column_int(stmt, 0));
        entry.tableCode = static_cast<uint8_t>(sqlite3_column_int(stmt, 1));
        entry.locationCode = static_cast<uint16_t>(sqlite3_column_int(stmt, 2));
        entry.europeanRoadNumber = sqlite3string(stmt, 3);
    }
    (void) sqlite3_finalize(stmt);

    return entry;
}

LocationCodeList::SOffset LocationCodeList::sOffset(uint16_t countryId, uint8_t tableCode, uint16_t locationCode) const
{
    /* check */
    // assert(countryId <= 0xffff);
    assert(tableCode <= 0x3f);
    // assert(locationCode <= 0xffff);

    /* prepare SQL statement */
    std::string sql = std::string("SELECT CID,TABCD,LCD,NEG_OFF_LCD,POS_OFF_LCD FROM SOFFSETS WHERE CID=")
                      + std::to_string(countryId) + " AND TABCD=" + std::to_string(tableCode) + " AND LCD="
                      + std::to_string(locationCode);

    /* execute SQL statement */
    LocationCodeList::SOffset entry;
    sqlite3_stmt * stmt = nullptr;
    (void) sqlite3_prepare(m_database, sql.c_str(), static_cast<int>(sql.size()), &stmt, nullptr);
    if (sqlite3_step(stmt) == SQLITE_ROW) {
        entry.countryId = static_cast<uint16_t>(sqlite3_column_int(stmt, 0));
        entry.tableCode = static_cast<uint8_t>(sqlite3_column_int(stmt, 1));
        entry.locationCode = static_cast<uint16_t>(sqlite3_column_int(stmt, 2));
        entry.negativeOffset = static_cast<uint16_t>(sqlite3_column_int(stmt, 3));
        entry.positiveOffset = static_cast<uint16_t>(sqlite3_column_int(stmt, 4));
    }
    (void) sqlite3_finalize(stmt);

    return entry;
}

LocationCodeList::Point LocationCodeList::point(uint16_t countryId, uint8_t tableCode, uint16_t locationCode) const
{
    /* check */
    // assert(countryId <= 0xffff);
    assert(tableCode <= 0x3f);
    // assert(locationCode <= 0xffff);

    /* prepare SQL statement */
    std::string sql = std::string("SELECT CID,TABCD,LCD,CLASS,TCD,STCD,JUNCTIONNUMBER,RNID,N1ID,N2ID,POL_LCD,OTH_LCD,SEG_LCD,ROA_LCD,INPOS,INNEG,OUTPOS,OUTNEG,PRESENTPOS,PRESENTNEG,DIVERSIONNEG,DIVERSIONPOS,XCOORD,YCOORD,INTERRUPTSROAD,URBAN FROM POINTS WHERE CID=")
                      + std::to_string(countryId) + " AND TABCD=" + std::to_string(tableCode) + " AND LCD="
                      + std::to_string(locationCode);

    /* execute SQL statement */
    LocationCodeList::Point entry;
    sqlite3_stmt * stmt = nullptr;
    (void) sqlite3_prepare(m_database, sql.c_str(), static_cast<int>(sql.size()), &stmt, nullptr);
    if (sqlite3_step(stmt) == SQLITE_ROW) {
        entry.countryId = static_cast<uint16_t>(sqlite3_column_int(stmt, 0));
        entry.tableCode = static_cast<uint8_t>(sqlite3_column_int(stmt, 1));
        entry.locationCode = static_cast<uint16_t>(sqlite3_column_int(stmt, 2));
        entry.typeClass = sqlite3string(stmt, 3)[0];
        entry.typeCode = static_cast<uint8_t>(sqlite3_column_int(stmt, 4));
        entry.subtypeCode = static_cast<uint8_t>(sqlite3_column_int(stmt, 5));
        entry.junctionNumber = sqlite3string(stmt, 6);
        entry.roadName = static_cast<uint16_t>(sqlite3_column_int(stmt, 7));
        entry.name1 = static_cast<uint16_t>(sqlite3_column_int(stmt, 8));
        entry.name2 = static_cast<uint16_t>(sqlite3_column_int(stmt, 9));
        entry.adminAreaReference = static_cast<uint16_t>(sqlite3_column_int(stmt, 10));
        entry.otherAreaReference = static_cast<uint16_t>(sqlite3_column_int(stmt, 11));
        entry.segmentReference = static_cast<uint16_t>(sqlite3_column_int(stmt, 12));
        entry.roadReference = static_cast<uint16_t>(sqlite3_column_int(stmt, 13));
        entry.inPos = static_cast<uint8_t>(sqlite3_column_int(stmt, 14));
        entry.inNeg = static_cast<uint8_t>(sqlite3_column_int(stmt, 15));
        entry.outPos = static_cast<uint8_t>(sqlite3_column_int(stmt, 16));
        entry.outNeg = static_cast<uint8_t>(sqlite3_column_int(stmt, 17));
        entry.presentPos = static_cast<uint8_t>(sqlite3_column_int(stmt, 18));
        entry.presentNeg = static_cast<uint8_t>(sqlite3_column_int(stmt, 19));
        entry.diversionPos = sqlite3string(stmt, 20);
        entry.diversionNeg = sqlite3string(stmt, 21);
        entry.xCoord = sqlite3string(stmt, 22);
        entry.yCoord = sqlite3string(stmt, 23);
        entry.interruptsRoad = static_cast<uint16_t>(sqlite3_column_int(stmt, 24));
        entry.urban = static_cast<uint8_t>(sqlite3_column_int(stmt, 25));
    }
    (void) sqlite3_finalize(stmt);

    return entry;
}

LocationCodeList::POffset LocationCodeList::pOffset(uint16_t countryId, uint8_t tableCode, uint16_t locationCode) const
{
    /* check */
    // assert(countryId <= 0xffff);
    assert(tableCode <= 0x3f);
    // assert(locationCode <= 0xffff);

    /* prepare SQL statement */
    std::string sql = std::string("SELECT CID,TABCD,LCD,NEG_OFF_LCD,POS_OFF_LCD FROM POFFSETS WHERE CID=")
                      + std::to_string(countryId) + " AND TABCD=" + std::to_string(tableCode) + " AND LCD="
                      + std::to_string(locationCode);

    /* execute SQL statement */
    LocationCodeList::POffset entry;
    sqlite3_stmt * stmt = nullptr;
    (void) sqlite3_prepare(m_database, sql.c_str(), static_cast<int>(sql.size()), &stmt, nullptr);
    if (sqlite3_step(stmt) == SQLITE_ROW) {
        entry.countryId = static_cast<uint16_t>(sqlite3_column_int(stmt, 0));
        entry.tableCode = static_cast<uint8_t>(sqlite3_column_int(stmt, 1));
        entry.locationCode = static_cast<uint16_t>(sqlite3_column_int(stmt, 2));
        entry.negativeOffset = static_cast<uint16_t>(sqlite3_column_int(stmt, 3));
        entry.positiveOffset = static_cast<uint16_t>(sqlite3_column_int(stmt, 4));
    }
    (void) sqlite3_finalize(stmt);

    return entry;
}

LocationCodeList::Intersection LocationCodeList::intersection(uint16_t countryId, uint8_t tableCode, uint16_t locationCode) const
{
    /* check */
    // assert(countryId <= 0xffff);
    assert(tableCode <= 0x3f);
    // assert(locationCode <= 0xffff);

    /* prepare SQL statement */
    std::string sql = std::string("SELECT CID,TABCD,LCD,INT_CID,INT_TABCD,INT_LCD FROM INTERSECTIONS WHERE CID=")
                      + std::to_string(countryId) + " AND TABCD=" + std::to_string(tableCode) + " AND LCD="
                      + std::to_string(locationCode);

    /* execute SQL statement */
    LocationCodeList::Intersection entry;
    sqlite3_stmt * stmt = nullptr;
    (void) sqlite3_prepare(m_database, sql.c_str(), static_cast<int>(sql.size()), &stmt, nullptr);
    if (sqlite3_step(stmt) == SQLITE_ROW) {
        entry.countryId = static_cast<uint16_t>(sqlite3_column_int(stmt, 0));
        entry.tableCode = static_cast<uint8_t>(sqlite3_column_int(stmt, 1));
        entry.locationCode = static_cast<uint16_t>(sqlite3_column_int(stmt, 2));
        entry.intersectionCountryId = static_cast<uint16_t>(sqlite3_column_int(stmt, 3));
        entry.intersectionTableCode = static_cast<uint8_t>(sqlite3_column_int(stmt, 4));
        entry.intersectionLocationCode = static_cast<uint16_t>(sqlite3_column_int(stmt, 5));
    }
    (void) sqlite3_finalize(stmt);

    return entry;
}

LocationCodeList::TypeUnion LocationCodeList::typeUnion(char typeClass, uint16_t typeCode, uint16_t subtypeCode) const
{
    /* check */
    assert((typeClass == 'A') || (typeClass == 'L') || (typeClass == 'P'));
    assert(typeCode <= 12);
    assert(subtypeCode <= 47);

    /* prepare SQL statement */
    std::string sql = std::string("SELECT CLASS,TCD,STCD,DESC,NATCODE,NATDESC FROM TYPESUNION WHERE CLASS='")
                      + typeClass + "' AND TCD=" + std::to_string(typeCode) + " AND STCD=" + std::to_string(subtypeCode);

    /* execute SQL statement */
    LocationCodeList::TypeUnion entry;
    sqlite3_stmt * stmt = nullptr;
    (void) sqlite3_prepare(m_database, sql.c_str(), static_cast<int>(sql.size()), &stmt, nullptr);
    if (sqlite3_step(stmt) == SQLITE_ROW) {
        entry.typeClass = sqlite3string(stmt, 0)[0];
        entry.typeCode = static_cast<uint16_t>(sqlite3_column_int(stmt, 1));
        entry.subtypeCode = static_cast<uint16_t>(sqlite3_column_int(stmt, 2));
        entry.description = sqlite3string(stmt, 3);
        entry.nationalCode = sqlite3string(stmt, 4);
        entry.nationalDescription = sqlite3string(stmt, 5);
    }
    (void) sqlite3_finalize(stmt);

    return entry;
}

char LocationCodeList::LocationUnion::origin() const
{
    char retVal = 0;

    switch (typeClass) {
    case 'A':
        switch (typeCode) {
        case 1:
        case 2:
        case 3:
        case 5:
            retVal = 'A';
            break;
        case 6:
            retVal = 'O';
            break;
        case 7:
        case 8:
        case 9:
        case 10:
        case 11:
            retVal = 'A';
            break;
        case 12:
            retVal = 'O';
            break;
        default:
            assert(false);
        }
        break;
    case 'L':
        switch (typeCode) {
        case 1:
        case 2:
            retVal = 'R';
            break;
        case 3:
        case 4:
            retVal = 'S';
            break;
        case 5:
        case 6:
        case 7:
        case 8:
            retVal = 'R';
            break;
        default:
            assert(false);
        }
        break;
    case 'P':
        retVal = 'P';
        break;
    default:
        assert(false);
    }
    return retVal;
}

LocationCodeList::LocationUnion LocationCodeList::locationUnion(uint16_t countryId, uint8_t tableCode, uint16_t locationCode) const
{
    /* check */
    // assert(countryId <= 0xffff);
    assert(tableCode <= 0x3f);
    // assert(locationCode <= 0xffff);

    /* prepare SQL statement */
    std::string sql = std::string("SELECT CID,TABCD,LCD,CLASS,TCD,STCD,NUMBER,RNID,N1ID,N2ID,POL_LCD,OTH_LCD,SEG_LCD,ROA_LCD,URBAN FROM LOCATIONSUNION WHERE CID=")
                      + std::to_string(countryId) + " AND TABCD=" + std::to_string(tableCode) + " AND LCD="
                      + std::to_string(locationCode);

    /* execute SQL statement */
    LocationCodeList::LocationUnion entry;
    sqlite3_stmt * stmt = nullptr;
    (void) sqlite3_prepare(m_database, sql.c_str(), static_cast<int>(sql.size()), &stmt, nullptr);
    if (sqlite3_step(stmt) == SQLITE_ROW) {
        entry.countryId = static_cast<uint16_t>(sqlite3_column_int(stmt, 0));
        entry.tableCode = static_cast<uint8_t>(sqlite3_column_int(stmt, 1));
        entry.locationCode = static_cast<uint16_t>(sqlite3_column_int(stmt, 2));
        entry.typeClass = sqlite3string(stmt, 3)[0];
        entry.typeCode = static_cast<uint8_t>(sqlite3_column_int(stmt, 4));
        entry.subtypeCode = static_cast<uint8_t>(sqlite3_column_int(stmt, 5));
        entry.number = sqlite3string(stmt, 6);
        entry.roadName = static_cast<uint16_t>(sqlite3_column_int(stmt, 7));
        entry.name1 = static_cast<uint16_t>(sqlite3_column_int(stmt, 8));
        entry.name2 = static_cast<uint16_t>(sqlite3_column_int(stmt, 9));
        entry.adminAreaReference = static_cast<uint16_t>(sqlite3_column_int(stmt, 10));
        entry.otherAreaReference = static_cast<uint16_t>(sqlite3_column_int(stmt, 11));
        entry.segmentReference = static_cast<uint16_t>(sqlite3_column_int(stmt, 12));
        entry.roadReference = static_cast<uint16_t>(sqlite3_column_int(stmt, 13));
        entry.urban = static_cast<uint8_t>(sqlite3_column_int(stmt, 14));
    }
    (void) sqlite3_finalize(stmt);

    return entry;
}

LocationCodeList::OffsetUnion LocationCodeList::offsetUnion(uint16_t countryId, uint8_t tableCode, uint16_t locationCode) const
{
    /* check */
    // assert(countryId <= 0xffff);
    assert(tableCode <= 0x3f);
    // assert(locationCode <= 0xffff);

    /* prepare SQL statement */
    std::string sql = std::string("SELECT CID,TABCD,LCD,NEG_OFF_LCD,POS_OFF_LCD FROM OFFSETSUNION WHERE CID=")
                      + std::to_string(countryId) + " AND TABCD=" + std::to_string(tableCode) + " AND LCD="
                      + std::to_string(locationCode);

    /* execute SQL statement */
    LocationCodeList::OffsetUnion entry;
    sqlite3_stmt * stmt = nullptr;
    (void) sqlite3_prepare(m_database, sql.c_str(), static_cast<int>(sql.size()), &stmt, nullptr);
    if (sqlite3_step(stmt) == SQLITE_ROW) {
        entry.countryId = static_cast<uint16_t>(sqlite3_column_int(stmt, 0));
        entry.tableCode = static_cast<uint8_t>(sqlite3_column_int(stmt, 1));
        entry.locationCode = static_cast<uint16_t>(sqlite3_column_int(stmt, 2));
        entry.negativeOffset = static_cast<uint16_t>(sqlite3_column_int(stmt, 3));
        entry.positiveOffset = static_cast<uint16_t>(sqlite3_column_int(stmt, 4));
    }
    (void) sqlite3_finalize(stmt);

    return entry;
}

}
