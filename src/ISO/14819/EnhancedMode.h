/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include <cstdint>
#include <vector>

#include <ISO/62106.h>

#include "MessageGeographicalScope.h"

#include "iso_14819_export.h"
#include "platform.h"

namespace ISO14819 {

/** Enhanced Mode */
class ISO_14819_EXPORT EnhancedMode
{
public:
    explicit EnhancedMode();

    /**
     * Get activity time (0..3 => 1, 2, 4, 8 seconds).
     *
     * @return Activity time (0..3 => 1, 2, 4, 8 seconds)
     */
    uint8_t activityTime() const;

    /**
     * Set activity time (0..3 => 1, 2, 4, 8 seconds).
     *
     * @param[in] value Activity time (0..3 => 1, 2, 4, 8 seconds)
     */
    void setActivityTime(uint8_t value);

    /**
     * Get window time (0..3 => 1, 2, 4, 8 seconds).
     *
     * @return Window time (0..3 => 1, 2, 4, 8 seconds)
     */
    uint8_t windowTime() const;

    /**
     * Set window time (0..3 => 1, 2, 4, 8 seconds).
     *
     * @param[in] value Window time (0..3 => 1, 2, 4, 8 seconds)
     */
    void setWindowTime(uint8_t value);

    /**
     * Get delay time (seconds).
     *
     * @return Delay time (seconds)
     */
    uint8_t delayTime() const;

    /**
     * Set delay time (seconds).
     *
     * @param[in] value Delay time (seconds)
     */
    void setDelayTime(uint8_t value);

private:
    /* Time Constraint: 60 (seconds)/(Ta+Tw) = n (where n is an integer > 0) */

    /** activity time (seconds): 0..3 => 1, 2, 4, 8 seconds */
    uint8_t m_activityTime : 2;

    /** window time (seconds): 0..3 => 1, 2, 4, 8 seconds */
    uint8_t m_windowTime : 2;

    /** delay time (seconds): 0..3 => 0, 1, 2, 3 seconds */
    uint8_t m_delayTime : 2;
};

};
