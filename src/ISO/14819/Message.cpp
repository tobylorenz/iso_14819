/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "Message.h"

#include <algorithm>
#include <cassert>
#include <set>

#include "EventList.h"
#include "TrafficMessageChannel.h"

namespace ISO14819 {

Message::Message() :
    telNumInformation(),
    telNumReport(),
    systemInformation(),
    eventListEntry(),
    receiveTime(),
    persistenceTime(),
    decrementTime(),
    startTime(),
    stopTime(),
    m_applicationId(0),
    m_groupsExpected(0),
    m_groupsReceived(0),
    m_event(0),
    m_interRoadLocationTableCountryCode(0),
    m_interRoadLocationTableNumber(0),
    m_location(0),
    m_direction(false),
    m_extent(0),
    m_durationPersistence(0),
    m_diversionAdvice(false),
    m_freeFormatFifo(),
    m_omcNextLabel(0),
    m_omcNextSubLabel(0),
    m_freeFormatState(FreeFormatState::Label),
    m_optionalMessageContent()
{
}

uint16_t Message::applicationId() const
{
    return m_applicationId;
}

void Message::setApplicationId(uint16_t value)
{
    m_applicationId = value;
}

bool Message::testMessage() const
{
    return (m_applicationId == TrafficMessageChannel::applicationIdTest);
}

void Message::setGroupsExpected(uint8_t value)
{
    /* check */
    assert(value <= 5);

    /* set */
    m_groupsExpected = value;
}

void Message::incGroupsReceived()
{
    /* check */
    assert(m_groupsReceived < 5);

    /* increase */
    m_groupsReceived++;
}

bool Message::complete() const
{
    /* For multi-group messages, with just first-message received, the groupsExpected is 0 and
     * the groupsReceived is 1, so this comparison works also. */
    return m_groupsReceived == m_groupsExpected;
}

uint16_t Message::event() const
{
    return m_event;
}

void Message::setEvent(uint16_t value)
{
    /* check */
    assert(value <= 0x7ff);

    /* set event */
    m_event = value;

    /* fill in implicit information from event list */
    eventListEntry = eventList().event(value);
}

uint8_t Message::locationTableExtendedCountryCode() const
{
    uint8_t ltecc = systemInformation.locationTableExtendedCountryCode();
    if (ltecc == 0) {
        ltecc = systemInformation.extendedCountryCode();
    }
    return ltecc;
}

uint8_t Message::locationTableCountryCode() const
{
    uint8_t ltcc = m_interRoadLocationTableCountryCode;
    if (ltcc == 0) {
        ltcc = systemInformation.locationTableCountryCode();
    }
    if (ltcc == 0) {
        ltcc = systemInformation.countryCode();
    }
    return ltcc;
}

uint8_t Message::locationTableNumber() const
{
    uint8_t ltn = m_interRoadLocationTableNumber;
    if (ltn == 0) {
        ltn = systemInformation.locationTableNumber();
    }
    return ltn;
}

uint16_t Message::location() const
{
    return m_location;
}

void Message::setLocation(uint16_t value)
{
    /* check */
    // assert(value <= 0xffff);

    /* set location */
    m_location = value;
}

void Message::setInterRoadLocation(uint8_t irltcc, uint8_t irltn, uint16_t irl)
{
    /* check */
    assert(irltcc <= 0xf);
    assert(irltn <= 0x3f);
    // assert(irl <= 0xffff);

    /* set location */
    m_interRoadLocationTableCountryCode = irltcc;
    m_interRoadLocationTableNumber = irltn;
    m_location = irl;
}

uint8_t Message::direction() const
{
    return m_direction;
}

void Message::setDirection(uint8_t value)
{
    /* check */
    assert(value <= 0x1);

    /* set location */
    m_direction = value;
}

uint8_t Message::extent() const
{
    return m_extent;
}

void Message::setExtent(uint8_t value)
{
    /* check */
    assert(value <= 0x1f);

    /* set location */
    m_extent = value;
}

uint8_t Message::durationPersistence() const
{
    return m_durationPersistence;
}

void Message::setDurationPersistence(uint8_t value)
{
    /* check */
    assert(value <= 7);

    /* set duration/persistence */
    m_durationPersistence = value;

    /* set persistence time */
    setPersistenceTime();

    /* set decrement time */
    decrementTime = receiveTime;
    setDecrementTime();
}

bool Message::diversionAdvice() const
{
    return m_diversionAdvice;
}

void Message::setDiversionAdvice(bool value)
{
    /* check */
    // assert(value <= 0x1);

    /* set location */
    m_diversionAdvice = value;
}

std::vector<OptionalMessageContent> Message::optionalMessageContent() const
{
    return m_optionalMessageContent;
}

void Message::addOptionalMessageContent(uint8_t label, uint16_t data)
{
    /* check */
    assert(label <= 0xf);

    /* add Optional Message Content */
    OptionalMessageContent omc;
    omc.setLabel(label);
    omc.setData(data);
    m_optionalMessageContent.push_back(omc);
}

std::set<uint16_t> Message::updateClasses() const
{
    std::set<uint16_t> retVal;

    /* insert update class of primary event */
    retVal.insert(eventListEntry.updateClass());

    /* insert update classes of optional message content */
    for (const OptionalMessageContent & omc : m_optionalMessageContent) {
        if (omc.label() == 9) {
            uint16_t additionalEvent = omc.data();
            Event additionalELE = eventList().event(additionalEvent);
            retVal.insert(additionalELE.updateClass());
        }
    }

    return retVal;
}

std::set<char> Message::durationTypes() const
{
    std::set<char> retVal;

    /* insert update class of primary event */
    retVal.insert(eventListEntry.durationType());

    /* insert update classes of optional message content */
    for (const OptionalMessageContent & omc : m_optionalMessageContent) {
        if (omc.label() == 9) {
            uint16_t additionalEvent = omc.data();
            Event additionalELE = eventList().event(additionalEvent);
            retVal.insert(additionalELE.durationType());
        }
    }

    return retVal;
}

void Message::pushFreeFormat(uint32_t data, uint8_t bits)
{
    /* check */
    assert(size > 0);

    /* push data into free format bit field */
    m_freeFormatFifo.push(data, bits);

    /* try to extract as much data as possible */
    bool readMore = true;
    while (readMore) {
        switch (m_freeFormatState) {
        case FreeFormatState::Label:
            if (m_freeFormatFifo.size() >= 4) {
                m_omcNextLabel = m_freeFormatFifo.pull(4) & 0xf;
                if (m_omcNextLabel == 15) {
                    m_freeFormatState = FreeFormatState::SubLabel;
                } else {
                    m_freeFormatState = FreeFormatState::Data;
                }
            } else {
                readMore = false;
            }
            break;
        case FreeFormatState::SubLabel:
            if (m_freeFormatFifo.size() >= 6) {
                m_omcNextSubLabel = m_freeFormatFifo.pull(6) & 0x3f;
                m_freeFormatState = FreeFormatState::SubData;

                /* push an OMC to show that a sub label was received */
                addOptionalMessageContent(m_omcNextLabel, m_omcNextSubLabel);
            } else {
                readMore = false;
            }
            break;
        case FreeFormatState::Data: {
            uint8_t dataSize = OptionalMessageContent::size(m_omcNextLabel);
            if (m_freeFormatFifo.size() >= dataSize) {
                uint16_t data = m_freeFormatFifo.pull(dataSize);
                m_freeFormatState = FreeFormatState::Label;

                /* push optional message content */
                if ((m_omcNextLabel == 0) && (data == 0)) {
                    /* null content */
                    return;
                } else {
                    addOptionalMessageContent(m_omcNextLabel, data);
                }
            } else {
                readMore = false;
            }
        }
        break;
        case FreeFormatState::SubData:
            switch (m_omcNextSubLabel) {
            case 1:
                if (telNumInformation.pull(m_freeFormatFifo)) {
                    /* complete */
                    m_freeFormatState = FreeFormatState::Label;
                } else {
                    readMore = false;
                };
                break;
            case 2:
                if (telNumReport.pull(m_freeFormatFifo)) {
                    /* complete */
                    m_freeFormatState = FreeFormatState::Label;
                } else {
                    readMore = false;
                };
                break;
            default: // 0, 3-62, 53
                /* Reserved for future use */
                // stop here. remain in this state.
                return;
            }
            break;
        }
    }
}

ISO62106::RdsTime Message::startStopTime(ISO62106::RdsTime time, uint8_t data)
{
    /* check */
    assert(data <= 0xff);

    /* convert to local time */
    time.toLocalTime();

    if (data <= 95) {
        /* 00:00 to 23:45 (15 minute interval) */
        /* spoken as "today at &&:&& UTC" */
        uint8_t hour = data / 4;
        uint8_t minute = 15 * (data % 4);
        time.setHour(hour);
        time.setMinute(minute);
    } else if ((data >= 96) && (data <= 200)) {
        /* Hour and day, starting at midnight following message receipt (1 hour interval) */
        /* spoken as "in %% days at %%:00 UTC" */
        uint8_t days = (data - 96) / 24;
        uint8_t hours = (data - 96) % 24;
        /* set time to (upcoming) midnight */
        time.setHour(0);
        time.setMinute(0);
        time.addTime(1 + days, hours, 0);
    } else if ((data >= 201) && (data <= 231)) {
        /* 1st to 31st day of the month (1 day interval) */
        /* spoken as "in %% days" */
        // @note Crazy things happen, if the month doesn't have 31 days!
        uint16_t year = time.year();
        uint8_t month = time.month();
        uint8_t day = data - 201 + 1;
        if (day <= time.day()) {
            month++;
            /* month overflow */
            if (month > 12) {
                month = 1;
                year++;
            }
        };
        time.setDate(year, month, day);
        time.setTime(0, 0);
    } else { /* 232..255 */
        /* 15th January to 31st December (half month interval) */
        /* spoken as "mid/end of %%.month" */
        uint16_t year = time.year();
        uint8_t month = 1 + (data - 232) / 2;
        bool midOfMonth = ((data - 232) % 2) == 0;
        if (month <= time.month()) {
            year++;
        }

        /* go to beginning of month after and then subtract one day to get to end of intended month */
        month++;
        if (month > 12) {
            month = 1;
            year++;
        }
        ISO62106::RdsTime newTime;
        newTime.setDate(year, month, 1);
        newTime.subTime(1, 0, 0);

        /* as we know the days in this month, we can also go back a half month now */
        if (midOfMonth) {
            newTime.setDate(newTime.year(), newTime.month(), newTime.day() / 2);
        }
        time = newTime;
    }

    /* convert (back) to UTC */
    time.toUtcTime();

    return time;
}

void Message::decrementDurationPersistence()
{
    m_durationPersistence--;
    setDecrementTime();
}

void Message::processOptionalMessageContents()
{
    for (OptionalMessageContent & omc : m_optionalMessageContent) {
        uint8_t label = omc.label();
        uint16_t data = omc.data();

        switch (label) {
        case 0:
            /* Duration */
            /* may be used only once per message */
            assert(data <= 0x7); // 3 bit
            if (data == 0) {
                /* code 000 is not allowed for optional content */
                // Do nothing. This is used as trailing zeros.
            } else {
                setDurationPersistence(data & 0x7);
            }
            break;
        case 1:
            /* Control code */
            assert(data <= 0x7); // 3 bit
            switch (data) {
            case 0:
                /* Default urgency increased by one level */
                eventListEntry.increaseUrgency();
                break;
            case 1:
                /* Default urgency reduced by one level */
                eventListEntry.decreaseUrgency();
                break;
            case 2:
                /* Default directionality of message changed */
                eventListEntry.switchDirectionality();
                break;
            case 3:
                /* Default dynamic or longer-lasting provision interchanged */
                eventListEntry.switchDurationType();
                break;
            case 4:
                /* Default spoken or unspoken duration interchanged */
                eventListEntry.switchSpoken();
                break;
            case 5:
                /* Equivalent of diversion bit set to "1" */
                m_diversionAdvice = true;
                break;
            case 6:
                /* Increase the number of steps in the problem extent by eight */
                m_extent += 8;
                break;
            case 7:
                /* Increase the number of steps in the problem extent by sixteen */
                m_extent += 16;
                break;
            }
            break;
        case 2:
            /* Length of route affected */
            assert(data <= 0x1f); // 5 bit
            // Do nothing. This is interpreted at terminal.
            break;
        case 3:
            /* Speed limit advice */
            assert(data <= 0x1f); // 5 bit
            // Do nothing. This is interpreted at terminal.
            break;
        case 4:
            /* Quantifier */
            assert(data <= 0x1f); // 5 bit
            // Do nothing. This is interpreted at terminal.
            break;
        case 5:
            /* Quantifier */
            assert(data <= 0xff); // 8 bit
            // Do nothing. This is interpreted at terminal.
            break;
        case 6:
            /* Supplementary information code */
            assert(data <= 0xff); // 8 bit
            // Do nothing. This is interpreted at terminal.
            break;
        case 7:
            /* Explicit start time */
            /* may be used only once per message */
            assert(data <= 0xff); // 8 bit
            startTime = startStopTime(receiveTime, static_cast<uint8_t>(data));
            break;
        case 8:
            /* Explicit stop time */
            /* may be used only once per message */
            assert(data <= 0xff); // 8 bit
            stopTime = startStopTime(receiveTime, static_cast<uint8_t>(data));
            break;
        case 9:
            /* Additional event */
            assert(data <= 0x7ff); // 11 bit
            // Do nothing. This is interpreted at terminal.
            break;
        case 10:
            /* Detailed diversion instructions */
            assert(data <= 0xffff); // 16 bit
            // Do nothing. This is interpreted at terminal.
            break;
        case 11:
            /* Destination */
            assert(data <= 0xffff); // 16 bit
            // Do nothing. This is interpreted at terminal.
            break;
        case 12:
            /* Precise location reference */
            /* may be used only once per message */
            assert(data <= 0xffff); // 16 bit
            // Do nothing. This is interpreted at terminal.
            break;
        case 13:
            /* Cross linkage to source of problem, on another route */
            assert(data <= 0xffff); // 16 bit
            // Do nothing. This is interpreted at terminal.
            break;
        case 14:
            /* Separator */
            // assert(data <= 0); // 0 bit
            // Do nothing. This is interpreted at terminal.
            break;
        case 15:
            /* Other information as defined by sub-labels */
            assert(data <= 0x3f); // 6 bit
            // Do nothing. The data is directly processed by pushFreeFormat and stored somewhere else.
            // break; // no need to break here, it's the last line anyway
        }
    }
}

bool Message::sameService(const Message & refMessage) const
{
    /* The specification requires to check if
     * messages come from the same RDS-TMC service as defined by
     * AFI = 1 or when the message comes from a non-conflicting service
     * referenced with the tuning variants 6, 7, 8 or 9.
     *
     * However the MessageMemory implementation doesn't differentiate between
     * receiving RDS program, AFI or non-conflicting service. It's just
     * checks on insertMessage if the ECC, CC, LTN and SID are the same.
     * Hence the requirement is fullfilled already at message insertion. */

    return ((locationTableExtendedCountryCode() == refMessage.locationTableExtendedCountryCode()) &&
            (locationTableCountryCode() == refMessage.locationTableCountryCode()) &&
            (locationTableNumber() == refMessage.locationTableNumber()) &&
            (systemInformation.serviceId() == refMessage.systemInformation.serviceId()));
}

bool Message::sameUpdateClasses(const Message & refMessage) const
{
    std::set<uint16_t> updateClasses1 = updateClasses();
    std::set<uint16_t> updateClasses2 = refMessage.updateClasses();
    std::set<uint16_t> updateClassesIntersection;
    std::set_intersection(
        updateClasses1.begin(), updateClasses1.end(),
        updateClasses2.begin(), updateClasses2.end(),
        std::inserter(updateClassesIntersection, updateClassesIntersection.begin()));
    return !updateClassesIntersection.empty();
}

void Message::setPersistenceTime()
{
    /* Check if any event has a dynamic duration type.
     * A multi-event message without a duration code or a detailed stop-time shall have a persistence period of 15 minutes
     * if at least one of the events is defined as dynamic, otherwise it shall be 1 hour.
     */
    bool dynamicEvent = (durationTypes().count('D') > 0);

    /* set persistence time */
    if (dynamicEvent) {
        /* D: dynamic event */
        switch (m_durationPersistence) {
        case 0:
        case 1:
            /* 15 minutes (no message to end-user) */
            persistenceTime = receiveTime;
            persistenceTime.addTime(0, 0, 15);
            eventListEntry.setSpoken(false);
            break;
        case 2:
            /* 30 minutes (with message to end-user) */
            persistenceTime = receiveTime;
            persistenceTime.addTime(0, 0, 30);
            eventListEntry.setSpoken(true);
            break;
        case 3:
            /* 1 hour */
            persistenceTime = receiveTime;
            persistenceTime.addTime(0, 1, 0);
            break;
        case 4:
            /* 2 hours */
            persistenceTime = receiveTime;
            persistenceTime.addTime(0, 2, 0);
            break;
        case 5:
            /* 3 hours */
            persistenceTime = receiveTime;
            persistenceTime.addTime(0, 3, 0);
            break;
        case 6:
            /* 4 hours */
            persistenceTime = receiveTime;
            persistenceTime.addTime(0, 4, 0);
            break;
        case 7:
            /* until midnight on the day of message receipt */
            persistenceTime = receiveTime;
            persistenceTime.toLocalTime();
            persistenceTime.setHour(0);
            persistenceTime.setMinute(0);
            persistenceTime.addTime(1, 0, 0); // +1 day
            persistenceTime.toUtcTime();
            break;
        }
    } else {
        /* L: longer-lasting event */
        switch (m_durationPersistence) {
        case 0:
            /* 1 hour */
            persistenceTime = receiveTime;
            persistenceTime.addTime(0, 1, 0);
            break;
        case 1:
            /* 2 hours */
            persistenceTime = receiveTime;
            persistenceTime.addTime(0, 2, 0);
            break;
        case 2:
            /* until midnight on the day of message receipt */
            persistenceTime = receiveTime;
            persistenceTime.toLocalTime();
            persistenceTime.setHour(0);
            persistenceTime.setMinute(0);
            persistenceTime.addTime(1, 0, 0); // +1 day
            persistenceTime.toUtcTime();
            break;
        case 3:
        case 4:
        case 5:
        case 6:
        case 7:
            /* until midnight on the day after message receipt */
            persistenceTime = receiveTime;
            persistenceTime.toLocalTime();
            persistenceTime.setHour(0);
            persistenceTime.setMinute(0);
            persistenceTime.addTime(2, 0, 0); // +2 days
            persistenceTime.toUtcTime();
            break;
        }
    }
}

void Message::setDecrementTime()
{
    switch (eventListEntry.durationType()) {
    case 'D':
        /* D: dynamic event */
        switch (m_durationPersistence) {
        case 0:
        case 1:
            /* do not decrement */
            decrementTime = ISO62106::RdsTime();
            break;
        case 2:
            /* decrement after 15 minutes */
            decrementTime.addTime(0, 0, 15);
            break;
        case 3:
            /* decrement after 30 minutes */
            decrementTime.addTime(0, 0, 30);
            break;
        case 4:
        case 5:
        case 6:
            /* decrement after 1 hour */
            decrementTime.addTime(0, 1, 0);
            break;
        case 7:
            /* do not decrement */
            decrementTime = ISO62106::RdsTime();
            break;
        }
        break;
    case 'L':
        /* L: longer-lasting event */
        switch (eventListEntry.nature()) {
        case 'I':
            /* I: Information */
            switch (m_durationPersistence) {
            case 0:
            case 1:
            case 2:
                /* do not decrement */
                decrementTime = ISO62106::RdsTime();
                break;
            case 3: {
                /* decrement at midnight */
                ISO62106::RdsTime time = decrementTime;
                time.toLocalTime();
                time.setHour(0);
                time.setMinute(0);
                time.addTime(1, 0, 0); // +1 days
                time.toUtcTime();
                decrementTime = time;
            }
            break;
            case 4: {
                /* decrement Friday midnight */
                ISO62106::RdsTime time = decrementTime;
                time.toLocalTime();
                time.setHour(0);
                time.setMinute(0);
                uint8_t wd = time.weekDay(); // 1 = Monday, 7 = Sunday
                wd = (7 + 5 - wd) % 7; // 5 = Friday
                time.addTime(1 + wd, 0, 0); // +1+wd days
                time.toUtcTime();
                decrementTime = time;
            }
            break;
            case 5: {
                /* decrement Sunday midnight */
                ISO62106::RdsTime time = decrementTime;
                time.toLocalTime();
                time.setHour(0);
                time.setMinute(0);
                uint8_t wd = time.weekDay(); // 1 = Monday, 7 = Sunday
                wd = (7 + 7 - wd) % 7; // 7 = Sunday
                time.addTime(1 + wd, 0, 0); // +1+wd days
                time.toUtcTime();
                decrementTime = time;
            }
            break;
            case 6:
            case 7:
                /* do not decrement */
                decrementTime = ISO62106::RdsTime();
                break;
            }
            break;
        case 'F':
            /* F: Forecast */
            switch (m_durationPersistence) {
            case 0:
            case 1:
            case 2:
                /* do not decrement */
                decrementTime = ISO62106::RdsTime();
                break;
            case 3:
            case 4: {
                /* decrement at midnight */
                ISO62106::RdsTime time = decrementTime;
                time.toLocalTime();
                time.setHour(0);
                time.setMinute(0);
                time.addTime(1, 0, 0); // +1 days
                time.toUtcTime();
                decrementTime = time;
            }
            break;
            case 5:
            case 6:
            case 7:
                /* do not decrement */
                decrementTime = ISO62106::RdsTime();
                break;
            }
            break;
        case 'S':
            /* S: Silent */
            /* do not decrement */
            decrementTime = ISO62106::RdsTime();
            break;
        }
        break;
    }
}

}
