/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include <cstdint>
#include <set>
#include <vector>

#include <ISO/62106.h>

#include "BitFifo.h"
#include "Event.h"
#include "EventList.h"
#include "OptionalMessageContent.h"
#include "SystemInformation.h"
#include "TelephoneNumber.h"

#include "iso_14819_export.h"
#include "platform.h"

namespace ISO14819 {

/**
 * @brief Message
 *
 * This class contains all information of a TMC message.
 */
class ISO_14819_EXPORT Message
{
public:
    explicit Message();

    /**
     * Get Application ID
     *
     * @return Application ID
     */
    uint16_t applicationId() const;

    /**
     * Set Application ID
     *
     * @param[in] value Application ID
     */
    void setApplicationId(uint16_t value);

    /**
     * Tests if message was sent with Application ID 0x0d45
     *
     * @return true if AID was 0x0d45
     */
    bool testMessage() const;

    /**
     * Set Number of messages expected in group.
     *
     * @param[in] value Number of messages expected in group.
     */
    void setGroupsExpected(uint8_t value);

    /**
     * Increase number of message received in group.
     */
    void incGroupsReceived();

    /**
     * Returns whether the message was received completely.
     *
     * @return true if message is complete, false otherwise
     */
    bool complete() const;

    /**
     * Get event.
     *
     * @return event
     */
    uint16_t event() const;

    /**
     * Set event and copy the Event List Entry.
     * It automatically fills in implicit information from the Event List.
     *
     * @param[in] value Event Code
     */
    void setEvent(uint16_t value);

    /**
     * Get the Location Table ExtendedCountry Code (LTECC)
     *
     * This is selected in the following order:
     *   - TMC System Information's ECC
     *   - RDS Receivers's ECC
     *
     * @return Location Table ExtendedCountry Code (LTECC)
     */
    uint8_t locationTableExtendedCountryCode() const;

    /**
     * Get the Location Table Country Code (LTCC)
     *
     * This is selected in the following order:
     *   - INTER-ROAD Message LTCC
     *   - TMC System Information's CC
     *   - RDS Receivers's CC
     *
     * @return Location Table Country Code (LTCC)
     */
    uint8_t locationTableCountryCode() const;

    /**
     * Get the Location Table Number (LTN) AKA Table Code (TABCD)
     *
     * This is selected in the following order:
     *   - INTER-ROAD Message's LTN
     *   - TMC System Information's LTN
     *
     * @return Location Table Number (LTN)
     */
    uint8_t locationTableNumber() const;

    /**
     * Get primary location.
     *
     * @return primary location
     */
    uint16_t location() const;

    /**
     * Set primary location.
     *
     * @param[in] value primary location
     */
    void setLocation(uint16_t value);

    /**
     * Set INTER-ROAD primary location.
     *
     * @param[in] irltcc INTER-ROAD location table country code
     * @param[in] irltn INTER-ROAD location table number
     * @param[in] irl INTER-ROAD primary location
     */
    void setInterRoadLocation(uint8_t irltcc, uint8_t irltn, uint16_t irl);

    /**
     * get direction
     *
     * @return direction
     */
    uint8_t direction() const;

    /**
     * set direction
     *
     * @param[in] value direction
     */
    void setDirection(uint8_t value);

    /**
     * get extent
     *
     * @return extent
     */
    uint8_t extent() const;

    /**
     * set extent
     *
     * @param[in] value extent
     */
    void setExtent(uint8_t value);

    /**
     * get duration/persistence
     *
     * @return duration/persistence
     */
    uint8_t durationPersistence() const;

    /**
     * set duration/persistence
     *
     * @param[in] value duration/persistence
     */
    void setDurationPersistence(uint8_t value);

    /**
     * get diversion advice
     *
     * @return diversion advice
     */
    bool diversionAdvice() const;

    /**
     * set diversion advice
     *
     * @param[in] value diversion advice
     */
    void setDiversionAdvice(bool value);

    /**
     * Get Optional Message Content
     *
     * @return Optional Message Content
     */
    std::vector<OptionalMessageContent> optionalMessageContent() const;

    /**
     * Add Optional Message Content
     *
     * @param[in] label Label
     * @param[in] data data
     */
    void addOptionalMessageContent(uint8_t label, uint16_t data);

    /** Telephone number to receive telephone based information from an IVR service */
    TelephoneNumber telNumInformation;

    /** Telephone number to report information to a service provider */
    TelephoneNumber telNumReport;

    /** receiver's system information */
    SystemInformation systemInformation;

    /** event list's entry */
    Event eventListEntry;

    /**
     * get update classes (a multi-group message may have more than one event and update class)
     *
     * @return update classes
     */
    std::set<uint16_t> updateClasses() const;

    /**
     * get duration types (a multi-group message may have more than one event and duration type)
     *
     * @return duration types
     */
    std::set<char> durationTypes() const;

    /* Times */

    /** receive time */
    ISO62106::RdsTime receiveTime;

    /**
     * @brief persistence time
     *
     * Persistence time after which the message is deleted, if no refresh has been received.
     */
    ISO62106::RdsTime persistenceTime;

    /**
     * @brief decrement time
     *
     * The next time the durationPersistence variable is decreased.
     */
    ISO62106::RdsTime decrementTime;

    /**
     * @brief start time
     *
     * This is set on reception of optional message content label 7.
     */
    ISO62106::RdsTime startTime;

    /**
     * @brief stop time
     *
     * This is set on reception of optional message content label 8.
     */
    ISO62106::RdsTime stopTime;

    /**
     * Push Free Format bits into FIFO and process the Optional Message Content therein.
     *
     * Some calculations on Free Format and Optional Message Content:
     *   - maximum free format bit size is 4 (MS) * 28 (FF) = 112
     *   - minimum bit size of opt. msg is 4 (label + no data)
     *   - maximum number of opt. msgs is 112 / 4 = 28
     *
     * @param[in] data Data bits
     * @param[in] bits Length of data
     */
    void pushFreeFormat(uint32_t data, uint8_t bits);

    /**
     * @brief Return start/stop time based on free format data
     *
     * Returns the start/stop time based on free format data.
     *
     * @param[in] time Timebase for calculation
     * @param[in] data Free format data for start/stop time
     * @return RDS time
     */
    static ISO62106::RdsTime startStopTime(ISO62106::RdsTime time, uint8_t data);

    /**
     * Decrements the durationPersistence variable and sets a new decrement time.
     */
    void decrementDurationPersistence();

    /**
     * Process Optional Message Contents.
     *
     * This processes all Optional Message Contents before giving it to Message Memory.
     */
    void processOptionalMessageContents();

    /**
     * Check if the referenced message come from the same TMC service as the
     * local message.
     *
     * @param[in] refMessage Referenced message
     * @return true if same service, false otherwise
     */
    bool sameService(const Message & refMessage) const;

    /**
     * Check if the referenced message contains an event that belongs to the
     * same update class as any event (a multi-group message may have more than
     * one event) in local message.
     *
     * @param[in] refMessage Referenced message
     * @return true if same service, false otherwise
     */
    bool sameUpdateClasses(const Message & refMessage) const;

private:
    /**
     * Application ID under which this message was received.
     */
    uint16_t m_applicationId;

    /**
     * Number of messages expected in group.
     *
     * For a multi-messages this remains 0, until it gets set with the second group received.
     */
    uint8_t m_groupsExpected : 3; // 0..5

    /** number of message received in group */
    uint8_t m_groupsReceived : 3; // 0..5

    /** event description */
    uint16_t m_event : 11;

    /** INTER-ROAD location table country code (LTCC) */
    uint8_t m_interRoadLocationTableCountryCode : 4;

    /** INTER-ROAD location table number (LTN) */
    uint8_t m_interRoadLocationTableNumber : 6;

    /** primary location */
    uint16_t m_location : 16;

    /** direction (0=positive, 1=negative) */
    uint8_t m_direction : 1;

    /** extent (0..31) */
    uint8_t m_extent : 5;

    /** duration / persistence */
    uint8_t m_durationPersistence : 3;

    /** diversion advice */
    bool m_diversionAdvice : 1;

    /** remaining bits */
    BitFifo m_freeFormatFifo;

    /** label of next data */
    uint8_t m_omcNextLabel : 4;

    /** sublabel of next data */
    uint8_t m_omcNextSubLabel : 6;

    /** free format state machine state */
    enum FreeFormatState {
        /** label to be parsed */
        Label,

        /** sublabel to be parsed */
        SubLabel,

        /** data to be parsed */
        Data,

        /** sub data to be parsed */
        SubData,
    };

    /** free format state machine state */
    FreeFormatState m_freeFormatState;

    /** Optional Message Content */
    std::vector<OptionalMessageContent> m_optionalMessageContent;

    /** set persistence time */
    void setPersistenceTime();

    /** set decrement time */
    void setDecrementTime();
};

}
