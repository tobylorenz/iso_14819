/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include <cstdint>

#include "PreciseLocationReference.h"

#include "iso_14819_export.h"
#include "platform.h"

namespace ISO14819 {

/** Optional Message Content */
class ISO_14819_EXPORT OptionalMessageContent
{
public:
    explicit OptionalMessageContent();

    /**
     * get label
     *
     * @return label
     */
    uint8_t label() const;

    /**
     * set label
     *
     * @param[in] label label
     */
    void setLabel(uint8_t label);

    /**
     * get data (or sublabel if data==15)
     *
     * @return data (or sublabel if data==15)
     */
    uint16_t data() const;

    /**
     * set data (or sublabel if data==15)
     *
     * @param[in] data data (or sublabel if data==15)
     */
    void setData(uint16_t data);

    /**
     * returns the (expected) size of the data in bits
     *
     * @param[in] label label
     * @return size in bits
     */
    static uint8_t size(uint8_t label);

    /**
     * for label 2: Length of route affected
     *
     * @note Event codes can also be used for length (e.g. code 102 to indicate stationary traffic for 1 km).
     *
     * @return Length of route affected
     */
    uint8_t lengthOfRouteAffected() const;

    /**
     * for label 3: Speed limit
     *
     * @return Speed limit
     */
    uint8_t speedLimit() const;

    /**
     * for label 12: Precise location reference
     *
     * @return Precise location reference
     */
    PreciseLocationReference preciseLocationReference() const;

private:
    /** label */
    uint8_t m_label : 4;

    /** data (or sublabel if data==15) */
    uint16_t m_data : 16;
};

}
