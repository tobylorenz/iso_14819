/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include <cstdint>
#include <string>
#include <vector>

#include <sqlite3.h>

#include "iso_14819_export.h"
#include "platform.h"

namespace ISO14819 {

/**
 * @brief Location Code List
 *
 * The Location Code List (LCL) contains tables to decode the position of an event.
 */
class ISO_14819_EXPORT LocationCodeList
{
public:
    /**
     * @brief Open TMC Location Code List database
     *
     * This function loads the Location Code List.
     *
     * @param[in] countryId Country ID (CID)
     * @param[in] tableCode Table code
     */
    explicit LocationCodeList(uint16_t countryId = 0, uint8_t tableCode = 0);

    // @todo README file to have Meta Information?

    /**
     * 1 Country (from COUNTRIES)
     *
     * This table contains the country codes used in the dataset. Usually there is only one country
     * code for each dataset.
     */
    struct Country {
        Country();

        /** Country ID (CID) */
        uint16_t countryId;

        /** Extended country code (ECC) */
        uint8_t extendedCountryCode : 8;

        /** Country code (CCD) */
        uint8_t countryCode : 4;

        /** Name (CNAME) */
        std::string name;
    };

    /**
     * Get Countries
     *
     * @return Countries
     */
    std::vector<Country> countries() const;

    /**
     * 2 Location Data Set (from LOCATIONDATASETS)
     *
     * This table describes the table number and the version of the dataset.
     */
    struct LocationDataSet {
        LocationDataSet();

        /** Country ID (CID) */
        uint16_t countryId;

        /** Table code (TABCD) */
        uint8_t tableCode : 6;

        /** Comment (DCOMMENT) */
        std::string comment;

        /** Version (VERSION) */
        std::string version;

        /** Version Description (VERSIONDESCRIPTION) */
        std::string versionDescription;
    };

    /**
     * Get Location Data Sets
     *
     * @return Location Data Sets
     */
    std::vector<LocationDataSet> locationDataSets() const;

    /**
     * 3 Location Code (from LOCATIONCODES)
     *
     * Contains all allowed location codes and marks those with "1" which used in the dataset.
     */
    struct LocationCode {
        LocationCode();

        /** Counry ID (CID) */
        uint16_t countryId;

        /** Table code (TABCD) */
        uint8_t tableCode : 6;

        /** Location code (LCD) */
        uint16_t locationCode : 16;

        /** Allocated (ALLOCATED) */
        bool allocated : 1;
    };

    /**
     * Get Location Code
     *
     * @param[in] countryId Country ID (CID)
     * @param[in] tableCode Table code (TABCD)
     * @param[in] locationCode Location code (LCD)
     * @return Location Code
     */
    LocationCode locationCode(uint16_t countryId, uint8_t tableCode, uint16_t locationCode) const;

    /**
     * 4 Class (from CLASSES)
     *
     * This table defines the categories (A: Area location, L: line location, P: point location) used in
     * the dataset.
     */
    struct Class {
        Class();

        /** Type class (CLASS) */
        char typeClass;
    };

    /**
     * Get Classes
     *
     * @return Classes
     */
    std::vector<Class> classes() const;

    /**
     * 5 Types (from TYPES)
     */
    struct Type {
        Type();

        /** Type class (CLASS) */
        char typeClass;

        /** Type code (TCD) */
        uint16_t typeCode;

        /** Type description (TDESC) */
        std::string typeDescription;

        /** National type code (TNATCD) */
        std::string nationalTypeCode;

        /** National type description (TNATDESC) */
        std::string nationalTypeDescription;
    };

    /**
     * Get Type
     *
     * @param[in] typeClass Type class (CLASS)
     * @param[in] typeCode Type code (TCD)
     * @return Type
     */
    Type type(char typeClass, uint16_t typeCode) const;

    /**
     * 6 Subtype (from SUBTYPES)
     *
     * This table defines the subtypes used in this dataset.
     */
    struct Subtype {
        Subtype();

        /** Type Class (CLASS) */
        char typeClass;

        /** Type Code (TCD) */
        uint16_t typeCode;

        /** Subtype Code (STCD) */
        uint16_t subtypeCode;

        /** Subtype description (SDESC) */
        std::string subtypeDescription;

        /** National subtype code (SNATCODE) */
        std::string nationalSubtypeCode;

        /** National subtype description (SNATDESC) */
        std::string nationalSubtypeDescription;
    };

    /**
     * Get Subtype
     *
     * @param[in] typeClass Type class (CLASS)
     * @param[in] typeCode Type code (TCD)
     * @param[in] subtypeCode Subtype code (STCD)
     * @return Subtype
     */
    Subtype subtype(char typeClass, uint16_t typeCode, uint16_t subtypeCode) const;

    /**
     * 7 Language (from LANGUAGES)
     *
     * This table describes the languages used e.g. for location name. There is one entry for each
     * language used in the dataset.
     */
    struct Language {
        Language();

        /** Country ID (CID) */
        uint16_t countryId;

        /** Language ID (LID) */
        uint8_t languageId;

        /** Language (LANGUAGE) */
        std::string language;
    };

    /**
     * Get Languages
     *
     * @return Languages
     */
    std::vector<Language> languages() const;

    /**
     * 8 European Road Number (from EUROROADNO)
     *
     * This table contains all European road numbers used in the dataset.
     */
    struct EuroRoadNo {
        EuroRoadNo();

        /** European road number (ENO) */
        std::string europeanRoadNumber;

        /** Comment (ECOMMENT) */
        std::string comment;
    };

    /**
     * Get European Road Number
     *
     * @param[in] europeanRoadNumber European road number (ENO)
     * @return European Road Number
     */
    EuroRoadNo euroRoadNo(std::string europeanRoadNumber) const;

    /**
     * 9 Name (from NAMES)
     *
     * This table contains all the string of the dataset e.g. name of the road, road numbers, location
     * names, etc. It is a good practice that each name is unique. The language ID specifies the
     * language used in this table. It will be one of the official languages of the country where the
     * TMC location table is meant for.
     */
    struct Name {
        Name();

        /** Country ID (CID) */
        uint16_t countryId;

        /** Language ID (LID) */
        uint8_t languageId;

        /** Name ID (NID) */
        uint16_t nameId;

        /** Name (NAME) */
        std::string name;

        /** Comment (NCOMMENT) */
        std::string comment;
    };

    /**
     * Get Name
     *
     * @param[in] countryId Country ID (CID)
     * @param[in] languageId Language ID (LID)
     * @param[in] nameId Name ID (NID)
     * @return Name
     */
    Name name(uint16_t countryId, uint8_t languageId, uint16_t nameId) const;

    /**
     * 10 Name Translation (from NAMETRANSLATIONS)
     *
     * This table contains the translation of the names table for each languages used in the dataset.
     */
    struct NameTranslation {
        NameTranslation();

        /** Country ID (CID) */
        uint16_t countryId;

        /** Language ID (LID) */
        uint8_t languageId;

        /** Name ID (NID) */
        uint16_t nameId;

        /** Translation (NTRANSLATION) */
        std::string translation;
    };

    /**
     * Get Name Translation
     *
     * @param[in] countryId Country ID (CID)
     * @param[in] nameId Name ID (NID)
     * @return Name Translation
     */
    NameTranslation nameTranslation(uint16_t countryId, uint16_t nameId) const;

    /**
     * 11 Sub Type Translation (from SUBTYPETRANSLATIONS)
     *
     * This table contains the translations of the subtypes for each language used in the dataset.
     */
    struct SubTypeTranslation {
        SubTypeTranslation();

        /** Country ID (CID) */
        uint16_t countryId;

        /** Language ID (LID) */
        uint8_t languageId;

        /** Type class (CLASS) */
        char typeClass;

        /** Type code (TCD) */
        uint16_t typeCode;

        /** Subtype code (STCD) */
        uint16_t subtypeCode;

        /** Translation (STRANSLATION) */
        std::string translation;
    };

    /**
     * Get Sub Type Translation
     *
     * @param[in] countryId Country ID (CID)
     * @param[in] languageId Language ID (LID)
     * @param[in] typeClass Type class (CLASS)
     * @param[in] typeCode Type code (TCD)
     * @param[in] subTypeCode Subtype code (STCD)
     * @return Sub Type Translation
     */
    SubTypeTranslation subTypeTranslation(uint16_t countryId, uint8_t languageId, char typeClass, uint16_t typeCode, uint16_t subTypeCode) const;

    /**
     * 12 European Road Number Belongs To Country (from ERNO_BELONGS_TO_CO)
     *
     * This table contains all European road number which belongs to the country.
     */
    struct EuroRoadNoBelongsToCountry {
        EuroRoadNoBelongsToCountry();

        /** Country ID (CID) */
        uint8_t countryId;

        /** European road number (ENO) */
        std::string europeanRoadNumber;
    };

    /**
     * Get European Road Number Belongs To Country
     *
     * @param[in] europeanRoadNumber European road number (ENO)
     * @return European Road Number Belongs To Country
     */
    std::vector<EuroRoadNoBelongsToCountry> euroRoadNoBelongsToCountries(std::string europeanRoadNumber) const;

    /**
     * 13 Administrative Area (from ADMINISTRATIVEAREA)
     *
     * This table contains all administrative areas of the dataset.
     */
    struct AdministrativeArea {
        AdministrativeArea();

        /** Country ID (CID) */
        uint16_t countryId;

        /** Table code (TABCD) */
        uint8_t tableCode : 6;

        /** Location code (LCD) */
        uint16_t locationCode : 16;

        /** Type class (CLASS) */
        char typeClass;

        /** Type code (TCD) */
        uint8_t typeCode;

        /** Subtype code (STCD) */
        uint8_t subtypeCode;

        /** Name (NID) */
        uint16_t name;

        /** Upward area reference (POL_LCD) */
        uint16_t upwardAreaReference : 16;
    };

    /**
     * Get Administrative Area
     *
     * @param[in] countryId Country ID (CID)
     * @param[in] tableCode Table code (TABCD)
     * @param[in] locationCode Location code (LCD)
     * @return Administrative Area
     */
    AdministrativeArea administrativeArea(uint16_t countryId, uint8_t tableCode, uint16_t locationCode) const;

    /**
     * 14 Other Area (from OTHERAREAS)
     *
     * This table contains the other areas of the dataset.
     */
    struct OtherArea {
        OtherArea();

        /** Country ID (CID) */
        uint16_t countryId;

        /** Table code (TABCD) */
        uint8_t tableCode : 6;

        /** Location code (LCD) */
        uint16_t locationCode : 16;

        /** Type class (CLASS) */
        char typeClass;

        /** Type code (TCD) */
        uint8_t typeCode;

        /** Subtype code (STCD) */
        uint8_t subtypeCode;

        /** Name1 (NID) */
        uint16_t name1;

        /** Admin area reference (POL_LCD) */
        uint16_t adminAreaReference : 16;
    };

    /**
     * Get Other Area
     *
     * @param[in] countryId Country ID (CID)
     * @param[in] tableCode Table code (TABCD)
     * @param[in] locationCode Location code (LCD)
     * @return Other Area
     */
    OtherArea otherArea(uint16_t countryId, uint8_t tableCode, uint16_t locationCode) const;

    /**
     * 15 Road (from ROADS)
     *
     * This table contains the road description of the dataset.
     */
    struct Road {
        Road();

        /** Country ID (CID) */
        uint16_t countryId;

        /** Table code (TABCD) */
        uint8_t tableCode : 6;

        /** Location code (LCD) */
        uint16_t locationCode : 16;

        /** Type class (CLASS) */
        char typeClass;

        /** Type code (TCD) */
        uint16_t typeCode;

        /** Subtype code (STCD) */
        uint16_t subtypeCode;

        /** Road number (ROADNUMBER) */
        std::string roadNumber;

        /** Road name (RNID) */
        uint16_t roadName;

        /** Name1 (N1ID) */
        uint16_t name1;

        /** Name2 (N2ID) */
        uint16_t name2;

        /** Admin area reference (POL_LCD) */
        uint16_t adminAreaReference : 16;

        /** Road network Level (PES_LEV) */
        uint8_t roadNetworkLevel : 4;
    };

    /**
     * Get Road
     *
     * @param[in] countryId Country ID (CID)
     * @param[in] tableCode Table code (TABCD)
     * @param[in] locationCode Location code (LCD)
     * @return Road
     */
    Road road(uint16_t countryId, uint8_t tableCode, uint16_t locationCode) const;

    /**
     * 16 Road Network Level Type (from ROAD_NETWORK_LEVEL_TYPES)
     */
    struct RoadNetworkLevelType {
        RoadNetworkLevelType();

        /** Road network level (PES_LEV) */
        uint8_t roadNetworkLevel : 4;

        /** Road network level description (PES_LEV_DESC) */
        std::string roadNetworkLevelDescription;

        /** National road network level description (TDESC) */
        std::string nationalRoadNetworkLevelDescription;
    };

    /**
     * Get Road Network Level Type
     *
     * @param[in] roadNetworkLevel Road network level (PES_LEV)
     * @return Road Network Level Type
     */
    RoadNetworkLevelType roadNetworkLevelType(uint8_t roadNetworkLevel) const;

    /**
     * 17 Segment (from SEGMENTS)
     *
     * This table defines the 1st and 2nd order segments of the dataset.
     */
    struct Segment {
        Segment();

        /** Country ID (CID) */
        uint16_t countryId;

        /** Table code (TABCD) */
        uint8_t tableCode : 6;

        /** Location code (LCD) */
        uint16_t locationCode : 16;

        /** Type class (CLASS) */
        char typeClass;

        /** Type code (TCD) */
        uint16_t typeCode;

        /** Subtype code (STCD) */
        uint16_t subtypeCode;

        /** Road number (ROADNUMBER) */
        std::string roadNumber;

        /** Road name (RNID) */
        uint16_t roadName;

        /** Name1 (N1ID) */
        uint16_t name1;

        /** Name2 (N2ID) */
        uint16_t name2;

        /** Road reference (ROA_LCD) */
        uint16_t roadReference : 16;

        /** Order1 segment reference (SEG_LCD) */
        uint16_t segmentReference : 16;

        /** Admin are reference (POL_LCD) */
        uint16_t adminAreaReference : 16;
    };

    /**
     * Get Segment
     *
     * @param[in] countryId Country ID (CID)
     * @param[in] tableCode Table code (TABCD)
     * @param[in] locationCode Location code (LCD)
     * @return Segment
     */
    Segment segment(uint16_t countryId, uint8_t tableCode, uint16_t locationCode) const;

    /**
     * 18 Segment Offset (from SOFFSETS)
     *
     * This table describes the positive and negative offsets for the segments.
     */
    struct SOffset {
        SOffset();

        /** Country ID (CID) */
        uint16_t countryId;

        /** Table code (TABCD) */
        uint8_t tableCode : 6;

        /** Location code (LCD) */
        uint16_t locationCode : 16;

        /** Negative offset (NEG_OFF_LCD) */
        uint16_t negativeOffset : 16;

        /** Positive offset (POS_OFF_LCD) */
        uint16_t positiveOffset : 16;
    };

    /**
     * Get Segment Offset
     *
     * @param[in] countryId Country ID (CID)
     * @param[in] tableCode Table code (TABCD)
     * @param[in] locationCode Location code (LCD)
     * @return Segment Offset
     */
    SOffset sOffset(uint16_t countryId, uint8_t tableCode, uint16_t locationCode) const;

    /**
     * 19 Segment Has European Road Number (from SEG_HAS_ERNO)
     *
     * This table relates the segments and the European road numbers.
     */
    struct SegHasEuroRoadNo {
        SegHasEuroRoadNo();

        /** Country ID (CID) */
        uint16_t countryId;

        /** Table code (TABCD) */
        uint8_t tableCode : 6;

        /** Location code (LCD) */
        uint16_t locationCode : 16;

        /** European road number (ENO) */
        std::string europeanRoadNumber;
    };

    /**
     * Get Segment Has European Road Number
     *
     * @param[in] countryId Country ID (CID)
     * @param[in] tableCode Table code (TABCD)
     * @param[in] locationCode Location code (LCD)
     * @return Segment Has European Road Number
     */
    SegHasEuroRoadNo segHasEuroRoadNo(uint16_t countryId, uint8_t tableCode, uint16_t locationCode) const;

    /**
     * 20 Point (from POINTS)
     *
     * This table contains all the point location of the dataset. The co-ordinates are in WGS84.
     */
    struct Point {
        Point();

        /** Country ID (CID) */
        uint16_t countryId;

        /** Table code (TABCD) */
        uint8_t tableCode : 6;

        /** Location code (LCD) */
        uint16_t locationCode : 16;

        /** Type class (CLASS) */
        char typeClass;

        /** Type code (TCD) */
        uint16_t typeCode;

        /** Subtype code (STCD) */
        uint16_t subtypeCode;

        /** Junction number (JUNCTIONNUMBER) */
        std::string junctionNumber;

        /** Road name (RNID) */
        uint16_t roadName;

        /** Name1 (N1ID) */
        uint16_t name1;

        /** Name2 (N2ID) */
        uint16_t name2;

        /** Admin area reference (POL_LCD) */
        uint16_t adminAreaReference : 16;

        /** Other area reference (OTH_LCD) */
        uint16_t otherAreaReference : 16;

        /** Segment reference (SEG_LCD) */
        uint16_t segmentReference : 16;

        /** Road reference (ROA_LCD) */
        uint16_t roadReference : 16;

        /** InPos (INPOS) */
        bool inPos : 1;

        /** InNeg (INNEG) */
        bool inNeg : 1;

        /** OutPos (OUTPOS) */
        bool outPos : 1;

        /** OutNeg (OUTNEG) */
        bool outNeg : 1;

        /** PresentPos (PRESENTPOS) */
        bool presentPos : 1;

        /** PresentNeg (PRESENTNEG) */
        bool presentNeg : 1;

        /** DiversionPos (DIVERSIONPOS) */
        std::string diversionPos;

        /** DiversionNeg (DIVERSIONNEG) */
        std::string diversionNeg;

        /** Xcoord (Longitude) (XCOORD) */
        std::string xCoord;

        /** Ycoord (Latitude) (YCOORD) */
        std::string yCoord;

        /** InterruptsRoad (INTERRUPTSROAD) */
        uint16_t interruptsRoad : 16;

        /** Urban (URBAN) */
        bool urban : 1;
    };

    /**
     * Get Point
     *
     * @param[in] countryId Country ID (CID)
     * @param[in] tableCode Table code (TABCD)
     * @param[in] locationCode Location code (LCD)
     * @return Point
     */
    Point point(uint16_t countryId, uint8_t tableCode, uint16_t locationCode) const;

    /**
     * 21 Point Offset (from POFFSETS)
     *
     * This table contains the positive and negative offsets for all point locations used in the dataset.
     */
    struct POffset {
        POffset();

        /** Country ID (CID) */
        uint16_t countryId;

        /** Table code (TABCD) */
        uint8_t tableCode : 6;

        /** Location code (LCD) */
        uint16_t locationCode : 16;

        /** Negative offset (NEG_OFF_LCD) */
        uint16_t negativeOffset : 16;

        /** Positive offset (POS_OFF_LCD) */
        uint16_t positiveOffset : 16;
    };

    /**
     * Get Point Offset
     *
     * @param[in] countryId Country ID (CID)
     * @param[in] tableCode Table code (TABCD)
     * @param[in] locationCode Location code (LCD)
     * @return Point Offset
     */
    POffset pOffset(uint16_t countryId, uint8_t tableCode, uint16_t locationCode) const;

    /**
     * 22 Intersection (from INTERSECTIONS)
     *
     * This table contains the relation between two or more locations which describes the same intersection
     * for different segments or roads. If there are more the two the location the first is
     * related to the second, the second to the third,... and the last points again to the first.
     */
    struct Intersection {
        Intersection();

        /** Country ID (CID) */
        uint16_t countryId;

        /** Table code (TABCD) */
        uint8_t tableCode : 6;

        /** Location code (LCD) */
        uint16_t locationCode : 16;

        /** Intersection country ID (INT_CID) */
        uint16_t intersectionCountryId;

        /** Intersection table code (INT_TABCD) */
        uint8_t intersectionTableCode : 6;

        /** Intersection location code (INT_LCD) */
        uint16_t intersectionLocationCode : 16;
    };

    /**
     * Get Intersection
     *
     * @param[in] countryId Country ID (CID)
     * @param[in] tableCode Table code (TABCD)
     * @param[in] locationCode Location code (LCD)
     * @return Intersection
     */
    Intersection intersection(uint16_t countryId, uint8_t tableCode, uint16_t locationCode) const;

    /**
     * Type Union (from TYPESUNION)
     *
     * This view is a union among Types, SubTypes.
     */
    struct TypeUnion {
        TypeUnion();

        /** Type class (CLASS) */
        char typeClass;

        /** Type code (TCD) */
        uint16_t typeCode;

        /** Subtype code (STCD) */
        uint16_t subtypeCode;

        /** Type/Subtype description (DESC) */
        std::string description;

        /** National Type/Subtype code (NATCODE) */
        std::string nationalCode;

        /** National Type/SubType description (NATDESC) */
        std::string nationalDescription;
    };

    /**
     * Get Type Union
     *
     * @param[in] typeClass Type class (CLASS)
     * @param[in] typeCode Type code (TCD)
     * @param[in] subtypeCode Subtype code (STCD)
     * @return Type Union
     */
    TypeUnion typeUnion(char typeClass, uint16_t typeCode, uint16_t subtypeCode) const;

    /**
     * Location Union (from LOCATIONSUNION)
     *
     * This view is a union among AdministrativeArea, OtherAreas, Roads, Segments, Points.
     */
    struct LocationUnion {
        LocationUnion();

        /** Country ID (CID) */
        uint16_t countryId;

        /** Table code (TABCD) */
        uint8_t tableCode : 6;

        /** Location code (LCD) */
        uint16_t locationCode : 16;

        /** Type class (CLASS) */
        char typeClass;

        /** Type code (TCD) */
        uint16_t typeCode;

        /** Subtype code (STCD) */
        uint16_t subtypeCode;

        /** Road/Junction number (NUMBER) */
        std::string number;

        /** Road name (RNID) */
        uint16_t roadName;

        /** Name/Name1 (N1ID) */
        uint16_t name1;

        /** Name2 (N2ID) */
        uint16_t name2;

        /** Admin area reference (POL_LCD) */
        uint16_t adminAreaReference : 16;

        /** Other area reference (OTH_LCD) */
        uint16_t otherAreaReference : 16;

        /** Segment reference (SEG_LCD) */
        uint16_t segmentReference : 16;

        /** Road reference (ROA_LCD) */
        uint16_t roadReference : 16;

        /** Urban (URBAN) */
        bool urban : 1;

        /**
         * Return origin of location:
         *   - A: AdministrativeArea
         *   - O: OtherArea
         *   - R: Road
         *   - S: Segment
         *   - P: Point
         *
         * @return origin of location
         */
        char origin() const;
    };

    /**
     * Get Location Union
     *
     * @param[in] countryId Country ID (CID)
     * @param[in] tableCode Table code (TABCD)
     * @param[in] locationCode Location code (LCD)
     * @return Location Union
     */
    LocationUnion locationUnion(uint16_t countryId, uint8_t tableCode, uint16_t locationCode) const;

    /**
     * Offset Union (from OFFSETSUNION)
     *
     * This view is a union among SOffsets, POffsets.
     */
    struct OffsetUnion {
        OffsetUnion();

        /** Country ID (CID) */
        uint16_t countryId;

        /** Table code (TABCD) */
        uint8_t tableCode : 6;

        /** Location code (LCD) */
        uint16_t locationCode : 16;

        /** Negative offset (NEG_OFF_LCD) */
        uint16_t negativeOffset : 16;

        /** Positive offset (POS_OFF_LCD) */
        uint16_t positiveOffset : 16;
    };

    /**
     * Get Offset Union
     *
     * @param[in] countryId Country ID (CID)
     * @param[in] tableCode Table code (TABCD)
     * @param[in] locationCode Location code (LCD)
     * @return Offset Union
     */
    OffsetUnion offsetUnion(uint16_t countryId, uint8_t tableCode, uint16_t locationCode) const;

private:
    /**
     * @brief Filename to the TMC Location Code List database
     *
     * This variable contains the filename to the Location Code List database.
     */
    std::string m_filename;

    /**
     * @brief Pointer to the TMC Location Code List database
     *
     * This variable contains the pointer to the Location Code List database.
     */
    sqlite3 * m_database;

    /** Country Identification (CID) */
    uint16_t m_countryId;

    /** Table code (TABCD) */
    uint8_t m_tableCode : 6;
};

}
