/*
 * Copyright (C) 2009-2016 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include <cstdint>

#include <ISO/62106.h>

#include "Message.h"
#include "SystemInformation.h"
#include "TuningInformation.h"

#include "iso_14819_export.h"
#include "platform.h"

namespace ISO14819 {

/* forward declarations */
class Message;

/**
 * @brief Traffic Message Channel
 *
 * This class is registered as Open Data Application (ODA) at the
 * Radio Data System (RDS) class. It directly handles
 * RDS group decoding, collects single- and multi-messages and
 * passes them to the Message Memory.
 */
class ISO_14819_EXPORT TrafficMessageChannel
{
public:
    explicit TrafficMessageChannel(uint16_t pi);

    /** TMC Application Identification Version 1 */
    static const uint16_t applicationIdV1 = 0xcd46;

    /** TMC Application Identification Version 2 */
    static const uint16_t applicationIdV2 = 0xcd47;

    /** TMC Application Identification for Test */
    static const uint16_t applicationIdTest = 0x0d45;

    /** link to program */
    const ISO62106::RdsProgram & rdsProgram;

    /** system information */
    SystemInformation systemInformation;

    /** tuning information */
    TuningInformation tuningInformation;

    /**
     * @brief Decoding of TMC message
     *
     * @param[in] pi PI code
     * @param[in] aid Application Identifier
     * @param[in] x Block 2 data
     * @param[in] y Block 3 data
     * @param[in] z Block 4 data
     */
    static void decodeA(uint16_t pi, uint16_t aid, uint8_t x, uint16_t y, uint16_t z);

    /**
     * @brief Decoding of ODA ident message information
     *
     * This function decodes the RDS ODA ident message.
     *
     * @param[in] pi PI code
     * @param[in] aid Application Identifier
     * @param[in] msg Message content
     */
    static void decodeIdent(uint16_t pi, uint16_t aid, uint16_t msg);

    /** register handler */
    static void registerHandler();

private:
    /** message that is currently processed */
    Message * m_message;

    /** last received continuity index (CI) */
    uint8_t m_lastContinuityIndex : 3;

    /** last group sequence indicator (GSI) */
    uint8_t m_lastGroupSequenceIndicator : 2;

    /** info from former decode, X part of msg */
    uint8_t m_lastX : 5;

    /** info from former decode, Y part of msg */
    uint16_t m_lastY : 16;

    /** info from former decode, Z part of msg */
    uint16_t m_lastZ : 16;

    /** info from former decode, message repetition */
    uint8_t m_decodeCnt;

    /**
     * @brief Decoding of single group TMC messages
     *
     * This function decodes single group TMC messages.
     *
     * @param[in] applicationId Application Identifier
     * @param[in] directionPersistance direction and persistance
     * @param[in] diversionAdvice diversion advice
     * @param[in] direction direction
     * @param[in] extent extent
     * @param[in] event event
     * @param[in] location location
     */
    void decodeSingle(uint16_t applicationId, uint8_t directionPersistance, uint8_t diversionAdvice, uint8_t direction, uint8_t extent, uint16_t event, uint16_t location);

    /**
     * @brief Decode tuning information
     *
     * This function decodes the tuning information.
     *
     * @param[in] address address code in blk2
     * @param[in] y message bits in blk3
     * @param[in] z message bits in blk4
     */
    void decodeTuningInformation(uint8_t address, uint16_t y, uint16_t z);

    /**
     * @brief Decoding of first group of multi-group TMC messages
     *
     * This function decodes first group of multi-group TMC messages.
     *
     * @param[in] applicationId Application Identifier
     * @param[in] continuityIndex continuity index
     * @param[in] direction direction
     * @param[in] extent extent
     * @param[in] event event
     * @param[in] location location
     */
    void decodeMultiFirst(uint16_t applicationId, uint8_t continuityIndex, uint8_t direction, uint8_t extent, uint16_t event, uint16_t location);

    /**
     * @brief Decoding of subsequent groups of multi-group TMC messages
     *
     * This function decodes subsequent groups of multi-group TMC messages.
     *
     * @param[in] continuityIndex continuity index
     * @param[in] secondGroupIndicator second group indicator
     * @param[in] groupSequenceId group sequence identifier
     * @param[in] freeFormat free format in blk 3 and blk 4 (28 bits from y11..z0)
     */
    void decodeMultiSubsequent(uint8_t continuityIndex, uint8_t secondGroupIndicator, uint8_t groupSequenceId, uint32_t freeFormat);

    /**
     * @brief Decoding of encryption administration group
     *
     * This function handles Encryption Administration group messages.
     *
     * @param[in] test Test Mode
     * @param[in] serviceId Service identifier
     * @param[in] encryptionId Encryption identifier
     * @param[in] locationTableNumberBeforeEncryption Location Table Number before encryption
     */
    void decodeEncryptedEag(uint8_t test, uint8_t serviceId, uint8_t encryptionId, uint8_t locationTableNumberBeforeEncryption);

    /**
     * @brief Push the message to Message Memory.
     */
    void pushToMessageMemory();
};

/**
 * This signals a decode issue.
 *
 * Usual events are:
 *   - not assigned, spare bits, variant unallocated, reserved for future use
 *   - for use by broadcasters, reserved for broadcasters use
 *   - not implemented yet
 *   - unknown ODA AID
 *   - iconv error
 */
extern ISO_14819_EXPORT sigc::signal<void, std::string> decodeIssue;

/**
 * Return reference to TMC list.
 *
 * @return Reference to TMC list
 */
extern ISO_14819_EXPORT std::map<uint16_t, TrafficMessageChannel> & trafficMessageChannel(); /* singleton */

/**
 * Return reference to TMC entry.
 *
 * @param[in] pi PI code
 * @return Reference to TMC entry
 */
extern ISO_14819_EXPORT TrafficMessageChannel & trafficMessageChannel(uint16_t pi);

}
