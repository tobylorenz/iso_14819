# Change Log
All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

## [1.0.1] - 2017-09-22
### Changed
- Update to latest project template.
- "using" instead of "typedef".
### Fixed
- tmcdump example now correctly retrieves location code list and displays names

## [1.0.0] - 2017-03-02
### Added
- Initial version
